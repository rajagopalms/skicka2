# Compile Utility Scripts
. .\Scripts\DeactivateFeatures.ps1
. .\Scripts\RemoveSiteCollections.ps1

function RetractSiteCollections
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML,
		[switch]$Remove,
		[switch]$DeactivateFeatures,
		[switch]$RetractSettings
	)

	if($DeactivateFeatures)
	{
		DisableFeatures $ConfigXML -Site
	}

	if($RetractSettings)
	{		
	}

	if($Remove)
	{
		RemoveSiteCollection $ConfigXML
	}
}