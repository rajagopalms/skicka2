function RestartTimerService
{
    ## Will restart the OWSTimer/SPTimerV4 service on all servers in Farm

    Write-Host "Restarting the OWSTimer/SPTimerV4 service on all servers in Farm..."

    $farmServerNameArray = GetServersInFarm

    Write-Host "Servers in farm:" 
    foreach($server in $farmServerNameArray)
    {
    	Write-Host $server.name
    }

    Write-Host "Stopping OWSTimer:"
    foreach($server in $farmServerNameArray)
    {
    	$temp = ("sc.exe \\" + $server.name + " stop SPTimerV4")
    	Write-Host ("Invoking: " + $temp)
    	$output = invoke-expression $temp
    	Write-Host "Done."
    }

    Write-Host "Sleep 10s (to make sure the first command is completed)..."
    sleep(10)

    Write-Host "Starting OWSTimer:"
    foreach($server in $farmServerNameArray)
    {
    	$temp = ("sc.exe \\" + $server.name + " start SPTimerV4")
    	Write-Host ("Invoking: " + $temp)
    	$output = invoke-expression $temp
    	Write-Host "Done."
    }

    Write-Host "Done restarting timer services."
}
