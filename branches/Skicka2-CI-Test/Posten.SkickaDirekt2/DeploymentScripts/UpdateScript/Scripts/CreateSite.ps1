function CreateSite
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML
	)

	PrintSpliter
	PrintLineMSG "Creation of Subsite(s) Start"
	
	$ConfigXML.Deployment.Standard.WebApplications.WebApplication | ForEach-Object {             
		[string]$webName = $_.Name		
		$_.SiteCollections.SiteCollection.SubSites.Site | ForEach-Object {  
			[string]$name = $_.CreateSite.Name
			[string]$desc = $_.CreateSite.Description 
			[string]$url = $_.CreateSite.URL 
			[string]$template = "CMSPUBLISHING#0";
			
			if($_.CreateSite.Template -ne $null -and $_.CreateSite.Template -ne "")
			{
				$template = $_.CreateSite.Template
			}
			
			$webApplication = $null
			$webApplication = Get-SPWebApplication -ErrorAction SilentlyContinue | Where {$_.DisplayName -eq $webName}							
			if($webApplication -eq $null)
			{
				StopScript "CreateSite" "The Specified Web Application doesn't Exist OR UnAvailable."
			}

			$webUrl = $url	
			$web = Get-SPWeb $webUrl -ErrorAction SilentlyContinue

			if (!$web)
			{
				Try
				{
					Write-Host "Create Subsite:" $webUrl "... " -f Blue -NoNewLine
					#$y = New-SPWeb -Url "$webUrl" -Language 1053 -Name "$name" -Template "CMSPUBLISHING#0" -Description "$desc"									
					$y = New-SPWeb -Url "$webUrl" -Language 1053 -Name "$name" -Template $template -Description "$desc"									
					Write-Host "DONE"
				} 
				Catch
				{
					StopScript "CreateSite" $_
				} 
				Finally { }          
			}				
			else
			{
				Write-Host "Subsite [$webUrl] already exist." -f Cyan
			}
		        
		}
	}

	PrintLineMSG "Creation of SubSite(s) Ended."
	PrintSpliter
}




