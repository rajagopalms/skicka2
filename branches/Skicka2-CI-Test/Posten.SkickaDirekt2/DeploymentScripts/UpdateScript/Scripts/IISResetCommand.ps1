function IISResetCommand
{
    # Will restart the OWSTimer/SPTimerV4 service on all servers in Farm

    Write-Host "Restarting the IIS processes on all servers in Farm..."

    $farmServerNameArray = GetServersInFarm

    Write-Host "Servers in farm:" 
    foreach($server in $farmServerNameArray)
    {
    	Write-Host $server.name
    }

    Write-Host "Restarting:"
    foreach($server in $farmServerNameArray)
    {
    	$temp = ("iisreset /noforce " + $server.name)
    	Write-Host ("Invoking: " + $temp)
    	$output = invoke-expression $temp
    	Write-Host "Done."
    }

    Write-Host "Done restarting IIS processes."
}