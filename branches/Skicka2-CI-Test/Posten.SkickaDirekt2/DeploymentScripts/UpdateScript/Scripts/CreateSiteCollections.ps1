function CreateSiteCollection
{
	param
	(
		[parameter(Mandatory = $true)]
		[xml] $ConfigXML
	)

	PrintSpliter
	PrintLineMSG "Creation of SiteCollection(s) Start"
	
	$ConfigXML.Deployment.Standard.WebApplications.WebApplication | ForEach-Object {             
		[string]$webName = $_.Name
		$_.SiteCollections.SiteCollection | ForEach-Object {  
			[string]$name = $_.CreateSiteCollections.Name
			[string]$desc = $_.CreateSiteCollections.Description 
			[string]$url = $_.CreateSiteCollections.URL 
			[string]$template = $_.CreateSiteCollections.Template
			[string]$language = $_.CreateSiteCollections.Language 
			[string]$admin = $_.CreateSiteCollections.Administrator
			[string]$email = $_.CreateSiteCollections.Email
			[string]$language = $_.CreateSiteCollections.Language
			[bool]$hostHeaderSite = [bool]::Parse($_.CreateSiteCollections.IsHostHeaderSite)

			$webApplication = $null
			$webApplication = Get-SPWebApplication -ErrorAction SilentlyContinue | Where {$_.DisplayName -eq $webName}							
			if($webApplication -eq $null)
			{
				StopScript "CreateSiteCollection" "The Specified Web Application doesn't Exist OR UnAvailable."
			}
				
			if($hostHeaderSite)
			{
				$sp = $null
				$sp = Get-SPSite -Identity $url -ErrorAction SilentlyContinue

				if($sp -ne $null)
				{
					PrintLineMSG "Site Collection [$url] already exists. Success."
				} 
				else
				{
					Try
            		{              
                		PrintMSG "Creating Site Collection .... $url"		
						New-SPSite -HostHeaderWebApplication $webApplication -Url $url -OwnerAlias $admin -Name $name -Template $template -Language $language -WarningAction silentlyContinue					
						PrintLineMSG "Site Collection Created. DONE! "
					} 
            		Catch
            		{
    					StopScript "CreateSiteCollection" $_
    				} 
            		Finally { }          
				}
			}
			else
			{
				$webURL = $webApplication.URL
				$siteURL = $webURL += $url		
				$sp = $null
				$sp = Get-SPSite -Identity $siteURL -ErrorAction SilentlyContinue

				if($sp -ne $null)
				{
					PrintLineMSG "Site Collection [$url] already exists. Success."
				} 
				else
				{
					Try
            		{              
                		PrintMSG "Creating Site Collection .... $siteURL"		
						New-SPSite -Url $siteURL -OwnerAlias $admin -Name $name -Template $template -Language $language						
						PrintLineMSG "Site Created. DONE! "
					} 
            		Catch
            		{
    					StopScript "CreateSiteCollection" $_
    				} 
            		Finally { }          
				}				
			}
		}
	}

	PrintLineMSG "Creation of SiteCollection(s) Ended."
	PrintSpliter
}



