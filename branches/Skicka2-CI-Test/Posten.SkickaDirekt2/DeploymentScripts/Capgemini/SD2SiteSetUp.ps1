.\LoadConfig.ps1

$SiteUrl = $settings["siteUrl"]
$SiteAdministrator = $settings["siteAdministrator"]
$SiteTitle = $settings["siteTitle"]
$SiteLanguage = $settings["language"]
$WebApplicationUrl = $settings["webApplicationUrl"]

#------ Skicka2 ------#
#Create Skicka2 Site If Does Not Already Exist
$targetSite = Get-SPSite $SiteUrl -ErrorAction SilentlyContinue

if($targetSite -ne $null)
{
echo "Removing Existing Site..."
Remove-SPSite $targetSite -Confirm:$false;
}

echo "Creating New Site..."
$targetSite = New-SPSite $SiteUrl -OwnerAlias $SiteAdministrator -Name $SiteTitle  -template "BLANKINTERNET#0" -Language $SiteLanguage
#-hostheader $WebApplicationUrl

$web = $targetSite.RootWeb
$web.AnonymousState = 2;
$web.update();
echo "New Site created at "$SiteUrl

#Activate Features in the right order


#Activate Features in the right order
echo "----------------------------------------------------------------------------"
echo "Activating Feature: SD2 Common"
echo "----------------------------------------------------------------------------"
Enable-SPFeature 299d011d-624d-4679-bb6a-a4fd14a4277f -url $SiteUrl 

echo "----------------------------------------------------------------------------"
echo "Activating Feature: SD2 Branding"
echo "----------------------------------------------------------------------------"
Enable-SPFeature 1d179295-9e02-4313-b564-9cb79d34719f -url $SiteUrl

echo "----------------------------------------------------------------------------"
echo "Activating Feature: SD2 WebParts"
echo "----------------------------------------------------------------------------"
Enable-SPFeature 458a54ca-73d1-4566-86d5-6024fb1d4012 -url $SiteUrl

echo "----------------------------------------------------------------------------"
echo "Activating Feature: SD2 SiteSetUp"
echo "----------------------------------------------------------------------------"
Enable-SPFeature dd0507f0-07b3-4603-9d35-7ff4ee5da865 -url $SiteUrl