﻿using System;

namespace Posten.Portal.Cart.BusinessEntities
{
    public class PaymentResult : IPaymentResult
    {
        public long TransactionId { get; set; }

        public string Receipt { get; set; }

        public Uri ReceiptUrl { get; set; }

        public long TransactionState { get; set; }
    }
}
