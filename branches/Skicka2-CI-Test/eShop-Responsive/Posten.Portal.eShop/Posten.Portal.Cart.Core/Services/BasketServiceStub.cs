﻿namespace Posten.Portal.Cart.Services
{
    using System;
    using System.Collections.Generic;

    using Posten.Portal.Cart.BusinessEntities;

    public class BasketServiceStub : IBasketService
    {
        #region Methods

        public bool AssignBillingAccount(string basketId, IBillingAccountInfo billingInfo)
        {
            throw new NotImplementedException();
        }

        public bool AssignDeliveryAccount(string basketId, IDeliveryAccountInfo deliveryInfo)
        {
            throw new NotImplementedException();
        }

        public bool AssignToKnownUser(string basketId, string knownUserId)
        {
            throw new NotImplementedException();
        }

        public bool FinalisePayment(string basketId, DateTime paymentDate, string paymentMethod, Uri receiptUrl, long transactionId, string transactionState)
        {
            throw new NotImplementedException();
        }

        public ICollection<IPaymentMethod> GetAvailablePayments(string basketId)
        {
            throw new NotImplementedException();
        }

        public ICollection<IPaymentArticle> GetBasketArticles(string basketId)
        {
            throw new NotImplementedException();
        }

        public IBasketItem GetBasketItem(string basketId)
        {
            throw new NotImplementedException();
        }

        public IBasketItem GetBasketItemByBookingId(string bookingId)
        {
            var basketItem = new BasketItem()
            {
                BasketId = "BasketId",
                BasketStatus = BasketStatus.Ordered,
                BillingAccountInfo = new BillingAccountInfo()
                {
                    AddressLine = "AddressLine",
                    City = "City",
                    CountryCode = "CountryCode",
                    CountryName = "CountryName",
                    Email = "Email",
                    IncludeAddressInfo = true,
                    LastName = "LastName",
                    Name = "Name",
                    Organization = "Organization",
                    PostalCode = "PostalCode",
                    Telephone = "Telephone",
                },
                BookingId = "BookingId",
                DeliveryAccountInfo = new DeliveryAccountInfo()
                {
                    AddressLine = "AddressLine",
                    City = "City",
                    CountryCode = "CountryCode",
                    CountryName = "CountryName",
                    Email = "Email",
                    LastName = "LastName",
                    FirstName = "FirstName",
                    Organization = "Organization",
                    PostalCode = "PostalCode",
                    Telephone = "Telephone",
                    RegionCode = "RegionCode",
                    RegionName = "RegionName",
                },
                OrderSummaryItems = new List<IOrderSummaryItem>()
                {
                    new OrderSummaryItem() 
                    {
                        Created = DateTime.Now,
                        CurrencyCode = "CurrencyCode",
                        Details = "Details",
                        FreeTextField1 = "FreeTextField1",
                        FreeTextField2 = "FreeTextField2",
                        LinePrice = 0,
                        LinePriceString = "LinePriceString",
                        LineTaxString = "LineTaxString",
                        ListPrice = 0,
                        Name = "Name",
                        OrderItemImageUrl = new Uri("http://OrderItemImageUrl"),
                        OrderItemType = OrderItemType.Service,
                        OrderNumber = "OrderNumber",
                        Quantity = 1,
                        Vat = 0,
                    }
                },
                PaymentDate = DateTime.Now,
                PaymentMethod = "PaymentMethod",
                ReceiptUrl = new Uri("http://ReceiptUrl"),
                Total = 0,
                TotalExclTax = 0,
                TotalExclTaxString = "TotalExclTaxString",
                TotalString  = "TotalString",
                TotalTax = 0,
                TotalTaxString = "TotalTaxString",
                TransactionId = 0,
                TransactionState = "TransactionState",
            };

            return basketItem;
        }

        public IBasketItem GetBasketItemByTransactionId(long transactionId)
        {
            throw new NotImplementedException();
        }

        public ICollection<IBasketItem> GetBaskets()
        {
            throw new NotImplementedException();
        }

        public IBasketItem GetDefaultBasketItem()
        {
            throw new NotImplementedException();
        }

        public IBasketItem GetResolvedBasketItem(string basketid)
        {
            throw new NotImplementedException();
        }

        public bool InitialisePayment(string basketId, string[] deliveryIds, long transactionId)
        {
            throw new NotImplementedException();
        }

        public string PaymentComplete(string basketId, long transactionId, out bool result)
        {
            throw new NotImplementedException();
        }

        public string PaymentCompleteBypassQueue(string basketId, long transactionId, out bool result)
        {
            throw new NotImplementedException();
        }

        public bool PrepareForCheckout(string basketId, int layoutversion)
        {
            throw new NotImplementedException();
        }

        public IBasketItem RecalculateVAT(string basketId, string countrycode, string postalcode)
        {
            throw new NotImplementedException();
        }

        public bool ReverseInitialisePayment(string basketId, long transactionId)
        {
            throw new NotImplementedException();
        }

        public bool VerifyPaymentComplete(string basketId, long transactionId)
        {
            throw new NotImplementedException();
        }

        #endregion Methods
    }
}