﻿

function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}


function showConfirmPopUp(title, message, target, okbuttonname, cancelbuttonname, callback) {
    var width = $(window).width();
    //Get the full page height including the scroll area
    var height = $(document).height();
    $('body').prepend("<div id='mask2'></div>");
    $('#mask2').css('height', height);
    $('#mask2').css('width', width);
    $('#mask2').css('z-index', '3001');
    $("#Body").append('<div id="popup"></div>');

    var content = "<div id='window2' class='window'>";
    content += "<a href='#' class='close'><img src='/_layouts/images/posten/cart/close.png' alt='close icon' /></a>"
    if (title != "") { content += "<h2>" + title + "</h2>"; }
    content += "<p>" + message + "</p>";
    content += "<div class='CloseButton'><span><a id='closebutton'>" + cancelbuttonname + "</a></span></div>&nbsp;&nbsp;&nbsp;&nbsp;<div class='CloseButton'><span><a id='okaybutton'>" + okbuttonname + "</a></span></div>";
    content += "</div>";

    var popupTarget = $("#" + target);
    popupTarget.css("display", "none");
    popupTarget.html(content);
    popupTarget.css("display", "block");
    var popup = $("#window2");

    //Center Pop Up
    popup.css('top', 200);
    popup.css('left', width / 2 - popup.width() / 2);

    $('#mask2, .close, #closebutton').click(function () {
        closePopUp();
        callback(false);
    });

    $('#okaybutton').click(function () {
        closePopUp();
        callback(true);
    });
}



function showPopUp(title, message, target, autoFade, fadeTimeOut,cancelButtoName) {
    var width = $(window).width();
    //Get the full page height including the scroll area
    var height = $(document).height();
    var mask = $('#mask');
    if (mask.length == 0) {
        $('body').prepend("<div id='mask'></div>");
    }
    $('#mask').css('height', height);
    $('#mask').css('width', width);
    $('#mask').css('z-index', '3001');
    $("#Body").append('<div id="popup"></div>');

    var content = "<div id='window2' class='window'>";
    if (!autoFade) { content += "<a href='#' class='close'><img src='/_layouts/images/posten/cart/close.png' alt='close icon' /></a>" }
    if (title != "") { content += "<h2>" + title + "</h2>"; }

    content += "<p>" + message + "</p>";
    if (!autoFade) { content += "<div class='PopUpButton CloseButton'><span><a id='closebutton'>" + cancelButtoName + "</a></span></div>"; }
    content += "</div>";

    var popupTarget = $("#" + target);
    popupTarget.css("display", "none");
    popupTarget.html(content);
    popupTarget.css("display", "block");
    var popup = $("#window2");

    //Center Pop Up
    popup.css('top', 200);
    popup.css('left', width / 2 - popup.width() / 2);

    if (!autoFade) {
        $('#mask, .close, #closebutton').click(function () {
            closePopUp();
        });
    }
    else {
        setTimeout(function () {
            closePopUp();
        }, fadeTimeOut);
    }
}

function closePopUp() {
    $('.window').fadeOut(1500);
    $('#mask').hide();
    $('#mask2').hide();
}