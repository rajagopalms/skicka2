﻿using Microsoft.SharePoint.WebControls;

namespace Posten.Portal.Cart.Presentation.ApplicationPages
{
    public partial class ConfirmationPage : UnsecuredLayoutsPageBase
    {
        protected override bool AllowAnonymousAccess
        {
            get { return true; }
        }
    }
}
