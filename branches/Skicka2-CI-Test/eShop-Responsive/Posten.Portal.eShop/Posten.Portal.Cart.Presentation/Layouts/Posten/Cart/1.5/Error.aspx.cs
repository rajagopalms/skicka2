﻿namespace Posten.Portal.Cart.Presentation.ApplicationPages.Legacy
{
    using System;

    using Microsoft.SharePoint;
    using Microsoft.SharePoint.WebControls;

    public partial class ErrorPage : UnsecuredLayoutsPageBase
    {
        #region Properties

        protected override bool AllowAnonymousAccess
        {
            get { return true; }
        }

        #endregion Properties

        #region Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            this.ErrorMessage.Text = this.Request.QueryString["Message"] ?? string.Empty;
        }

        #endregion Methods
    }
}