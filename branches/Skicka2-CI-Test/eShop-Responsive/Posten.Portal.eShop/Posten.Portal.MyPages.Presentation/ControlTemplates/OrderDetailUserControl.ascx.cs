﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Posten.Portal.Cart;
using Posten.Portal.Cart.BusinessEntities;
using Posten.Portal.Platform.Common.Logging;
using System.Reflection;
using System.Resources;

namespace Posten.Portal.MyPages.Presentation.ControlTemplates
{
    public partial class OrderDetailUserControl : UserControl
    {
        protected string BookingId
        {
            get
            {
                return Request.QueryString["BookingId"] ?? Request.QueryString["bookingId"] ?? Request.QueryString["bookingid"] ?? string.Empty;
            }
        }

        protected string ReceiptUrl 
        {
            get 
            {
                return string.Format("/_layouts/Posten/Cart/common/Receipt.ashx?BookingId={0}", this.OrderContext.BookingId);
            }
        }

        protected Cart.OrderContext OrderContext
        {
            get
            {
                return Cart.OrderContext.GetContext(this.BookingId);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (IsPostBack)
                {
                    return;
                }

                this.orderDetailModule.HeaderText = GetGlobalResourceObject("PostenMyPages", "OrderHistory_OrderNumber_Text") +":" + this.OrderContext.BookingId;
                this.PrintReceiptButton.Attributes.Add("onclick", string.Format("window.open('{0}'); return false;", this.ReceiptUrl));
                /*basket totals*/
                IBasketItem basketItems = this.OrderContext.BasketItem;
                this.TotalAmount.Text = basketItems.TotalString;
                this.TotalExclusiveTax.Text = basketItems.TotalExclTaxString;
                this.TotalTaxAmount.Text = basketItems.TotalTaxString;
                this.Total.Text = basketItems.TotalString;

                /*get products and bind to repeater*/
                this.ProductsList.DataSource = this.OrderContext.ProductOrderItems;
                this.ProductsList.DataBind();

                /*get services and bind to repeater*/
                this.ServicesList.DataSource = this.OrderContext.ServiceOrderItems;
                this.ServicesList.DataBind();

                this.ShowAccountInformation();
            }
            catch (Exception ex) {
                PostenLogger.LogError(ex.Message.ToString() + "\n\r" + ex.StackTrace.ToString(), PostenLogger.ApplicationLogCategory, MethodInfo.GetCurrentMethod().DeclaringType, MethodInfo.GetCurrentMethod().Name);
            }
        }

        private void ShowAccountInformation()
        {            
            IBasketItem basketItem = this.OrderContext.BasketItem;

            IDeliveryAccountInfo deliveryAccountInfo = basketItem.DeliveryAccountInfo;
            IBillingAccountInfo billingAccountInfo = basketItem.BillingAccountInfo;

            string name = (!string.IsNullOrEmpty(deliveryAccountInfo.FirstName) && !string.IsNullOrEmpty(deliveryAccountInfo.LastName)) ?
                            string.Format("{0} {1}", deliveryAccountInfo.FirstName, deliveryAccountInfo.LastName) :
                            (!string.IsNullOrEmpty(billingAccountInfo.Name + " " + billingAccountInfo.LastName)) ? billingAccountInfo.Name + billingAccountInfo.LastName : string.Empty;

            string address = (!string.IsNullOrEmpty(deliveryAccountInfo.AddressLine)) ?
                             deliveryAccountInfo.AddressLine :
                             (!string.IsNullOrEmpty(billingAccountInfo.AddressLine)) ? billingAccountInfo.AddressLine : string.Empty;
            
            string postal = (!string.IsNullOrEmpty(deliveryAccountInfo.PostalCode)) ?
                            deliveryAccountInfo.PostalCode :
                            (!string.IsNullOrEmpty(billingAccountInfo.PostalCode)) ? billingAccountInfo.PostalCode : string.Empty;

            string city =   (!string.IsNullOrEmpty(deliveryAccountInfo.City)) ?
                            deliveryAccountInfo.City :
                            (!string.IsNullOrEmpty(billingAccountInfo.City)) ? billingAccountInfo.City : string.Empty;

            string country = (!string.IsNullOrEmpty(deliveryAccountInfo.CountryName)) ?
                            deliveryAccountInfo.CountryName : string.Empty;

            string email = (!string.IsNullOrEmpty(deliveryAccountInfo.Email)) ?
                             deliveryAccountInfo.Email :
                             (!string.IsNullOrEmpty(billingAccountInfo.Email)) ? billingAccountInfo.Email : string.Empty;

            string telno = (!string.IsNullOrEmpty(deliveryAccountInfo.Telephone)) ?
                             deliveryAccountInfo.Telephone :
                             (!string.IsNullOrEmpty(billingAccountInfo.Telephone)) ? billingAccountInfo.Telephone : string.Empty;
            
            string paymentMethod = basketItem.PaymentMethod;
            
            /*format date*/
            DateTime dt = basketItem.PaymentDate;
            string pdaystr = dt.ToString("dddd");
            string pdayint = dt.Day.ToOrdinal();
            string pmonth = dt.ToString("MMMM");
            string pyear = dt.ToString("yyyy");
            string ptime = dt.ToString("hh:mm tt");

            this.PaymentDateValue.Text = string.Format("{0} {1} {2} {3} at {4}", pdaystr, pdayint, pmonth, pyear, ptime);
            this.CustomerNameValue.Text = name;
            this.DeliveryCustomerName.Text = name;
            this.AddressValue.Text = address;
            this.PostalCodeValue.Text = postal;
            this.CityValue.Text = city;
            this.CountryValue.Text = country;
            this.EmailValue.Text = email;
            //this.PhoneNumberValue.Text = string.Empty;
            this.MobileNumberValue.Text = telno;
            this.PaymentTypeValue.Text = paymentMethod;
            this.DeliveryShippingValue.Text = string.Empty;
        }
    }
}
