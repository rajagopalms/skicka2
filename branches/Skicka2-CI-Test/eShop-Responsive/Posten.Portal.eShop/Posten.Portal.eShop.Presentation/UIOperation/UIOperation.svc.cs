﻿namespace Posten.Portal.eShop.Presentation
{
    using System;
    using System.ServiceModel.Activation;
    using System.Threading;

    using Microsoft.SharePoint.Client.Services;

    using Posten.Portal.Cart;
    using Posten.Portal.Cart.BusinessEntities;
    using Posten.Portal.eShop.BusinessEntities;
    using Posten.Portal.eShop.Services;
    using Posten.Portal.Platform.Common.Container;

    [BasicHttpBindingServiceMetadataExchangeEndpoint]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class UIOperation : IUIOperation
    {
        #region Methods

        public bool AddProduct(string productNumber, string productOrderQuantity)
        {
            try
            {
                ProductModel product = ProductContext.GetProduct(productNumber);

                if (product != null)
                {
                    string result = BasketContext.Current.AddProductOrder(new ProductOrderItem
                    {
                        ArticleId = product.Id,
                        ArticleName = product.Name,
                        ArticlePrice = product.Price,
                        Vat = product.Vat,
                        VatPercentage = product.VatPercentage,
                        Quantity = Convert.ToInt32(productOrderQuantity),
                        CurrencyCode = product.CurrencyCode,
                    });
                    if (result != null)
                    {
                        return true;
                    }
                }

                return false;
            }
            catch
            {
                throw;
            }
        }

        #endregion Methods
    }
}