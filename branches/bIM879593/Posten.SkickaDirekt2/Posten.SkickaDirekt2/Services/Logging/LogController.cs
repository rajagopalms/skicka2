﻿namespace Posten.Portal.Skicka.Services.Logging
{
    using System;
    using System.Security.Permissions;
    using System.Web.Script.Serialization;
    using Posten.Portal.Skicka.Services.Utils;
    using Microsoft.SharePoint;
    using Microsoft.SharePoint.Administration;
    using Microsoft.SharePoint.Security;

    using Posten.SkickaDirekt2.Resources;

    public class LogController : ILogController
    {
        #region Fields

        private readonly LoggingService _logger;

        #endregion Fields

        #region Constructors

        [SharePointPermission(SecurityAction.InheritanceDemand, ObjectModel = true)]
        [SharePointPermission(SecurityAction.LinkDemand, ObjectModel = true)]
        public LogController()
        {
            _logger = SPFarm.Local.Services.GetValue<LoggingService>(Constants.LogCategoryDefault);

            // If the PortalLoggingService has not been provisioned as a persistent object then _logger will be null so get a new PortalLoggingService instead
            // that logger will not use the settings (trace and event level) from central admin however. It will use default values.
            if (_logger == null)
            {
                _logger = new LoggingService(Constants.LogCategoryDefault, SPFarm.Local);
            }
        }

        #endregion Constructors

        #region Methods

        public string LogError(string message)
        {
            Log(TraceSeverity.Unexpected, message);
            var correlationID = LogUtility.LogVerbose(message, this.GetType(), System.Reflection.MethodBase.GetCurrentMethod().Name);
            return correlationID;
        }

        public string LogError(string transactionLogID, string message)
        {
            var correlationID = LogUtility.GetCorrelationId();
            var jsonLogItem = ReturnLogItem(transactionLogID, correlationID, message);            
            LogUtility.LogVerbose(jsonLogItem, this.GetType(), System.Reflection.MethodBase.GetCurrentMethod().Name);
            
            Log(TraceSeverity.Unexpected, jsonLogItem);
            return correlationID;
        }

        public string LogError(Exception exception)
        {
            Log(TraceSeverity.Unexpected, exception.ToString());
            var correlationID = LogUtility.LogCriticalException(exception, this.GetType(), System.Reflection.MethodBase.GetCurrentMethod().Name);
            return correlationID;
        }

        public string LogError(string transactionLogID, Exception exception)
        {
            var correlationID = LogUtility.GetCorrelationId();
            var jsonLogItem = ReturnLogItem(transactionLogID, correlationID, exception.ToString());
            LogUtility.LogCriticalException(exception, this.GetType(), System.Reflection.MethodBase.GetCurrentMethod().Name);
            
            Log(TraceSeverity.Unexpected, jsonLogItem);            
            return correlationID;
        }

        public string LogMessage(string message)
        {
            Log(TraceSeverity.Medium, message);
            var correlationID = LogUtility.LogVerbose(message, this.GetType(), System.Reflection.MethodBase.GetCurrentMethod().Name);
            return correlationID;
        }

        public string LogMessage(string transactionLogID, string message)
        {
            var correlationID = LogUtility.GetCorrelationId();
            var jsonLogItem = ReturnLogItem(transactionLogID, correlationID, message);            
            LogUtility.LogVerbose(jsonLogItem, this.GetType(), System.Reflection.MethodBase.GetCurrentMethod().Name);
            
            Log(TraceSeverity.Medium, jsonLogItem);
            return correlationID;
        }

        public string LogVerbose(string message)
        {
            Log(TraceSeverity.Verbose, message);
            var correlationID = LogUtility.LogVerbose(message, this.GetType(), System.Reflection.MethodBase.GetCurrentMethod().Name);
            return correlationID;
        }

        public string LogVerbose(string transactionLogID, string message)
        {
            var correlationID = LogUtility.GetCorrelationId();
            var jsonLogItem = ReturnLogItem(transactionLogID, correlationID, message);            
            LogUtility.LogVerbose(jsonLogItem, this.GetType(), System.Reflection.MethodBase.GetCurrentMethod().Name);
            
            Log(TraceSeverity.Verbose, jsonLogItem);
            return correlationID;
        }

        [SharePointPermission(SecurityAction.InheritanceDemand, ObjectModel = true)]
        [SharePointPermission(SecurityAction.LinkDemand, ObjectModel = true)]
        public void Unregister()
        {
            if (_logger != null)
            {
                _logger.Delete();
            }
        }

        private static string ReturnLogItem(string transactionLogID,string correlationID, string logMessage)
        {
            var logItem = new
            {
                LogMessage = logMessage,
                TransactionLogID = transactionLogID,
                CorrelationID = correlationID
            };

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var jsonLogItem = serializer.Serialize(logItem);

            return jsonLogItem;
        }

        private void Log(TraceSeverity severity, string message)
        {
            try
            {
                var category = _logger.Areas[Constants.LogCategoryDefault].Categories[Constants.LogCategoryDefault];
                _logger.WriteTrace(0, category, severity, message);
            }
            catch (SPException exception)
            {
                var errorMsg = exception.Message;
            }
        }

        #endregion Methods
    }
}