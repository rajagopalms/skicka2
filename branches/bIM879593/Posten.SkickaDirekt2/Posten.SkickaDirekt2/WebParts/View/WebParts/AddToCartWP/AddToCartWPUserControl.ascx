﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="CustomControls" TagName="AddToCartWPDisplay" Src="~/_CONTROLTEMPLATES/Posten.SkickaDirekt2/AddToCartWP/AddToCartWPDisplay.ascx" %>
<%@ Register TagPrefix="CustomControls" TagName="AddToCartWPEdit" Src="~/_CONTROLTEMPLATES/Posten.SkickaDirekt2/AddToCartWP/AddToCartWPEdit.ascx" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddToCartWPUserControl.ascx.cs" Inherits="Posten.SkickaDirekt2.WebParts.UserControls.AddToCartWPUserControl" %>

<%--<SharePoint:ScriptLink runat="server" ID="sampleWP" Name="/Posten.SkickaDirekt2/JS/AddToCartWP.js" Localizable="false" LoadAfterUI="true"></SharePoint:ScriptLink>--%>

<div id="AddToCartWPDisplayPanel" runat="server">
    <CustomControls:AddToCartWPDisplay ID="displayControl" runat="server"></CustomControls:AddToCartWPDisplay>
</div>

<div id="AddToCartWPEditPanel" runat="server">
    <CustomControls:AddToCartWPEdit ID="editControl" runat="server"></CustomControls:AddToCartWPEdit>
</div>
<div id="savePanel" runat="server">
    <input id="saveButton" runat="server" type="button" value="Spara" />
</div>

