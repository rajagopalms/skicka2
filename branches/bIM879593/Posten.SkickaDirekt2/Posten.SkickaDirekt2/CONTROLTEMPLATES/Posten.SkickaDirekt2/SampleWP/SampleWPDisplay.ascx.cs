﻿namespace Posten.SkickaDirekt2.CustomControls
{
    using System;
    using System.Web.UI;

    public partial class SampleWPDisplay : UserControl
    {
        #region Fields

        private string sampleProperty;

        #endregion Fields

        #region Properties

        public string SampleProperty
        {
            get
            {
                return sampleProperty;
            }
            set
            {
                sampleProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            this.propertyDisplay.InnerText = sampleProperty;
        }

        #endregion Methods
    }
}