﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Src="~/_CONTROLTEMPLATES/Posten.SkickaDirekt2/ServicesResultItemWP/ServicesResultItem.ascx" TagPrefix="CustomControl" TagName="ServicesResultItem" %>

<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ServicesResultItemWPDisplay.ascx.cs" Inherits="Posten.SkickaDirekt2.CustomControls.ServicesResultItemWPDisplay" %>

<SharePoint:ScriptLink runat="server" ID="ServicesResultItemJS" Name="/Posten.SkickaDirekt2/JS/ServicesResultItemWP.js" Localizable="false" LoadAfterUI="true"></SharePoint:ScriptLink>

<div id="ServicesResultItemDisplay" style="display: none" data-bind="visible: (ProductSuggestions()!== null), scrollToView: ProductsScrollTo()">

    <div class="borders rounded bottomMargin" data-bind="visible: (ProductSuggestions()!== null)">
        <%--Vårt förslag--%>
        <div class="paddings lightGradient header topRounded" data-bind="visible: (ProductSuggestions()!== null)">
            <span runat="server" id="headerPropertyDisplay"></span>
        </div>

        <%--Selected Product Suggestion by user--%>
        <div data-bind="if: (SelectedProductSuggestion() !== null && SelectedProductSuggestion() !== undefined)">
        <div data-bind="template: { name: 'ServiceResultItemTemplate', data: SelectedProductSuggestion() }, visible: (SelectedProductSuggestion() !== null)"></div>        
        </div>
        <%--No items found--%>
        <%--<div class="paddings header" data-bind="visible: ((!SelectedProductSuggestion()) && RankedServices() && (RankedServices().length == 0))">Vi har tyvärr inte lyckats hitta någon tjänst som matchar dina kriterier.</div>--%>
                
        <%--Suggestions by system--%>
        <div data-bind="if: ProductSuggestions() !== null && RankedServices() && RankedServices().length > 0">     
            <div data-bind="foreach: RankedServices()">
                <div data-bind="if: $data.Rank  == 1">
                    <div data-bind="template: { name: 'ServiceResultItemTemplate', data: $data }"></div>  
                </div>              
            </div>

            <%--Click for more suggestions--%>
            <div class="paddings lightGradient rightText topBorder">                
                    <a id="moreProposalsLinkPropertyDisplay" runat="server" href="#" onclick="javascript: resultItemToggleResults(this); return false" class="bold resultShowMoreClosed"></a>               
            </div>

             <div id="resultMoreSuggestions" class="lightGradient noneDisplay">
                <div data-bind="foreach: RankedServices() ">  
                    <div data-bind="if: $data.Rank > 1">
                        <div data-bind="template: { name: 'ServiceResultItemTemplate', data: $data }"></div>            
                    </div>          
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/html" id="ServiceResultItemTemplate">
    <CustomControl:ServicesResultItem runat="server" id="ServicesResultItem" />
</script>