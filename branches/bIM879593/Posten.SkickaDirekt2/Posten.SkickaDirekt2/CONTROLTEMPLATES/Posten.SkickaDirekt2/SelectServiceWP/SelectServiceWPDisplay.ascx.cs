﻿namespace Posten.SkickaDirekt2.CustomControls
{
    using System;
    using System.Web;
    using System.Web.UI;

    public partial class SelectServiceWPDisplay : UserControl
    {
        #region Fields

        private string countryInformationProperty;
        private string countryLabelProperty;
        private string countryTooltipProperty;
        private string fastButtonProperty;
        private string headerProperty;
        private string secureButtonProperty;
        private string sendServiceLabelProperty;
        private string sendServiceTooltipProperty;
        private string serviceFilterLabelProperty;
        private string serviceFilterTooltipProperty;
        private string sizeErrorProperty;
        private string sizeLabelProperty;
        private string sizeTooltipProperty;
        private string traceableButtonProperty;
        private string weightLabelProperty;
        private string weightTooltipProperty;

        #endregion Fields

        #region Properties
        public string CountryInformationProperty
        {
            get
            {
                return countryInformationProperty;
            }
            set
            {
                countryInformationProperty = value;
            }
        }

        public string CountryLabelProperty
        {
            get
            {
                return countryLabelProperty;
            }
            set
            {
                countryLabelProperty = value;
            }
        }

        public string CountryTooltipProperty
        {
            get
            {
                return countryTooltipProperty;
            }
            set
            {
                countryTooltipProperty = value;
            }
        }

        public string FastButtonProperty
        {
            get
            {
                return fastButtonProperty;
            }
            set
            {
                fastButtonProperty = value;
            }
        }

        public string HeaderProperty
        {
            get
            {
                return headerProperty;
            }
            set
            {
                headerProperty = value;
            }
        }

        public string SecureButtonProperty
        {
            get
            {
                return secureButtonProperty;
            }
            set
            {
                secureButtonProperty = value;
            }
        }

        public string SendServiceLabelProperty
        {
            get
            {
                return sendServiceLabelProperty;
            }
            set
            {
                sendServiceLabelProperty = value;
            }
        }

        public string SendServiceTooltipProperty
        {
            get
            {
                return sendServiceTooltipProperty;
            }
            set
            {
                sendServiceTooltipProperty = value;
            }
        }

        public string ServiceFilterLabelProperty
        {
            get
            {
                return serviceFilterLabelProperty;
            }
            set
            {
                serviceFilterLabelProperty = value;
            }
        }

        public string ServiceFilterTooltipProperty
        {
            get
            {
                return serviceFilterTooltipProperty;
            }
            set
            {
                serviceFilterTooltipProperty = value;
            }
        }

        public string SizeErrorProperty
        {
            get
            {
                return sizeErrorProperty;
            }
            set
            {
                sizeErrorProperty = value;
            }
        }

        public string SizeLabelProperty
        {
            get
            {
                return sizeLabelProperty;
            }
            set
            {
                sizeLabelProperty = value;
            }
        }

        public string SizeTooltipProperty
        {
            get
            {
                return sizeTooltipProperty;
            }
            set
            {
                sizeTooltipProperty = value;
            }
        }

        public string TraceableButtonProperty
        {
            get
            {
                return traceableButtonProperty;
            }
            set
            {
                traceableButtonProperty = value;
            }
        }

        public string WeightLabelProperty
        {
            get
            {
                return weightLabelProperty;
            }
            set
            {
                weightLabelProperty = value;
            }
        }

        public string WeightTooltipProperty
        {
            get
            {
                return weightTooltipProperty;
            }
            set
            {
                weightTooltipProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            this.headerPropertyDisplay.InnerText = headerProperty;
            this.countryLabelPropertyDisplay.InnerText = countryLabelProperty;
            this.sendServiceLabelPropertyDisplay.InnerText = sendServiceLabelProperty;
            this.sizeLabelPropertyDisplay.InnerText = sizeLabelProperty;
            this.weightLabelPropertyDisplay.InnerText = weightLabelProperty;
            this.serviceFilterLabelPropertyDisplay.InnerText = serviceFilterLabelProperty;
            this.countryTooltipPropertyDisplay.InnerHtml = countryTooltipProperty;
            this.sendServiceTooltipPropertyDisplay.InnerHtml = sendServiceTooltipProperty;
            this.sizeTooltipPropertyDisplay.InnerHtml = sizeTooltipProperty;
            this.weightTooltipPropertyDisplay.InnerHtml = weightTooltipProperty;
            this.serviceFilterTooltipPropertyDisplay.InnerHtml = serviceFilterTooltipProperty;
            this.fastButtonPropertyDisplay.InnerText = fastButtonProperty;
            this.secureButtonPropertyDisplay.InnerText = secureButtonProperty;
            this.traceableButtonPropertyDisplay.InnerText = traceableButtonProperty;
            this.sizeErrorPropertyDisplay.InnerText = sizeErrorProperty;

            this.chooseServiceLabel.InnerText = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_ChooseSercviceLabel") as string;
            this.lengthLabel.InnerText = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_LengthLabel") as string;
            this.widthLabel.InnerText = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_WidthLabel") as string;
            this.heightLabel.InnerText = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_HeightLabel") as string;
            this.diameterLabel.InnerText = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_DiameterLabel") as string;
            this.sendTubeLabel.InnerText = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_SendTubeLabel") as string;

            this.envelopeC4.InnerText = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_EnvelopeC4") as string;
            this.envelopeC5.InnerText = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_EnvelopeC5") as string;
            this.measureMyself.InnerText = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_MeasureMyself") as string;
            this.heightUnit.InnerText = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_SizeUnit") as string;
            this.widthUnit.InnerText = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_SizeUnit") as string;
            this.lengthUnit.InnerText = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_SizeUnit") as string;
            this.diameterUnit.InnerText = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_SizeUnit") as string;
            this.selectServiceLetterImage.Src = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_LetterlImage") as string;
            this.selectServiceParcelImage.Src = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SelectService_ParcelImage") as string;
        }

        #endregion Methods
    }
}