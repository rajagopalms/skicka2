﻿namespace Posten.Portal.Skicka.Services.SkickaService
{
    using System.Collections.Generic;
    using System.Web;

    using Posten.Portal.Skicka.Services.BusinessEntities;

    public interface ISkickaService
    {
        #region Methods        

        ReceivingPlaceBE FindReceivingPlace(string bookingId);

        List<OrderLabelBE> GetAvailableOrderLabels();

        ConfigurationItemBE GetConfigurationItem();

        ConsignmentBE GetConsignment(string orderNumber);

        List<ConsignmentBE> GetConsignments(string bookningId);

        byte[] GetItemNotesPDF(string bookningId);

        List<ReceivingPlaceBE> GetValidServicePoints(string bookingId, string[] productionPointIds);

        string GetVersion();

        InformPutInCartResponseBE InformPutInCart(
            string orderNumber,
            ValidationResponseBE validatedService,
            string serviceName,
            string serviceCode,
            SizeBE size,
            int weight,
            AddressBE fromAddress,
            AddressBE toAddress,
            double codAmount,
            bool plusgiro,
            string postforskottKontoNr,
            string postforskottPaymentReference,
            string orderconfirmationUrl,
            string urlText,
            string url,
            string details,
            string receiverDestinationId);

        InformPutInCartResponseBE InformUpdateInCart(
            string newOrderNumber,
            string oldOrderNumber,
            string baseServiceOrderRowNumber,
            string serviceName,
            string serviceCode,
            ValidationResponseBE validatedService,
            ServicePropertiesBE serviceCombination,
            SizeBE size,
            int weight,
            AddressBE fromAddress,
            AddressBE toAddress,
            double codAmount,
            bool plusgiro,
            string postforskottKontoNr,
            string postforskottPaymentReference,
            string orderconfirmationUrl,
            string urlText,
            string url,
            string details,
            string receiverDestinationId);

        void Initialize(HttpContext context);

        string OrderLabels(string[] prodIds, string customerName, string contactPerson, string email, string streetAddress, string zip, string city, string phone, string addressCo);

        string ReturnWithColon(string intime);

        bool RemoveConsignment(string orderNumber);
        #endregion Methods
    }
}