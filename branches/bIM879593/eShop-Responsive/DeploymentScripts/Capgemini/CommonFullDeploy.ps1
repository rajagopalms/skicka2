# Get Start Time
$startDTM = (Get-Date)

. .\Functions.ps1

Write-Host "*******Cleaning Skicka2 Packages from the farm *****************"
Reset-IISOnServers
RetractSolution "Posten.Portal.Platform.Common.Presentation.wsp"
RetractSolution "Posten.Portal.Platform.Common.Dependencies.wsp"
RetractSolution "Posten.Portal.Platform.Common.wsp"

Reset-IISOnServers $True


Write-Host "*******Deploying Skicka2 Packages on the farm *****************"
Reset-IISOnServers
DeploySolution "Posten.Portal.Platform.Common.wsp"
DeploySolution "Posten.Portal.Platform.Common.Dependencies.wsp"
DeploySolution "Posten.Portal.Platform.Common.Presentation.wsp"

Reset-IISOnServers $True

Write-Host "*******Setting up Skicka2 Site*****************"
.\CommonFeatureActivation.ps1


