﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Posten.Portal.Cart.BusinessEntities
{
    public interface IPaymentRequestBase
    {
        ICollection<IPaymentArticle> Articles { get; set; }
        bool CaptureNow { get; set; }
        long ClientId { get; set; }
        long PaymentMethod { get; set; }
        long TotalAmount { get; set; }
        long TotalAmountNoTax { get; set; }
        long TotalTax { get; set; }
    }
}
