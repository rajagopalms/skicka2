﻿using System;
using System.Collections.Generic;
using System.Linq;
using Posten.Portal.MyPages.BusinessEntities;

namespace Posten.Portal.MyPages.Translators
{
    internal static class OrderHistoryTranslator
    {
        internal static OrderHistoryModel ToBusinessEntity(this Services.Proxy.OrderHistoryService.OrderHistoryItem item)
        {
            return new OrderHistoryModel
            {
                OrderNumber = item.OrderNo,
                OrderDate = Convert.ToDateTime(item.OrderDate),
                OrderAmount = item.OrderAmount,
                Status = item.Status,
                StatusDescription = item.StatusDescription
            };
        }

        internal static ICollection<OrderHistoryModel> ToBusinessEntityCollection(this IEnumerable<Services.Proxy.OrderHistoryService.OrderHistoryItem> items)
        {
            return items.Select(item => item.ToBusinessEntity()).ToList();
        } 
    }
}
