﻿using System;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using Posten.Portal.Cart.BusinessEntities;
using Posten.Portal.Cart.Presentation.ApplicationPages;
using Posten.Portal.Cart.Utilities;
using Posten.Portal.Cart.Translators;
using Posten.Portal.Cart;
using System.Linq;

namespace Posten.Portal.Cart.Presentation.ControlTemplates
{
    using System.Web;
    using Posten.Portal.Platform.Common.Container;
    using Posten.Portal.Cart.Services;

    public abstract class CheckoutBase : UserControl
    {
        
        protected abstract string LayoutVersion { get; }

        protected abstract HiddenField BasketIdControl { get; }

        protected abstract bool IsOrganization { get; }

        protected abstract string BillingCountry { get; }

        protected abstract string SocialSecurityNumber { get; }

        protected abstract string FirstName { get; }

        protected abstract string LastName { get; }

        protected abstract IBillingAccountInfo BillingInfo { get; }

        protected abstract IDeliveryAccountInfo DeliveryInfo { get; }

        protected abstract int Layout { get; }

        protected abstract string GetCurrentUrl { get; }
        
        protected BasketContext BasketContext
        {
            get
            {
                return BasketContext.GetContext(new Guid(HttpContext.Current.Response.Cookies["PostenBasketId"].Value));//this.BasketIdControl.Value.ToGuid());
            }
        }

        protected abstract PaymentMethodType PaymentMethod { get; }

        protected abstract void ShowErrorMessage(string message);

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            // do not cache this page
            this.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            this.Response.Cache.SetNoStore();

            if (!IsPostBack)
            { 
                // reload the basketItem to get an existing basket-id
                this.BasketIdControl.Value = BasketContext.Current.Reload().BasketId.ToString();
            }
        }

        protected void ProcessCheckout(object sender, EventArgs e)
        {
            try
            {
                // get the basket-id from a hidden field, so we realy checkout the expected basket
                var basketContext = this.BasketContext;
                var paymentService = IoC.Resolve<IPaymentService>();
                var basketService = IoC.Resolve<IBasketService>();

                // only checkout if there is any products or services in the basket
                if (basketContext == null || basketContext.Reload().NumberOfArticle == 0)
                {
                    this.ShowErrorMessage(Utils.GetResourceString("CheckOut_Empty_Basket_Error_Message"));
                    return;
                }

                // check if the basket has a booking-id, redirect to the confirmation page if it does
                if (!string.IsNullOrEmpty(basketContext.BasketItem.BookingId))
                {
                    this.Response.Redirect(PaymentContext.GetConfirmationPageUrl(basketContext, this.LayoutVersion).ToString());
                    return;
                }

                bool billingResult = basketContext.AssignBillingInfo(this.BillingInfo);
                if (!billingResult)
                {
                    this.ShowErrorMessage(Utils.GetResourceString("Checkout_UnexpectedError_Text"));
                    return;
                }

                bool deliveryResult = basketContext.AssignDeliveryInfo(this.DeliveryInfo);
                if (!deliveryResult)
                {
                    this.ShowErrorMessage(Utils.GetResourceString("Checkout_UnexpectedError_Text"));
                    return;
                }

                var prepareResult = basketContext.PrepareForCheckout(this.Layout);
                if (!prepareResult)
                {
                    this.ShowErrorMessage(Utils.GetResourceString("Checkout_UnexpectedError_Text"));
                    return;
                }

                
                // redirect to the payment processing page only when it is non invoice
                if (PaymentMethod != PaymentMethodType.Invoice)
                {
                    if (basketContext.InvoiceFee != null)
                    {
                        basketContext.RemoveInvoiceFee();
                    }
                    Response.Redirect(PaymentContext.GetPaymentInitializeUrl(basketContext, this.LayoutVersion, this.PaymentMethod).ToString(), false);
                    Context.ApplicationInstance.CompleteRequest();
                }
                else
                {
                    //create a directpayment request.
                    var paymentrequest = basketContext.ToDirectPaymentRequest(PaymentMethod);

                    if (this.IsOrganization)
                    {
                        paymentrequest.BillingAddress = new CompanyLegalAddress()
                        {
                            City = basketContext.BasketItem.DeliveryAccountInfo.City,
                            Country = this.BillingCountry,
                            PostalCode = basketContext.BasketItem.DeliveryAccountInfo.PostalCode,
                            StreetAddress = basketContext.BasketItem.DeliveryAccountInfo.AddressLine,
                            CompanyName = basketContext.BasketItem.DeliveryAccountInfo.Organization,
                            FirstName = BasketContext.BasketItem.DeliveryAccountInfo.FirstName,
                            LastName = BasketContext.BasketItem.DeliveryAccountInfo.LastName
                        };
                    }
                    else
                        paymentrequest.BillingAddress = new PrivateLegalAddress()
                        {
                            City = basketContext.BasketItem.DeliveryAccountInfo.City,//basketContext.BasketItem.BillingAccountInfo.City,
                            Country = basketContext.BasketItem.DeliveryAccountInfo.CountryCode,
                            PostalCode = basketContext.BasketItem.DeliveryAccountInfo.PostalCode,
                            StreetAddress = basketContext.BasketItem.DeliveryAccountInfo.AddressLine,
                            FirstName = basketContext.BasketItem.DeliveryAccountInfo.FirstName,//this.FirstName, 
                            LastName = basketContext.BasketItem.DeliveryAccountInfo.LastName//this.LastName 
                        };

                    paymentrequest.Customer = new Customer()
                    {
                        Email = this.BasketContext.BasketItem.BillingAccountInfo.Email,
                        IpAddress = Utils.GetCurrentIpAddress(),
                        Phone = BasketContext.BasketItem.DeliveryAccountInfo.Telephone,
                        SocialSecurityNumber = this.SocialSecurityNumber
                    };

                    paymentrequest.LegalAddress = paymentrequest.BillingAddress;

                    //call direct payment in payment service
                    var paymentresult = paymentService.DirectPayment(paymentrequest);

                    //Initialize BasketService
                    if (!basketService.InitialisePayment(this.BasketContext.BasketId.ToString(), paymentresult.DeliveryIdMappings.Select(item => item.DeliveryId).ToArray(), paymentresult.TransactionId))
                    {
                        this.ShowErrorMessage(Utils.GetResourceString("Checkout_UnexpectedError_Text"));
                        return;
                    };

                    //Redirect to Finalize                
                    Response.Redirect(PaymentContext.GetFinalizePaymentUrlForInvoice(BasketContext, LayoutVersion, PaymentMethod).ToString(), false);
                    Context.ApplicationInstance.CompleteRequest();
                }
            }
            catch (ThreadAbortException)
            {
                // Response.Redirect throws ThreadAbortException
                throw;
            }
            catch (Exception ex)
            {
                // TODO: log error
                ex.RecursiveLog();
                this.ShowErrorMessage(ex.Message);
                
                return;
            }
        }
    }
}
