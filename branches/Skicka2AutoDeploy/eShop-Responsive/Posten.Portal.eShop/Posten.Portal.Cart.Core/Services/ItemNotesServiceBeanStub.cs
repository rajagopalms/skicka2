﻿namespace Posten.Portal.Cart.Services
{
    using Posten.Portal.Cart.Services.Proxy.ItemNotesServiceBean;

    public class ItemNotesServiceBeanStub : IItemNotesServiceBean
    {
        #region Methods

        public void InformPaymentComplete(string[] orderNumbers, status status)
        {
        }

        public void InformPaymentPending(string buyerEMail, string[] orderNumbers, string transactionId)
        {
        }

        #endregion Methods
    }
}