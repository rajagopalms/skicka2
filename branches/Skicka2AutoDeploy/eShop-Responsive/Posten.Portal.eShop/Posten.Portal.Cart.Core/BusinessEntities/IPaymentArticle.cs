﻿using System;

namespace Posten.Portal.Cart.BusinessEntities
{
    public interface IPaymentArticle
    {
        string ArticleDescription { get; set; }

        string ArticleId { get; set; }

        string ArticleName { get; set; }

        long ArticlePrice { get; set; }

        string CompanyCode { get; set; }

        string CustomerId { get; set; }

        string FootnoteText { get; set; }

        string Freetext { get; set; }

        string OrderNumber { get; set; }

        long Quantity { get; set; }

        int QuantityDecimals { get; set; }

        string ReferenceText { get; set; }

        long Tax { get; set; }

        string TaxCountry { get; set; }

        int TaxRate { get; set; }

        long TotalPrice { get; set; }

        long TotalPriceVatIncluded { get; set; }

        string TaxCode { get; set; }
    }
}
