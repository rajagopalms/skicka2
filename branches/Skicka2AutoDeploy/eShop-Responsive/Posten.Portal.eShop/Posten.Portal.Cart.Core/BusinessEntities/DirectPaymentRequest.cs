﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Posten.Portal.Cart.BusinessEntities
{
    public class DirectPaymentRequest : PaymentRequestBase, IDirectPaymentRequest
    {

        #region IDirectPaymentRequest Members

        public ILegalAddress LegalAddress {get;set;}

        public ILegalAddress BillingAddress { get; set; }
        
        public ICustomer Customer {get;set;}

        public string Currency { get; set; }

        public string Language { get; set; }
        
        #endregion
    }
}
