﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Register Tagprefix="SharePointWebControls" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="PublishingWebControls" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="PublishingNavigation" Namespace="Microsoft.SharePoint.Publishing.Navigation" Assembly="Microsoft.SharePoint.Publishing, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="PostenWebControls" Namespace="Posten.Portal.Platform.Common.Presentation.Controls" Assembly="Posten.Portal.Platform.Common.Presentation, Version=3.0.0.0, Culture=neutral, PublicKeyToken=9f4da00116c38ec5" %>
<%@ Register Tagprefix="MyPagesWebControls" Namespace="Posten.Portal.MyPages.Presentation.CustomControls" Assembly="$SharePoint.Project.AssemblyFullName$" %>
<%@ Page language="C#" Inherits="Posten.Portal.MyPages.Presentation.PageLayouts.MyPagesLayoutPage" %>

<asp:Content ContentPlaceholderID="PlaceHolderAdditionalPageHead" runat="server">
	<SharePointWebControls:CssRegistration name="<% $SPUrl:~sitecollection/Style Library/~language/Core Styles/page-layouts-21.css %>" runat="server"/>
	<PublishingWebControls:EditModePanel runat="server">
		<!-- Styles for edit mode only-->
		<SharePointWebControls:CssRegistration name="<% $SPUrl:~sitecollection/Style Library/~language/Core Styles/edit-mode-21.css %>"
			After="<% $SPUrl:~sitecollection/Style Library/~language/Core Styles/page-layouts-21.css %>" runat="server"/>
	</PublishingWebControls:EditModePanel>
	<SharePointWebControls:CssRegistration name="<% $SPUrl:~sitecollection/Style Library/~language/Core Styles/rca.css %>" runat="server"/>
	<SharePointWebControls:FieldValue id="PageStylesField" FieldName="HeaderStyleDefinitions" runat="server"/>
</asp:Content>
<asp:Content ContentPlaceholderID="PlaceHolderPageTitle" runat="server">
	<SharePointWebControls:FieldValue id="PageTitle" FieldName="Title" runat="server"/>
</asp:Content>
<asp:Content ContentPlaceholderID="PlaceHolderPageTitleInTitleArea" runat="server">
	<SharePointWebControls:FieldValue FieldName="Title" runat="server"/>
</asp:Content>
<asp:Content ContentPlaceHolderId="PlaceHolderTitleBreadcrumb" runat="server">
	<SharePointWebControls:ListSiteMapPath runat="server" SiteMapProviders="CurrentNavigation" RenderCurrentNodeAsLink="false" PathSeparator="" CssClass="s4-breadcrumb" NodeStyle-CssClass="s4-breadcrumbNode" CurrentNodeStyle-CssClass="s4-breadcrumbCurrentNode" RootNodeStyle-CssClass="s4-breadcrumbRootNode" NodeImageOffsetX=0 NodeImageOffsetY=353 NodeImageWidth=16 NodeImageHeight=16 NodeImageUrl="/_layouts/images/fgimg.png" HideInteriorRootNodes="true" SkipLinkText="" />
</asp:Content>
<asp:Content ContentPlaceholderID="PlaceHolderMain" runat="server">
	<div class="box colspan-4 page">
        <h1 class="title">
            <SharePointWebControls:TextField runat="server" FieldName="Title" />
        </h1>
        <PostenWebControls:HideEmptyField FieldName="PublishingPageImage" runat="server">
            <div class="captioned-image">
                <div class="image">
	                <PublishingWebControls:RichImageField FieldName="PublishingPageImage" runat="server"/>
                </div>
                <div class="caption">
	                <PublishingWebControls:RichHtmlField FieldName="PublishingImageCaption" AllowTextMarkup="false" AllowTables="false" AllowLists="false" AllowHeadings="false" AllowStyles="false" AllowFontColorsMenu="false" AllowParagraphFormatting="false" AllowFonts="false" AllowInsert="false" PreviewValueSize="Small" runat="server"/>
                </div>
            </div>
        </PostenWebControls:HideEmptyField>
        <PostenWebControls:HideEmptyField FieldName="Comments" runat="server">
            <div class="introduction">
                <SharePointWebControls:NoteField FieldName="Comments" runat="server"/>
            </div>
        </PostenWebControls:HideEmptyField>
        <div class="webpartzone">
            <WebPartPages:WebPartZone runat="server" ID="TopZone" AllowPersonalization="false" PartChromeType="None" Title="<%$Resources:cms,WebPartZoneTitle_Top%>" />
        </div>
        <div class="banner" style="margin-top: 6px; margin-bottom: 16px;"><img src="/_layouts/images/posten/mypages/sample/sample-mypages-banner.jpg" /></div>
        <div class="box colspan-2 webpartzone">
            <WebPartPages:WebPartZone runat="server" ID="LeftColumnZone" AllowPersonalization="false" PartChromeType="None" Title="<%$Resources:cms,WebPartZoneTitle_LeftColumn%>" />
        </div>
        <div class="box colspan-2 webpartzone">
            <WebPartPages:WebPartZone runat="server" ID="RightColumnZone" AllowPersonalization="false" PartChromeType="None" Title="<%$Resources:cms,WebPartZoneTitle_RightColumn%>" />
            <PostenWebControls:ModuleControl runat="server" ModuleStyle="Theme" HeaderText='<%$Resources:PostenMyPages,Page_Home_PostenInforms_Text %>'>
                <div class="headline"><MyPagesWebControls:LoginName FormatString="Welcome, {0}!" runat="server"/></div>
                <PostenWebControls:HideEmptyField FieldName="PublishingPageContent" runat="server">
                    <div class="content">
                        <PublishingWebControls:RichHtmlField FieldName="PublishingPageContent" HasInitialFocus="True" AllowFonts="false" AllowTextMarkup="false" AllowParagraphFormatting="false" PrefixStyleSheet="posten-" MinimumEditHeight="400px" runat="server"/>
                    </div>
                </PostenWebControls:HideEmptyField>
            </PostenWebControls:ModuleControl>
        </div>
    </div>
</asp:Content>
