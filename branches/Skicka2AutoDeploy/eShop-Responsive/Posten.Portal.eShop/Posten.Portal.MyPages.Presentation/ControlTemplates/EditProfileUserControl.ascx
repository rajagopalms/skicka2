﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="PostenWebControls" Namespace="Posten.Portal.Platform.Common.Presentation.Controls" Assembly="Posten.Portal.Platform.Common.Presentation, Version=3.0.0.0, Culture=neutral, PublicKeyToken=9f4da00116c38ec5" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditProfileUserControl.ascx.cs" Inherits="Posten.Portal.MyPages.Presentation.ControlTemplates.EditProfileUserControl" %>

<script src="/_layouts/Posten/Cart/Presentation/js/common/utils.js" type="text/javascript"></script>
<div class="module myPagesProfile head-common noBorder">
	<div class="edit-profile">       
        <div style="width: 100%;"> 
            <PostenWebControls:ModuleControl ID="CustomerInformationHeader" HeaderText="<%$Resources:PostenMyPages,Edit_Profile_Customer_Information_Title %>" runat="server">
            <table width="100%">
            <tr>
                <td>
                <div class="information-label">
                    <asp:Label ID="PersonalInformationLabel" CssClass="basic-info" runat="server" Text="Personal Information"></asp:Label>
                    <asp:Label ID="AdressDisclaimer" CssClass="address-disclaimer" runat="server" Text="Note that the address change takes effect only on future orders"></asp:Label>
                </div> 
                </td>
            </tr>
            <tr>
                <td>
                    <div class="radio-button">
                        <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Selected="True" Text='Private person' class="private"></asp:ListItem>
                        <asp:ListItem Text='Company' class="company"></asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </td>
            </tr>
            <tr class="civic-section">
                <td>
                    <div class="civic-label">
                        <label for=""><asp:Literal runat="server" ID="CivicLabel2" Text='Civic' />:</label></div>
                    <div class="civic-tbox">
                        <asp:TextBox ID="txtValidate" runat="server" Text=""></asp:TextBox>
                        <div class="instruction">
                        <asp:Literal runat="server" ID="CustDataInstructionLabel" Text='happened your data from the population register.' /></div></div> 
                    <div class="btn-retrieve">
                        <PostenWebControls:PostenButton ID="btnRetrieveAddress" runat="server" Text='Get Address' /></div>
                </td>
            </tr>
            <tr class="customer-details">
                <td>
                    <div class="firstname">
                        <label for="">
                            <asp:Literal runat="server" ID="FirstNameLabel" Text='FirstName' />:&nbsp;*</label>
                        <asp:TextBox ID="txtFirstName" runat="server" Text="" CssClass="firstname required"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="FirstNameRequired" runat="server" ControlToValidate="txtFirstName" EnableClientScript="true" />
                    </div>
                    <div class="lastname">
                        <label for="">
                            <asp:Literal runat="server" ID="SurNameLabel" Text='SurName' />:&nbsp;*</label>
                        <asp:TextBox ID="txtLastName" runat="server" Text="" CssClass="lastname required"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="LastNameRequired" runat="server" ControlToValidate="txtLastName" EnableClientScript="true" />
                    </div>
                    <div class="address">
                        <label for="">
                            <asp:Literal runat="server" ID="AddressLabel" Text='Address' />:&nbsp;*</label>
                            <asp:TextBox ID="txtAddress" runat="server" CssClass="address required"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="AddressRequired" runat="server" ControlToValidate="txtAddress" EnableClientScript="true" />
                    </div>
                    <div class="postalandcity">
                        <label for="">
                            <asp:Literal runat="server" ID="PostalLabelAndCity" Text='Postal & City' />:&nbsp;*</label>
                            <asp:TextBox ID="txtPostalCode" runat="server" CssClass="postalcode required"></asp:TextBox>
                            <asp:TextBox ID="txtCity" runat="server" CssClass="city required enablestate"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="PostalCodeRequired" runat="server" ControlToValidate="txtPostalCode" EnableClientScript="true" />
                            <asp:RequiredFieldValidator ID="CityRequired" runat="server" ControlToValidate="txtCity" EnableClientScript="true" />
                    </div>
                    <div class="country">
                        <label for="">
                            <asp:Literal runat="server" ID="CountryLabel" Text='Country' />:&nbsp;*</label>
                        <asp:DropDownList ID="ddlstCountry" runat="server" CssClass="countryitem required">
                            <asp:ListItem Value="SE" Selected="True">Sverige</asp:ListItem>
                            <asp:ListItem Value="DK">Danmark</asp:ListItem>
                            <asp:ListItem Value="NO">Norge</asp:ListItem>
                            <asp:ListItem Value="DE">Tyskland</asp:ListItem></asp:DropDownList>
                    </div>
                    <div class="mobilenumber">
                        <label for="">
                            <asp:Literal runat="server" ID="MobileNoLabel1" Text='Mobile Number' />:</label>
                            <div>
                                <asp:TextBox ID="txtMobileNumber" CssClass="mobile" runat="server"></asp:TextBox>
                                <div class="disclaimer">
                                    <asp:Literal runat="server" ID="MobileDisclaimerLabel" Text='Mobile number required for SMS notification, provide faster delivery.' /></div>
                                </div>
                    </div>
                </td>
            </tr>
            </table>            
            </PostenWebControls:ModuleControl>

            <PostenWebControls:ModuleControl ID="EmailHeader" HeaderText="<%$Resources:PostenMyPages,Edit_Profile_Change_Email_Title %>" runat="server">
            <table width="100%">
                <tr class="email-section">
                    <td>
                        <div class="email">
                            <label for=""><asp:Literal runat="server" ID="YourEmailLabel" Text='Email Address' />:</label>
                            <asp:TextBox ID="txtEmail" runat="server" Text="" CssClass="required email"></asp:TextBox>
                        </div>
                        <div class="confirm-email">
                            <label>
                                <asp:Literal runat="server" ID="ConfirmEmailLabel" Text='Confirm Email Address' />:&nbsp;</label>
                            <span>
                                <asp:TextBox ID="txtConfirmEmail" runat="server" Text="" TextMode="Password" CssClass="required email"></asp:TextBox>
                                <asp:CompareValidator ID="EmailCompareValidator" ControlToValidate="txtEmail" ControlToCompare="txtConfirmEmail" runat="server" EnableClientScript="true" Text="Email did not match" />
                                <asp:RegularExpressionValidator ID="ConfirmEmailValidation" runat="server" EnableClientScript="true" ControlToValidate="txtConfirmEmail" ValidationExpression="" Text="Invalid Email Address" /></span>
                        </div>
                    </td>
                </tr>
            </table>
            </PostenWebControls:ModuleControl>

            <PostenWebControls:ModuleControl ID="PasswordHeader" HeaderText="<%$Resources:PostenMyPages,Edit_Profile_Change_Password_Title %>" runat="server">
                <table width="100%">
                    <tr class="password-section">
                        <td>
                            <div id="CurrentPassword" class="current-pword" runat="server">
                                <div class="current-pword-lbl">
                                    <asp:Literal runat="server" ID="CurrentPasswordLabel" Text='Current Password' />:</div>
                                <div class="current-pword-tbox">
                                    <asp:TextBox ID="txtCurrentPassword" runat="server" TextMode="Password"></asp:TextBox></div>
                            </div>
                            <div id="NewPassword" class="new-pword" runat="server">
                                <div class="new-pword-lbl">
                                    <asp:Literal runat="server" ID="NewPasswordLabel" Text='New Password' />:</div>
                                <div class="new-pword-tbox">
                                    <asp:TextBox ID="txtNewPassword" runat="server" TextMode="Password"></asp:TextBox></div>
                            </div>
                            <div id="ConfirmPassword" class="confirm-pword" runat="server">
                                <div class="confirm-pword-lbl">
                                    <asp:Literal runat="server" ID="ConfirmNewPasswordLabel" Text='Confirm New Password' />:</div>
                                <div class="confirm-pword-tbox">
                                    <asp:TextBox ID="txtConfirmNewPassword" runat="server" TextMode="Password"></asp:TextBox>
                                    <asp:CompareValidator ID="PasswordValidator" ControlToValidate="txtNewPassword" ControlToCompare="txtConfirmNewPassword" runat="server" EnableClientScript="true" Text="New Password did not match" /></div>
                            </div>
                        </td>
                    </tr>
                </table>
            </PostenWebControls:ModuleControl>

            <div class="save-section">                    
                <span>
                    <asp:Literal ID="PersonalActLabel" runat="server" Text="For more information on the Personal Data Act (PUL)"></asp:Literal></span>
                <div class="save-details-btn">                                
                    <span><PostenWebControls:PostenButton ID="btnSave" runat="server" Text='Save changes' CssClass="btn-Save validatesavedetails" /></span></div>
            </div>
        </div>       
	</div>
</div>
<script type="text/javascript" language="javascript">
    var ziplength = 5;

    $(".postalcode").change(function () { getCity(); });
    $(".country").change(function () { getCity(); });

    function getCity() {
        if ($(".postalcode").val().length <= ziplength && $(".postalcode").val() != "" && $(".countryitem").val() != "") {
            $.ajax({
                type: "POST",
                url: "/_vti_bin/Posten/Cart/UIOperation.svc/GetLocation",
                data: '{"postalcode":"' + $(".postalcode").val() + '","countrycode":"' + $(".countryitem").val() + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg != null) {
                        $(".city").val(msg.city);
                        $(".countryitem").val(msg.country);
                        $(".postalcode").val(msg.zip);
                    }
                    else { //$(".city").val('');                         
                        showPopUp('Update could not be processed.', 'Postal Code or Country does not exist', 'popup', false, 800, "Close");
                    }
                },
                error: function errorFunction() {
                    //$(".city").val('');                    
                    showPopUp('Update could not be processed.', 'Postal Code or Country does not exist', 'popup', false, 800, "Close");
                }
            });
        } 
     }
</script>