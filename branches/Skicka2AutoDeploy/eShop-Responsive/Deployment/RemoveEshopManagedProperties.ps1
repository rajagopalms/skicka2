
if ((Get-PSSnapin | where {$_.Name -eq "Microsoft.SharePoint.PowerShell"}) -eq $null) {
	Write-Host "Loading SharePoint Powershell snapin..." -NoNewline
	Add-PSSnapin Microsoft.SharePoint.PowerShell;
	Write-Host "DONE."
}

$SourcePath ="EshopManagedProperties.xml"

function RemoveMapping
([string]$managedpropertyname)
{


	$managedproperty = Get-SPEnterpriseSearchMetadataManagedProperty -SearchApplication $searchapp -Identity $managedpropertyname  -ErrorAction silentlycontinue
	Write-Host "Removing all mapping for :$managedpropertyname . . ." -NoNewLine
	if ($managedproperty)
	{
		foreach($mapping in $managedproperty.GetMappings())
		{
			$mapping |Remove-SPEnterpriseSearchMetadataMapping 
		}
		
		$managedproperty | Remove-SPEnterpriseSearchMetadataManagedProperty 
		
	}
	Write-Host "Done" 
	
}

function RemovedCrawledProperties
{
	Write-Host "Removing all crawled Properties..." -NoNewLine

	$schema = New-Object Microsoft.Office.Server.Search.Administration.Schema ľArgumentList $searchapp
	$category = $schema.AllCategories["Business Data"];
	$category.DeleteUnmappedProperties();
	$category.Update();  
	Write-Host "Done"
}

function SetNotIndexable([string]$crawledpropertyname)
{
	Write-Host "Setting crawled Property" + $crawledpropertyname +" to not be included in index..." -NoNewLine

	$crawledproperty = Get-SPEnterpriseSearchMetadataCrawledProperty -Name $crawledpropertyname -SearchApplication $searchapp
	if($crawledproperty)
	{
		$crawledproperty.IsMappedToContents =$false
		$crawledproperty.Update()
	}
	Write-Host "Done"
}

Write-Host "Loading Source XML"
$xmldata = new-object "System.Xml.XmlDocument"		
$xmldata.LoadXml((get-content $SourcePath))		


$SearchAppnode = $xmldata.SelectSingleNode("SearchApplication")
$searchapp = Get-SPEnterpriseSearchServiceApplication $SearchAppnode.Name


$Metadatanodes = $xmldata.SelectNodes("SearchApplication/ManagedProperties/Mapping")
foreach($node in $Metadatanodes)
{	
	Write-Host "Removing ManagedProperty: " $node.ManagedProperty -NoNewline
	Write-Host "using crawledproperty: " $node.CrawledProperty
	RemoveMapping $node.ManagedProperty 
}

$CrawledPropertyNodes = $xmldata.SelectNodes("SearchApplication/CrawledProperties/Property")
foreach($node in $CrawledPropertyNodes)
{	
	Write-Host "Removing crawledproperty: " $node.Name -NoNewline
	Write-Host "from index " 
	SetNotIndexable $node.Name
}


RemovedCrawledProperties

Write-Host "ManagedProperties Deletion is completed."