
Write-Host "Loading Powershell addin ..." -nonewline
Add-PSSnapin Microsoft.SharePoint.PowerShell
Write-host "done"

#read variables from EshopManagedProperties.xml
Write-Host "Loading Source XML"
$xmldata = new-object "System.Xml.XmlDocument"		
$xmldata.LoadXml((get-content "EshopManagedProperties.xml"))		

$SearchAppnode = $xmldata.SelectSingleNode("SearchApplication")


$searchconfigurationnode = $xmldata.SelectSingleNode("SearchApplication/SearchConfiguration")
$applicationMapping = $searchconfigurationnode.ApplicationUrlMapping;

# Set the farm properties that will be needed by the application
$farm = Get-SPFarm
write-host "Setting Farm properties..." -nonewline
$farm.Properties["CatalogApplicationUrlMappingKey"] = $applicationMapping
$farm.update()
write-host "done"

