try {
		$path = "Local.config"
		$global:settings = @{}
		$config = [xml](Get-Content $path)
		foreach ($addNode in $config.configuration.settings.add) {
			$global:settings[$addNode.Key] = $addNode.Value
		}
	}
	catch {
		echo "Error loading the config file" -F Red
		throw
	}

