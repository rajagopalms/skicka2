.\LoadConfig.ps1

$SiteUrl = $settings["siteUrl"]
$SiteAdministrator = $settings["siteAdministrator"]
$SiteTitle = $settings["siteTitle"]
$SiteLanguage = $settings["language"]
$WebApplicationUrl = $settings["webApplicationUrl"]

#------ EShop-Responsive ------#

#Activate Features in the right order
echo "----------------------------------------------------------------------------"
echo "Activating Feature: PostenCommon_CommonConfig_3.0"
echo "----------------------------------------------------------------------------"
Enable-SPFeature 600ffcc9-c63d-4f34-a1e1-334871742411 -url $SiteUrl 

echo "----------------------------------------------------------------------------"
echo "Activating Feature: Posten Common Components 3.0 Presentation Web.config Settings"
echo "----------------------------------------------------------------------------"
Enable-SPFeature b664b3ea-a36a-4f55-b5a2-db48cdce24b1 -url $SiteUrl 