﻿namespace Posten.SkickaDirekt2.WebParts
{
    using System.ComponentModel;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls.WebParts;

    using Microsoft.SharePoint;
    using Microsoft.SharePoint.WebPartPages;

    using Posten.SkickaDirekt2.Resources;
    using Posten.SkickaDirekt2.WebParts.UserControls;

    [ToolboxItemAttribute(false)]
    public class AddToCartWP : System.Web.UI.WebControls.WebParts.WebPart
    {
        #region Fields

        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/Posten.SkickaDirekt2.WebParts.UserControls/AddToCartWP/AddToCartWPUserControl.ascx";

        private string accountValidationProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_AddToCart_AccountValidation") as string;
        private string addToCartButtonProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_AddToCart_AddToCartButton") as string;
        private string receiverValidationProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_AddToCart_ReceiverValidation") as string;
        private string senderValidationProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_AddToCart_SenderValidation") as string;

        //One-way Properties
        private string confirmationUrlProperty = Constants.ConfirmationDownloadLink;
        private string waybillUrlProperty = Constants.WaybillDownloadLink;
        private string confirmationPageUrlProperty = Constants.ConfirmationPageUrl;
        private string applicationUrlProperty = "";
        private string editUrlProperty = Constants.EditUrl;
        private string produceUrlProperty = Constants.ProduceUrl;
        
        #endregion Fields

        #region Properties
        [WebBrowsable(true),
        WebDisplayName("Confirmation Download Url:"),
        WebDescription("Confirmation Download Url."),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string ConfirmationUrlProperty
        {
            get
            {
                return confirmationUrlProperty;
            }
            set
            {
                confirmationUrlProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Waybill Download Url:"),
        WebDescription("Waybill Download Url."),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string WaybillUrlProperty
        {
            get
            {
                return waybillUrlProperty;
            }
            set
            {
                waybillUrlProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Confirmation Page Url:"),
        WebDescription("Confirmation Page Url."),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string ConfirmationPageUrlProperty
        {
            get
            {
                return confirmationPageUrlProperty;
            }
            set
            {
                confirmationPageUrlProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Application Url:"),
        WebDescription("Application Url."),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string ApplicationUrlProperty
        {
            get
            {
                return applicationUrlProperty;
            }
            set
            {
                applicationUrlProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Edit Url:"),
        WebDescription("Edit Url."),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string EditUrlProperty
        {
            get
            {
                return editUrlProperty;
            }
            set
            {
                editUrlProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Produce Url:"),
        WebDescription("Produce Url."),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string ProduceUrlProperty
        {
            get
            {
                return produceUrlProperty;
            }
            set
            {
                produceUrlProperty = value;
            }
        }

        //*************************************************************


        [WebBrowsable(true),
        WebDisplayName("Account validation message:"),
        WebDescription("Validation message for account section."),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string AccountValidationProperty
        {
            get
            {
                return accountValidationProperty;
            }
            set
            {
                accountValidationProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Add To Cart button:"),
        WebDescription("Caption for button Add To Cart"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string AddToCartButtonProperty
        {
            get
            {
                return addToCartButtonProperty;
            }
            set
            {
                addToCartButtonProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Receiver validation message:"),
        WebDescription("Validation message for receiver section."),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string ReceiverValidationProperty
        {
            get
            {
                return receiverValidationProperty;
            }
            set
            {
                receiverValidationProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Sender validation message:"),
        WebDescription("Validation message for sender section."),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string SenderValidationProperty
        {
            get
            {
                return senderValidationProperty;
            }
            set
            {
                senderValidationProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        public void SaveProperties()
        {
            SPWeb web = SPContext.Current.Web;
            SPFile file = web.GetFile(HttpContext.Current.Request.Url.ToString());
            using (SPLimitedWebPartManager manager = file.GetLimitedWebPartManager(PersonalizationScope.Shared))
            {
                AddToCartWP webPart = (AddToCartWP)manager.WebParts[this.ID];

                webPart.AddToCartButtonProperty = this.addToCartButtonProperty;
                webPart.AccountValidationProperty = this.accountValidationProperty;
                webPart.SenderValidationProperty = this.senderValidationProperty;
                webPart.ReceiverValidationProperty = this.receiverValidationProperty;

                web.AllowUnsafeUpdates = true;
                manager.SaveChanges(webPart);
                web.AllowUnsafeUpdates = false;
                manager.Web.Dispose();
            }
        }

        protected override void CreateChildControls()
        {
            AddToCartWPUserControl control = (AddToCartWPUserControl)Page.LoadControl(_ascxPath);
            control.ParentWebPart = this;
            Controls.Add(control);
        }

        #endregion Methods
    }
}