﻿namespace Posten.Portal.Skicka.Services.BusinessEntities
{
    using System;
    using System.Collections.Generic;

    using Posten.Portal.Skicka.Services.BusinessEntities.CartEntities;

    public class ServiceOrderItem
    {
        #region Properties

        //public ServiceOrderItem();
        public string ApplicationId
        {
            get;
            set;
        }

        public string ApplicationName
        {
            get;
            set;
        }

        public string BasketId
        {
            get;
            set;
        }

        public Uri ConfirmationUrl
        {
            get;
            set;
        }

        public string CurrencyCode
        {
            get;
            set;
        }

        public Uri CustomApplicationLink
        {
            get;
            set;
        }

        public string CustomApplicationLinkText
        {
            get;
            set;
        }

        public string Details
        {
            get;
            set;
        }

        public string EditUrl
        {
            get;
            set;
        }

        public string FreeTextField1
        {
            get;
            set;
        }

        public string FreeTextField2
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string OrderNumber
        {
            get;
            set;
        }

        public Uri ProduceUrl
        {
            get;
            set;
        }

        public Uri ServiceImageUrl
        {
            get;
            set;
        }

        public ICollection<ServiceOrderLineItem> ServiceOrderLines
        {
            get;
            set;
        }

        public decimal SubTotal
        {
            get;
            set;
        }

        public decimal Vat
        {
            get;
            set;
        }

        #endregion Properties
    }
}