﻿namespace Posten.Portal.Skicka.Services.BusinessEntities
{
    using System;
    using System.Linq;
    using System.Runtime.Serialization;

    using Posten.Portal.Skicka.PVT.Proxy;
    using Posten.Portal.Skicka.Services.Utils;

    [Serializable]
    [DataContract]
    public class BaseBE
    {
        #region Constructors

        public BaseBE(responseStatus status, string statusMsg, responseInfo[] responseInfos)
        {
            this.Status = (ResponseStatus)Enum.Parse(typeof(ResponseStatus), Enum.GetName(typeof(responseStatus), status));
            this.StatusMsg = statusMsg;

            if (responseInfos != null)
            {
                this.ResponseInfos = Array.ConvertAll(
                          responseInfos,
                          r => this.TranslateResponseInfoToResponseInfoBE(r));

                this.ResponseInfos = this.ResponseInfos.Where(o => o != null).ToArray();
            }
        }

        public BaseBE()
        {
        }

        #endregion Constructors

        #region Enumerations

        public enum ResponseStatus
        {
            SUCCESS,
            INVALID_INPUT,
            FUNCTIONAL_ERROR_FROM_EXTERNAL_SYSTEM,
            INTERNAL_ERROR,
            GENERAL_FUNCTIONAL_ERROR,
            DB_ERROR,
        }

        #endregion Enumerations

        #region Properties

        [DataMember]
        public ResponseInfoBE[] ResponseInfos
        {
            get;
            set;
        }

        [DataMember]
        public ResponseStatus Status
        {
            get;
            set;
        }

        [DataMember]
        public string StatusMsg
        {
            get;
            set;
        }

        #endregion Properties

        #region Methods

        private ResponseInfoBE TranslateResponseInfoToResponseInfoBE(responseInfo responseInfo)
        {
            try
            {
                return new ResponseInfoBE(responseInfo);
            }
            catch (Exception ex)
            {
                LogUtility.LogCriticalException(ex, this.GetType(), System.Reflection.MethodBase.GetCurrentMethod().Name);

                return null;
            }
        }

        #endregion Methods
    }
}