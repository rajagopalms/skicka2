﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ServicesResultItemWPEdit.ascx.cs" Inherits="Posten.SkickaDirekt2.CustomControls.ServicesResultItemWPEdit" %>

<div id="ServicesResultItemEdit">
    <div class="borders rounded bottomMargin">
        <div class="paddings lightGradient header bottomBorder topRounded">
            <input id="headerPropertyEdit" runat="server" type="text" enableviewstate="true" />
        </div>
        <div class="bottomBorder">
            <div class="paddings relativePosition">
                <div class="leftFloat">
                    <div class="resultItemHeader bottomMargin">Brev Skicka Lätt</div>
                    <div class="resultItemOptions bottomMargin">
                        <div class="smallBottomMargin">
                            <input id="additionsHeaderPropertyEdit" runat="server" type="text" enableviewstate="true" class="inputEdit" />
                            <span class="circle tooltip">
                                <div class="buttonGradient insideCircle bold">?</div>
                            </span>
                            <br />
                            <textarea class="textareaEdit" rows="3" cols="20" id="additionsTooltipPropertyEdit" runat="server" enableviewstate="true" />
                        </div>
                        <div class="bothClear"></div>
                        <div class="leftFloat rightPadding resultItemOptionEdit">
                            <input type="checkbox" />Postförskott
                        </div>
                        <div class="bothClear"></div>
                    </div>
                    <ul class="resultItemProperties bottomMargin">
                        <li>Spårbart</li>
                    </ul>
                    <input id="morePropertiesLinkPropertyEdit" runat="server" type="text" enableviewstate="true" />
                    <a href="#" onclick="javascript: return false" class="blueLink bold resultShowMoreClosedEdit"></a>
                </div>
                <div class="rightFloat leftPadding">
                    <div class="rightText">
                        <span class="resultItemPrice">54 kr</span><br />
                        <span id="EUPriceVat" runat="server"></span>
                        <input id="vatTextValueEdit" runat="server" type="text" />
                        <span id="nonEUPriceVat" runat="server"></span>
                        <input id="noVatTextValueEdit" runat="server" type="text" />
                        <br />
                        <div class="buttonContainer topMargin bottomMargin largeButton resultItemButtonEdit centerText rounded buttonGradient">
                            <input id="wayBillPropertyEdit" runat="server" type="text" enableviewstate="true" class="inputEdit" />
                        </div>
                        <br />
                        <input id="buyOnlinePropertyEdit" runat="server" type="text" />
                    </div>
                </div>
                <div class="bothClear"></div>
            </div>
        </div>
        <div class="paddings lightGradient bottomRounded rightText">
            <input class="inputEdit" id="moreProposalsLinkPropertyEdit" runat="server" type="text" enableviewstate="true" />
        </div>
    </div>
</div>