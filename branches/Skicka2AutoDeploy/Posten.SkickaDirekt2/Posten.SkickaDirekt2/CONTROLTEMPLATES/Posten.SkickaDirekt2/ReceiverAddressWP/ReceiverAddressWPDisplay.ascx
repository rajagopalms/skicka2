﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReceiverAddressWPDisplay.ascx.cs" Inherits="Posten.SkickaDirekt2.CustomControls.ReceiverAddressWPDisplay" %>

<SharePoint:ScriptLink runat="server" ID="ScriptLink1" Name="/Posten.SkickaDirekt2/JS/ReceiverAddressWP.js" Localizable="false" LoadAfterUI="true"></SharePoint:ScriptLink>

<div id="ReceiverAddressDisplay" style="display: none" data-bind="visible: true, if: SelectedProductSuggestion()">
    <div class="borders rounded bottomMargin">
        <div class="paddings lightGradient header bottomBorder topRounded">
            <span runat="server" id="headerPropertyDisplay"></span>
        </div>
        <div class="paddings" data-bind="with: ReceiverAddress">
            <div class="leftPart"><span runat="server" id="receiverTypeLabelPropertyDisplay"></span></div>
            <div class="rightPart rightPartText">
                <input type="radio" name="receiverType" data-bind="checked: IsCompany, checkedValue: false" /><label for="receiverTypePrivate" class="rightMargin"><span runat="server" id="privateLabelPropertyDisplay"></span></label>
                <input type="radio" name="receiverType" data-bind="checked: IsCompany, checkedValue: true" class="leftMargin" /><label for="receiverTypeCompany"><span runat="server" id="companyLabelPropertyDisplay"></span></label>
            </div>
            <div class="bothClear bottomMargin"></div>

            <div data-bind="visible: IsCompany()" class="bottomMargin">
                <div class="leftPart">
                    <span runat="server" id="companyNameLabelPropertyDisplay"></span>
                    <span data-bind="if: IsCompany()">*</span>
                </div>
                <div class="rightPart">
                    <input type="text" class="field largestField" id="receiverCompanyName" maxlength="30" data-bind="value: CompanyName" />
                </div>
                <div class="bothClear"></div>
            </div>

            <div id="receiverNameDisplay" class="bottomMargin">
                <div class="leftPart">
                    <span runat="server" id="nameLabelPropertyDisplay"></span>
                    <span data-bind="if: !IsCompany()">*</span>
                </div>
                <div class="rightPart">
                    <input type="text" class="field largestField" id="receiverName" maxlength="30" data-bind="value: Name" />
                </div>
                <div class="bothClear"></div>
            </div>

            <div id="receiverAddressDisplay" class="bottomMargin">
                <div class="leftPart">
                    <span runat="server" id="addressLabelPropertyDisplay"></span>
                    <span data-bind="if: !IsCompany()">*</span>
                </div>
                <div class="rightPart">
                    <input type="text" class="field largestField" id="ReceiverAddress" maxlength="30" data-bind="value: AddressField1" />
                </div>
                <div class="bothClear"></div>
            </div>

            <div id="receiverAddress2Display" class="bottomMargin">
                <div class="leftPart">
                    <span runat="server" id="address2LabelPropertyDisplay"></span>
                    <span class="circle tooltip">
                        <div class="buttonGradient insideCircle bold">?</div>
                        <span runat="server" id="address2TooltipPropertyDisplay"></span>
                    </span>
                </div>
                <div class="rightPart">
                    <input type="text" class="field largestField" id="receiverAddress2" maxlength="30" data-bind="value: AddressField2" />
                </div>
                <div class="bothClear"></div>
            </div>

            <div id="receiverZipCodeDisplay" class="bottomMargin">
                <div class="leftPart">
                    <span runat="server" id="zipLabelPropertyDisplay"></span>
                </div>
                <div class="rightPart">
                    <div class="leftFloat">
                        <input type="text" class="field smallField" id="receiverZipCode" data-bind="value: ZipCode" />
                    </div>
                    <div class="leftFloat relativePosition">
                        <span class="ajaxSmall ajaxLeft" id="receiverCityProgress" style="display: none;" data-bind="visible: $root.ShowReceiverZipProgress"></span>
                    </div>
                </div>
                <div class="bothClear">
                    <span>
                      <br />
                      <span class="errorText" data-bind="visible: $root.InvalidZipMsg">Ogiltigt postnummer</span>
                    </span>
                </div>
            </div>

            <div id="receiverCityDisplay" class="bottomMargin">
                <div class="leftPart">
                    <span runat="server" id="cityLabelPropertyDisplay"></span>
                </div>
                <div class="rightPart">
                    <div class="ajaxSmallRightContainer">
                        <div class="leftFloat" data-bind="if: $root.SelectedProductSuggestion().Product.International">
                            <input type="text" class="field mediumField" data-bind="if:!($root.InvalidZipMsg), value: City" />
                        </div>
                        <div class="leftFloat" data-bind="if: !$root.SelectedProductSuggestion().Product.International">
                            <input type="text" class="field mediumField" id="receiverCity" runat="server" data-bind="if:!($root.InvalidZipMsg), value: City" />
                        </div>
                    </div>
                </div>
                <div class="bothClear"></div>
            </div>

             <div id="receiverCountryDisplay" class="bottomMargin">
                <div class="leftPart">
                    <span runat="server" id="countryLabelPropertyDisplay">Land</span>
                </div>
                <div class="rightPart">
                    <div class="leftFloat" >
                        <input type="text" class="field mediumField" id="Text1" runat="server" disabled="disabled" data-bind="value: Country" />
                    </div>
                </div>
                <div class="bothClear"></div>
            </div>

            <div data-bind="if: $root.SelectedProductSuggestion().Product.ShowEmail()">
                <div id="receiverEmailDisplay" class="bottomMargin">
                    <div class="leftPart">
                        <span runat="server" id="emailLabelPropertyDisplay"></span><span data-bind="if: $root.SelectedProductSuggestion().Product.RequireEmail()">*</span>
                        <span class="circle tooltip">
                            <div class="buttonGradient insideCircle bold">?</div>
                            <span runat="server" id="emailTooltipPropertyDisplay"></span>
                        </span>
                    </div>
                    <div class="rightPart">
                        <input type="text" class="field largestField" id="receiverEmail" maxlength="63" data-bind="value: Email" />
                    </div>
                    <div class="bothClear"></div>
                </div>

                <div id="receiverEmailConfirmDisplay" class="bottomMargin">
                    <div class="leftPart">
                        <span runat="server" id="email2LabelPropertyDisplay"></span><span data-bind="if: $root.SelectedProductSuggestion().Product.RequireEmail()">*</span>
                    </div>
                    <div class="rightPart">
                        <input type="text" class="field largestField" id="receiverEmailConfirm" maxlength="63" data-bind="value: Email2" />
                    </div>
                    <div class="bothClear"></div>
                </div>
            </div>

            <div data-bind="if: $root.SelectedProductSuggestion().Product.ShowMobile()">
                <div id="receiverMobilePhoneDisplay" class="bottomMargin">
                    <div class="leftPart">
                        <span runat="server" id="mobilePhoneLabelPropertyDisplay"></span><span data-bind="if: $root.SelectedProductSuggestion().Product.RequireMobile()">*</span>
                        <span class="circle tooltip">
                            <div class="buttonGradient insideCircle bold">?</div>
                            <span runat="server" id="mobilePhoneTooltipPropertyDisplay"></span>
                        </span>
                    </div>
                    <div class="rightPart">
                        <div data-bind="validationOptions: { insertMessages: false }">
                            <div class="leftFloat smallRightPadding" data-bind="if: $root.SelectedProductSuggestion().Product.International">
                                + <input type="text" class="field countryCodeField" id="receiverCountryMobile" data-bind="value: MobilePhoneCountryPrefix"/>
                            </div>
                            <div class="leftFloat">
                                <input type="text" class="field mediumField" id="receiverMobilePhone" data-bind="value: MobilePhoneNumber" />
                            </div>
                            <div class="bothClear"></div>
                        </div>
                        <div class="errorText">
                            <span data-bind=""><span data-bind="text: $root.ReceiverAddress().MobilePhoneCountryPrefix.error"></span>&nbsp;</span>
                            <span data-bind=""><span data-bind="text: $root.ReceiverAddress().MobilePhoneNumber.error"></span></span>
                        </div>
                    </div>
                    <div></div>
                    <div class="bothClear"></div>
                </div>
            </div>

            <div data-bind="if: $root.SelectedProductSuggestion().Product.ShowTelephone()">
                <div id="receiverTelephoneDisplay" class="bottomMargin">
                    <div class="leftPart">
                        <span runat="server" id="telephoneLabelPropertyDisplay"></span><span data-bind="if: $root.SelectedProductSuggestion().Product.RequireTelephone()">*</span>
                        <span class="circle tooltip">
                            <div class="buttonGradient insideCircle bold">?</div>
                            <span runat="server" id="telephoneTooltipPropertyDisplay"></span>
                        </span>
                    </div>
                    <div class="rightPart">
                        <div data-bind="validationOptions: { insertMessages: false }">
                            <div class="leftFloat smallRightPadding" data-bind="if: $root.SelectedProductSuggestion().Product.International">
                                + <input type="text" class="field countryCodeField" id="receiverCountryTelephone" data-bind="value: TelephoneCountryPrefix" />
                            </div>
                            <div class="leftFloat">
                                <input type="text" class="field mediumField" id="receiverTelephone" data-bind="value: TelephoneNumber" />
                            </div>
                            <div class="bothClear"></div>
                        </div>
                        <div class="errorText">
                            <span data-bind="visible: !TelephoneCountryPrefix.isValid() && $root.ValidateAddress()"><span data-bind="text: TelephoneCountryPrefix.error"></span>&nbsp;</span>
                            <span data-bind="if: !TelephoneNumber.isValid() && $root.ValidateAddress()"><span data-bind="text: TelephoneNumber.error"></span></span>
                        </div>
                    </div>
                    <div class="bothClear"></div>
                </div>
            </div>

            <div data-bind="if: $root.SelectedProductSuggestion().Product.ShowEntryCode()">
                <div id="receiverCodeDisplay" class="bottomMargin">
                    <div class="leftPart">
                        <span runat="server" id="entryCodeLabelPropertyDisplay"></span><span data-bind="if: $root.SelectedProductSuggestion().Product.RequireEntryCode()">*</span>
                    </div>
                    <div class="rightPart">
                        <input type="text" class="field mediumField" data-bind="value: EntryCode" />
                    </div>
                    <div class="bothClear"></div>
                </div>
            </div>
        </div>
    </div>
</div>
