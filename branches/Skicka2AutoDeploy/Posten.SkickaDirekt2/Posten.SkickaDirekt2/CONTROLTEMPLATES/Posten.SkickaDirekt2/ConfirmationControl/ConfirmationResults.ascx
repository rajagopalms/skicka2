﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConfirmationResults.ascx.cs" Inherits="Posten.SkickaDirekt2.CustomControls.ConfirmationResults" %>

<%--<%@ Register Assembly="Posten.Portal.Skicka.Services, Version=1.5.0.0, Culture=neutral, PublicKeyToken=5834df7a17464b31"
    Namespace="Posten.Portal.Skicka.Services.BusinessEntities" TagPrefix="Services" %>--%>

<link rel='stylesheet' type='text/css' href='/_layouts/Posten.SkickaDirekt2/CSS/orderconfirmation_skicka.css' />
<link rel='stylesheet' type='text/css' href='/_layouts/Posten.SkickaDirekt2/CSS/buttons.css' /> 
<script type='text/javascript' src='_layouts/Posten/Cart/Presentation/js/common/jquery-1.4.3.min.js'></script>
<script type='text/javascript' src='_layouts/Posten/Cart/Presentation/js/common/jquery.ba-postmessage.min.js'></script>
<script type='text/javascript' src='_layouts/Posten/Cart/Presentation/js/common/PostenCosIFrameResizer.js'></script>
<link rel='stylesheet' type='text/css' href='/_layouts/posten/common/presentation/styles/posten.css' />
<link rel='stylesheet' type='text/css' href='/_layouts/posten/common/presentation/styles/posten.se.css' />
<link rel='stylesheet' type='text/css' href='/_LAYOUTS/Posten/Cart/Presentation/Styles/webshop_confirm.css' />
<script type="text/javascript">
    function noOfCbx() {
        var cn = document.getElementById("OrderLabelCbxGroup").childNodes.length;
        var checkboxes = 0;
        for (var j = 0; j < cn; j++) {
            if (document.getElementById("OrderLabelCbxGroup").childNodes.item(j).childNodes.length != 0) {
                checkboxes += document.getElementById("OrderLabelCbxGroup").childNodes.item(j).childNodes.length;
            }
        }
        return checkboxes;
    }

    function checkboxSelected() {
        if (document.getElementById("bottomBord").className == "bottomBorder orderLabelFormHide") {
            showFormBottomPart();
        } else if (isOrderLabelChecked() == false) {
           hideFormBottomPart();
           
        }
    }

    function hideFormBottomPart() {
        document.getElementById("bottomBord").className = "bottomBorder orderLabelFormHide";
        document.getElementById("formBlock").className = "blockWhiteWrapper orderLabelFormHide";
    }

    function showFormBottomPart() {
        document.getElementById("bottomBord").className = "bottomBorder orderLabelFormVisible";
        document.getElementById("formBlock").className = "blockWhiteWrapper orderLabelFormVisible";
        document.getElementById("mailConfirm").innerHTML = "";
    }

    function isOrderLabelChecked() {
        var isChecked = false;
        for (i = 1; i < noOfCbx() + 1; i++) {
            if (document.getElementById("cb" + i)) {
                if (document.getElementById("cb" + i).checked) {
                    isChecked = true;
                }
            }
        }
        return isChecked;
    }

    function validateInput() {
        // Remove potentially added error classes
        $("#checkboxMsg").removeClass('msgVisible');
        $("#checkboxMsg").addClass('msgHidden');
        $("#toAddressName").removeClass('inputError');
        $("#toAddressStreet").removeClass('inputError');
        $("#toAddressZip").removeClass('inputError');
        $("#toAddressCity").removeClass('inputError');
        $("#toPhoneNumber").removeClass("inputError");
        $("#toEmail").removeClass("inputError");

        var inputOK = "yes";
        if (isOrderLabelChecked() == false) {
            $("#checkboxMsg").removeClass('msgHidden');
            $("#checkboxMsg").addClass('msgVisible');
            inputOK = "no";
        }
        if ($("#toAddressName").val() == "") {
            $("#toAddressName").addClass('inputError');
            inputOK = "no";
        }
        if ($("#toAddressStreet").val() == "") {
            $("#toAddressStreet").addClass('inputError');
            inputOK = "no";
        }
        if (checkZipCode($("#toAddressZip").val()) == false) {
            $("#toAddressZip").addClass("inputError");
            inputOK = "no";
        }
        if ($("#toAddressCity").val() == "") {
            $("#toAddressCity").addClass("inputError");
            inputOK = "no";
        }
        if (checkPhoneNr($("#toPhoneNumber").val()) == false) {
            $("#toPhoneNumber").addClass("inputError");
            inputOK = "no";
        }

        if (!isValidEmailAddress($("#toEmail").val())) {
            $("#toEmail").addClass("inputError");
            inputOK = "no";
        }
        return inputOK;
    }

    function isValidEmailAddress(emailAddress) {
        var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
        return pattern.test(emailAddress);
    }

    $(document).ready(function () {

        var resizer = new PostenCosFrameResizer();
        resizer.autoResize();

        // Display result onClick
       // $("#btnSendMail").click(function (event) {
       //     event.preventDefault();
       //     $('#mailConfirm').empty();

       //     // If input ok call ajax-funktion
       //     if (validateInput() == "yes") {
       //         // Add loading-class to div
       //         $("#mailConfirm").addClass('ajaxLoading');
       //         var ids = new Array();
       //         for (var k = 1; k < (noOfCbx() + 1) ; k++) {
       //             if (document.getElementById("cb" + k)) {
       //                 if (document.getElementById("cb" + k).checked == true) {
       //                     ids.push(document.getElementById("cb" + k).value);
       //                 }
       //             }
       //         }
                
       //var requestData = JSON.stringify(
       //{
       //    prodIds: ids,
       //    customerName: $('#toAddressCompany').val(),
       //    contactPerson: $('#toAddressName').val(),
       //    email: $('#toEmail').val(),
       //    streetAddress: $('#toAddressStreet').val(),
       //    zip: $('#toAddressZip').val(),
       //    city: $('#toAddressCity').val(),
       //    phone: $('#toPhoneNumber').val(),
       //    addressCo: $('#toAddressCO').val()
       //});

       //         $.ajax({
       //             type: "POST",
       //             timeout: 60000,
       //             async: true,
       //             headers: { "X-TransactionLogID": returnSessionValue(logIDKey) },
       //             url: homeUrl+ "/_vti_bin/posten.skickadirekt2/transaction.svc/OrderLabels",
       //             data: requestData,
       //             contentType: "application/json; charset=utf-8",
       //             dataType: "json",
       //             success: AjaxSucceeded,
       //             error: AjaxFailed
       //         });
       //     }
       //     else {
       //         $('#mailConfirm').html("<div class='infoBoxError'>Du har inte fyllt i alla nödvändiga uppgifter.<br /><b>Fyll i de rödmarkerade fälten.</b></div>");
       //     }
       // });

    });

    function AjaxSucceeded(result) {       
        // Replace the div's content with the page method's return
        $('#mailConfirm').html(result.OrderLabelsResult);
        // Remove loading-class to div
        $('#mailConfirm').removeClass('ajaxLoading');
        emptyFormFields();
        hideFormBottomPart();
    }

    function emptyFormFields() {
        document.getElementById("toAddressName").value = "";
        document.getElementById("toAddressCompany").value = "";
        document.getElementById("toAddressStreet").value = "";
        document.getElementById("toAddressZip").value = "";
        document.getElementById("toAddressCity").value = "";
        document.getElementById("toPhoneNumber").value = "";
        document.getElementById("toEmail").value = "";
        document.getElementById("toAddressCO").value = "";
        for (var ki = 1; ki < (noOfCbx() + 1) ; ki++) {
            if (document.getElementById("cb" + ki)) {
                if (document.getElementById("cb" + ki).checked == true) {
                    document.getElementById("cb" + ki).checked = false;
                }
            }
        }
    }

    function AjaxFailed() {        
        $('#mailConfirm').removeClass('ajaxLoading');
        $('#mailConfirm').html("<div class='infoBoxError'>Ett timeout fel uppstod.<br /><b>Försök igen senare.</b></div>");
    }
    function openNewWin(url, width, height) {
        window.open(url, 'mywindow', 'width=' + width + ',height=' + height + ',toolbar=yes,,,status=yes,,scrollbars=yes,,resizable=yes')
    }
    function isNumber(inVarChar) {
        var isNum = true;
        for (var j = 0; j < 10; j++) {
            if (inVarChar == j) {
                isNum = true;
                break;
            } else {
                isNum = false;
            }
        }
        return isNum;
    }
    function isPlusOrMinus(inVarChar) {
        var isPlusOrMinus = true;
        if (inVarChar == "+") {
            isPlusOrMinus = true;
        } else if (inVarChar == "-") {
            isPlusOrMinus = true;
        } else {
            isPlusOrMinus = false;
        }
        return isPlusOrMinus;
    }
    function checkPhoneNr(inNr) {
        var result = true;
        if (inNr != "") {
            var newPhoneNr = "";
            for (var i = 0; i < inNr.length; i++) {
                inNr = inNr.replace(" ", "");
                if (isNumber(inNr.charAt(i)) || isPlusOrMinus(inNr.charAt(i))) {
                    newPhoneNr += inNr.charAt(i);
                } else {
                    result = false;
                }
            }
        } else {
            result = false;
        }
        return result;
    }
    function checkZipCode(inZip) {
        var zipResult = true;
        if (inZip != "") {
            var newZipNr = "";
            for (var m = 0; m < inZip.length; m++) {
                inZip = inZip.replace(" ", "");
                if (isNumber(inZip.charAt(m))) {
                    newZipNr += inZip.charAt(m);
                } else {
                    zipResult = false;
                }
            }
            if (newZipNr.length > 5 || newZipNr.length < 5) {
                zipResult = false;
            }
        } else {
            zipResult = false;
        }
        return zipResult;
    }
</script>
<%--<!-- for local-use only -->
<div id="posten">
<div id="main-content">
<!-- for local-use only -->--%>
<html>
<body class="iframe-content">
    <div id="skicka">
        <h3 id="PageHeader" runat="server"></h3>
        <!-- CONFIRMATION ITEMS -->
        <ul class="confirmation-items">
            <!-- ITEM: PRINT -->
            <li class="item" id="firstParagraph">
                <img id="Img1" src="/_LAYOUTS/Posten.SkickaDirekt2/Images/ConsignmentIllustration.jpg" class="item-image"
                    alt="" />
                <div class="inner">
                    <h4 id="Step1Header" runat="server">                         
                         Skriv ut och fäst frakthandling                         
                    </h4>
                    <a runat="server" id="pdfLink" href="#" target="_blank"><span class="inputWrapper button theme">
                        <div class="edge top">
                            <div class="left">
                            </div>
                            <div class="right">
                            </div>
                        </div>                      
                        
                        <div class="PrintShippingDocument">
                            Skriv ut frakthandling                          
                        </div>                        
                        <div class="edge bottom">
                            <div class="left">
                            </div>
                            <div class="right">
                            </div>
                        </div>
                    </span></a>
                    
                    <ul>
                        <li> Skriv ut frakthandlingen på vanligt papper eller på en etikett.</li>
                        <li> Klipp ut frakthandlingen och fäst den på brevets framsida eller paketets ovansida med tejp. Tejpa längs med frakthandlingens alla kanter, både kort- och långsidor.</li>
                        <li> Se till att streckkoden inte täcks av snören eller liknande.</li>
                    </ul>
                    
                    Frakthandlingen gäller i 60 dagar och kan därefter värken användas eller lösas in.
                    <br />
                    <br />
                    <b>Vid Frågor kontakta Postnords kundtjänst
                    <br />
                    Telefon: 0771-37 10 15
                    </b>
                    <br />
                    <br />
                    <div>
                    <%--<asp:LinkButton ID="homeLink" runat="server" Text="Fortsätt handla"></asp:LinkButton>--%>
                    <a runat="server" id="homeLink" href="javascript:redirectHome();">
                     <span class="inputWrapper button theme">                        
                        <div class="edge top">
                            <div class="left">
                            </div>
                            <div class="right">
                            </div>
                        </div>
                         <div class="PrintShippingDocument">
                            Fortsätt handla
                         </div>       
                        <div class="edge bottom">
                            <div class="left">
                            </div>
                            <div class="right">
                            </div>
                        </div>
                        </span>
                    </a>
                    </div>
                    <ul style="clear: both" id="Step1Content" runat="server">
                    </ul>
                    <span id="Step1Footer" runat="server"></span>
                </div>
                <div class="clearfix">
                </div>
            </li>
            <!-- // ITEM: PRINT -->
            <!-- ITEM: ATTACH -->
            <li class="item" id="secondParagraph" runat="server" visible="false">
                <img id="Img2" src="/_LAYOUTS/Posten.SkickaDirekt2/Images/HowToIllustration.jpg" class="item-image"
                    alt="" />
                <div class="inner">
                    <h4 id="Step2Header" runat="server"></h4>
                    <span id="Step2Ingress" runat="server"></span>
                    <ul>
                        <!-- if brev utrikes -->
                        <li id="brev_utrikes" runat="server" visible="false"></li>
                        <!-- if express -->
                        <li id="express_both" runat="server" visible="false"></li>
                        <!-- if mottagningsbevis utrikes -->
                        <li id="mottag_utrikes" runat="server" visible="false"></li>
                        <!-- if lördagsutdelning -->
                        <li id="express_lordag" runat="server" visible="false"></li>
                        <!-- if utrikes -->
                        <li id="utrikes_alla" runat="server" visible="false"></li>
                    </ul>
                    <span id="Step2Footer" runat="server"></span>
                </div>
                <div class="clearfix">
                </div>
            </li>
            <!-- // ITEM: ATTACH -->
            <!-- ITEM: HAND IN -->
            <li class="item last-item" id="thirdParagraph">
                <%--<img id="mapImg" src="" class="item-image" runat="server" alt="" />--%>
                <div class="inner">
                    <h4 id="thirdParHeader" runat="server"></h4>
                    <div id="errorMsg" runat="server" visible="false">
                    </div>
                    <ul class="box-list" id="leftBoxlist" runat="server" visible="true">
                        <li id="recNameLi" runat="server">
                            <div id="receiverName" runat="server">
                            </div>
                            <ul id="recNameUl" runat="server">
                                <li>
                                    <div id="receiverAddress" runat="server">
                                    </div>
                                </li>
                                <li>
                                    <div id="receiverZipNCity" runat="server">
                                    </div>
                                </li>
                                <li>
                                    <div id="receiverPhone" runat="server">
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li id="recOpentimesLi" runat="server" style="clear: left">
                            <div id="recOpentimes" runat="server">
                            </div>
                            <ul>
                                <li>
                                    <div id="receiverOpenTimes" runat="server">
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="box-list" id="rightBoxlist" runat="server" visible="true">
                        <li id="recStoptimesLi" runat="server" style="float: left;">
                            <div id="receiverStopTimes" runat="server">
                            </div>
                            <ul>
                                <li>
                                    <div id="receiverStoppingTimes" runat="server">
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li id="recSoklinkLi" runat="server"></li>
                    </ul>
                </div>
                <div class="clearfix">
                </div>
            </li>
            <!-- // ITEM: HAND IN -->
        </ul>
        <div style="padding: 0px 0px 10px 20px;" class="clearfix">
            <%--<a href="" id="imgpselank" runat="server" target="_blank">Visa större karta</a>--%>
        </div>
        <!-- // ITEM: LABEL-ORDERING -->
        <h3 id="BuyLabelHeader" runat="server"></h3>
        <div style="padding: 20px;">
            <p id="BuyLabelContent" runat="server">
            </p>
        </div>
        <!-- // ITEM: LABEL-ORDERING -->
        <!-- // ITEM: ADHESIVE-LABEL ORDERING -->
       <%-- <h3 id="OrderLabelHeader" runat="server"></h3>
        <div id="OrderLabelCbxGroup">
            <div class="checkboxRow" id="CheckboxRow">
                <asp:Repeater ID="OrderLabelRepeater" runat="server" EnableViewState="true">
                    <HeaderTemplate>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div class="checkboxCell">
                            <div style="float: left; width: 16px; margin-right: 4px">
                                <input type="checkbox" onclick="checkboxSelected();" id="cb<%# ((RepeaterItem)Container).ItemIndex + 1 %>"
                                  value="<%#DataBinder.Eval(Container.DataItem,"ProdId") %>" title="<%#DataBinder.Eval(Container.DataItem,"Name") %>" />
                            </div>
                            <div style="float: left; width: 148px;">
                                <b>
                                    <%#DataBinder.Eval(Container.DataItem,"Name") %></b><br />
                                <%#DataBinder.Eval(Container.DataItem,"Quantity") %>
                                st
                            </div>                          
                        </div>
                        <%#GetNewRowCode(((RepeaterItem)Container).ItemIndex+1)%>
                    </ItemTemplate>
                    <FooterTemplate>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
            <div class="clearfix" style="height: 10px">
            </div>
            <br style="clear: both;">
        </div>
        <div class="clearfix" style="height: 0px;">
        </div>
        <div class="bottomBorder orderLabelFormHide" id="bottomBord">
        </div>
        <div id="checkboxMsg" class="msgHidden" style="margin-left: 0px">
            Du måste välja någon etikett eller plastficka
        </div>
        <div class="blockWhiteWrapper orderLabelFormHide" id="formBlock">
            <div style="margin-top: 15px" id="mailConfirmForm">
                <div style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; margin-top: 25px; margin-bottom: 15px">
                    <b>Skicka till:</b>
                </div>
                <div class="skickaText bold">
                    Företagsnamn <span class="normal">(Ej obligatorisk)</span>
                    <br />
                    <input class="textBox" tabindex="1" id="toAddressCompany" type="text" />
                </div>
                <div class="skickaText bold">
                    Namn
                    <br />
                    <input class="textBox" tabindex="2" id="toAddressName" type="text" />
                    <br />
                </div>
                <div class="skickaText bold">
                    C/O <span class="normal">(Ej obligatorisk)</span>
                    <br />
                    <input class="textBox" tabindex="3" id="toAddressCO" type="text" />
                </div>
                <div class="skickaText bold">
                    Adress<br />
                    <input class="textBox" tabindex="4" id="toAddressStreet" type="text" />
                </div>
                <div class="skickaText bold">
                    Postnummer<br />
                    <input class="textBox" tabindex="5" id="toAddressZip" type="text" maxlength="5" />
                </div>
                <div class="skickaText bold">
                    Postort<br />
                    <input class="textBox" tabindex="6" id="toAddressCity" type="text" />
                </div>
                <div class="skickaText bold">
                    Telefonnummer<br />
                    <input class="textBox" tabindex="7" id="toPhoneNumber" type="text" />
                </div>
                <div class="skickaText bold" style="margin-bottom: 10px">
                    E-post<br />
                    <input class="textBox" tabindex="8" id="toEmail" type="text" />
                </div>
                <div class="blockContentLeft">
                    <a id="btnSendMail" href="#" class="button blue" style="text-decoration: none; margin-bottom: 5px">
                        <span><strong>Beställ</strong> </span></a>
                </div>
                <div class="normal" style="clear: left; margin-bottom: 10px; margin-top: 10px" id="OrderLabelFooter" runat="server">
                </div>
            </div>
        </div>
        <div id="mailConfirm">
        </div>--%>
        <!-- // ITEM: ADHESIVE-LABEL ORDERING -->
    </div>
</body>
</html>

<script type="text/javascript">
    var homeUrl = "<%= HomeUrl %>";
    var logIDKey = "transactionLogID";

    localStorage.clear();

    function redirectHome() {
        window.top.location.href = homeUrl;
    }

    function returnSessionValue(key) {
        var value = "";
        if (sessionStorage.getItem(key)) {
            value = sessionStorage.getItem(key);
        }

        return value;
    };
</script>