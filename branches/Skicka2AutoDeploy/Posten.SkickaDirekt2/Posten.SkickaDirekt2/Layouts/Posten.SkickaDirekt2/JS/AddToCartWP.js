﻿/* -------------------------------------------------------------------------------------*/
/* --------------------------------------- Initialization ------------------------------*/
/* -------------------------------------------------------------------------------------*/
var basketOrderNumberKey = "basketOrderNumbers";
var basketIdKey = "basketId";
var tc; var vv; var adLine;
$(document).ready(function () {
    ko.applyBindings(window.skickaAddToCartVM, document.getElementById('AddToCartWPDisplay'));

    skickaAddToCartVM.HandleGetServiceOrderNumbersForBasket();
});

/* -------------------------------------------------------------------------------------*/
/* --------------------------------------- Web service calls  --------------------------*/
/* -------------------------------------------------------------------------------------*/
var reData;
function pvtValidate(pvtValidateObj, callback) {
    var requestData = JSON.stringify(
        {
            size: pvtValidateObj.size,
            weight: pvtValidateObj.weight,
            additionalServiceIDs: pvtValidateObj.additionalServiceIDs,
            account: pvtValidateObj.account,
            countryCode: pvtValidateObj.countryCode,
            selectedServiceID: pvtValidateObj.selectedServiceID,
            sender: pvtValidateObj.sender,
            receiver: pvtValidateObj.receiver
        });

    try {
        reData = requestData;

        $.ajax({
            type: "POST",
            cache: true,
            contentType: "application/json; charset=utf-8",
            async: true,
            url: serverRelativeUrl + servicePath + "/ValidateItem",
            dataType: "json",
            data: requestData,
            headers: { "X-TransactionLogID": returnSessionValue(logIDKey) },
            success: function (data) {
                var result = JSON.parse(data.ValidateItemResult);
                if (result.Valid === true) {
                    //validation of pay on delivery
                    //validation of delivery date
                    callback(result);
                }
                else {
                    skickaAddToCartVM.PvtValid(false);
                    skickaAddToCartVM.ShowProgress(false);
                    skickaAddToCartVM.ISPValidationError(result.StatusMsg);
                }
            },
            error: function (data) {
                //Generate Error Msg
                console.log(data);
                skickaAddToCartVM.ShowProgress(false);
                return false;
            }
        });
    }
    catch (e) {
        console.log(e.message);
        skickaAddToCartVM.ShowProgress(false);
    }
}


function addToCart(cartItem, validationResponse, callback) {
    var requestData = JSON.stringify(
        {
            ApplicationId: cartItem.ApplicationId,
            ApplicationName: cartItem.ApplicationName,
            BasketId: cartItem.BasketId,
            ConfirmationUrl: cartItem.ConfirmationUrl,
            CurrencyCode: cartItem.CurrencyCode,
            CustomApplicationLink: cartItem.CustomApplicationLink,
            CustomApplicationLinkText: cartItem.CustomApplicationLinkText,
            Details: cartItem.Details,
            EditUrl: cartItem.EditUrl,
            FreeTextField1: cartItem.FreeTextField1,
            FreeTextField2: cartItem.FreeTextField2,
            Name: cartItem.Name,
            OrderNumber: cartItem.OrderNumber,
            ProduceUrl: cartItem.ProduceUrl,
            ServiceImageUrl: cartItem.ServiceImageUrl,
            ServiceOrderLines: JSON.stringify(skickaAddToCartVM.ServiceOrderLineItemList()),
            SubTotal: cartItem.SubTotal,
            Vat: cartItem.Vat,
            Total: ""
        });

    try {
        $.ajax({
            type: "POST",
            cache: true,
            contentType: "application/json; charset=utf-8",
            async: true,
            url: serverRelativeUrl + "/_vti_bin/Posten/Cart/CartTransaction.svc/AddItemToCart",
            dataType: "json",
            data: requestData,
            headers: { "X-TransactionLogID": returnSessionValue(logIDKey) },
            success: function (data) {
                var result = data;
                var basketOrderNumber = result.OrderId;
                var basketId = result.BasketId;
                setLocalValue(basketIdKey, basketId);
                //TODO: databind using ko
                var basketid = 'basketIDHolder';
                $("[id$=" + basketid + "]").val(basketId);
                callback(basketOrderNumber, validationResponse);
                console.log("KK success");
            },
            error: function (data) {
                skickaAddToCartVM.ShowProgress(false);
                console.log(data);
                console.log("KK failed");
            }
        });
    }
    catch (e) {
        console.log(e.message);
    }
}


var successData, errorData;
var empty= [];
function updateInCart(cartItem, validationResponse, callback) {
    var oldBasketOrderNumber = skickaAddToCartVM.ConsignmentToEdit().OrderNumber;
    var requestData = JSON.stringify(
        {
            ApplicationId: cartItem.ApplicationId,
            ApplicationName: cartItem.ApplicationName,
            BasketId: cartItem.BasketId,
            ConfirmationUrl: cartItem.ConfirmationUrl,
            CurrencyCode: cartItem.CurrencyCode,
            CustomApplicationLink: cartItem.CustomApplicationLink,
            CustomApplicationLinkText: cartItem.CustomApplicationLinkText,
            Details: cartItem.Details,
            EditUrl: cartItem.EditUrl,
            FreeTextField1: cartItem.FreeTextField1,
            FreeTextField2: cartItem.FreeTextField2,
            Name: cartItem.Name,
            OrderNumber: cartItem.OrderNumber,
            ProduceUrl: cartItem.ProduceUrl,
            ServiceImageUrl: cartItem.ServiceImageUrl,
            ServiceOrderLines: JSON.stringify(skickaAddToCartVM.ServiceOrderLineItemList()),
            SubTotal: cartItem.SubTotal,
            Vat: cartItem.Vat,
            Total: "",
            updateOrderId: oldBasketOrderNumber 
        });
    
    try {
        $.ajax({
            type: "POST",
            cache: true,
            contentType: "application/json; charset=utf-8",
            async: true,
            url: serverRelativeUrl + "/_vti_bin/Posten/Cart/CartTransaction.svc/UpdateItemInCart",
            dataType: "json",
            data: requestData,
            headers: { "X-TransactionLogID": returnSessionValue(logIDKey) },
            success: function (data) {
                successData = data;
                var result = data;
                var basketOrderNumber = result;
                callback(basketOrderNumber, oldBasketOrderNumber, validationResponse);
            },
            error: function (data) {
                errorData = data;
                console.log(data);
                console.log('exception in update in KK');
            }

        });
    }
    catch (e) {
        console.log(e.message);
    }
}

function informPutInCart(basketOrderItem, ispObject, basketOrderNumber, receiverDestinationId, callback, shoppingCartScrollTo) {
    var requestData = JSON.stringify(
    {
        basketOrderNumber: basketOrderNumber,
        validationResponse: ispObject.validationResponse,
        serviceName: ispObject.serviceCombination,
        serviceCode: ispObject.serviceCode,
        size: ispObject.size,
        weight: ispObject.weight,
        fromAddress: ispObject.fromAddress,
        toAddress: ispObject.toAddress,
        codAmount: ispObject.codAmount,
        plusgiro: ispObject.plusgiro,
        postforskottKontoNr: ispObject.postforskottKontoNr,
        postforskottPaymentReference: ispObject.postforskottPaymentReference,
        orderconfirmationUrl: ispObject.orderconfirmationUrl,
        urlText: ispObject.urlText,
        url: ispObject.url,
        details: ispObject.details,
        receiverDestinationId: receiverDestinationId
    });

    try {
        $.ajax({
            type: "POST",
            cache: true,
            contentType: "application/json; charset=utf-8",
            async: true,
            url: serverRelativeUrl + servicePath + "/AddOrderToCart",
            dataType: "json",
            data: requestData,
            headers: { "X-TransactionLogID": returnSessionValue(logIDKey) },
            success: function (data) {
                tr = data;
                var result = data;
                shoppingCartScrollTo(true);
                shoppingCartScrollTo(false); // Should be taken care of by the custom binding function in core.js

                callback(result, basketOrderNumber);
            },
            error: function (data) {
                console.log(data);

                //Remove item from cart
                RemoveItemFromCart(basketOrderNumber);
                
                // Hide in progress sign
                skickaAddToCartVM.ShowProgress(false);
                               
                return false;
            }
        });
    }
    catch (e) {
        console.log(e.message);

        //Remove item from cart
        RemoveItemFromCart(basketOrderNumber);
        //Show error message

        // Hide in progress sign
        skickaAddToCartVM.ShowProgress(false);
    }
}

function updatePutInCart(newBasketOrderNumber, ispObject, oldBasketOrderNumber, receiverDestinationId, callback, shoppingCartScrollTo) {
    var requestData = JSON.stringify(
    {
        basketOrderNumber: oldBasketOrderNumber,
        validationResponse: ispObject.validationResponse,
        serviceName: ispObject.serviceCombination,
        serviceCode: ispObject.serviceCode,
        size: ispObject.size,
        weight: ispObject.weight,
        fromAddress: ispObject.fromAddress,
        toAddress: ispObject.toAddress,
        codAmount: ispObject.codAmount,
        plusgiro: ispObject.plusgiro,
        postforskottKontoNr: ispObject.postforskottKontoNr,
        postforskottPaymentReference: ispObject.postforskottPaymentReference,
        orderconfirmationUrl: ispObject.orderconfirmationUrl,
        urlText: ispObject.urlText,
        url: ispObject.url,
        details: ispObject.details,
        receiverDestinationId: receiverDestinationId,
        updateOrderId: newBasketOrderNumber
    });
    try {
        $.ajax({
            type: "POST",
            cache: true,
            contentType: "application/json; charset=utf-8",
            async: true,
            url: serverRelativeUrl + servicePath + "/UpdateOrderInCart",
            dataType: "json",
            data: requestData,
            headers: { "X-TransactionLogID": returnSessionValue(logIDKey) },
            success: function (data) {
                if (data !== null) {
                    tr = data;
                    var result = data;
                    shoppingCartScrollTo(true);
                    shoppingCartScrollTo(false); // Should be taken care of by the custom binding function in core.js

                    callback(oldBasketOrderNumber, newBasketOrderNumber);
                }
                else {
                    console.log("exception in itemnotes");
                    // Hide in progress sign
                    skickaAddToCartVM.ShowProgress(false);
                }
            },
            error: function (data) {
                console.log(data);
                // Hide in progress sign
                skickaAddToCartVM.ShowProgress(false);
                return false;
            }
        });
    }
    catch (e) {
        console.log(e.message);
        // Hide in progress sign
        skickaAddToCartVM.ShowProgress(false);
    }
}

function RemoveItemFromCart(orderNumber) {
    $.ajax({
        type: "POST",
        cache: true,
        contentType: "application/json; charset=utf-8",
        async: true,
        url: serverRelativeUrl + "/_vti_bin/Posten/Cart/CartTransaction.svc/RemoveService",
        dataType: "json",
        data: JSON.stringify({ serviceOrderNumber: orderNumber, basketID: returnLocalValue(basketIdKey) }),
        headers: { "X-TransactionLogID": returnSessionValue(logIDKey) },
        success: function (data) {
            var result = data;
            
            //Remove item with the given basketOrderNumberfrom LocalStorage
            removeLocalConsignment(orderNumber);

            return data;
        },
        error: function (data) {
            console.log("exception cart removal failure: " + data);
            
            return false;
        }
    });
}

var cList;
function GetConsignmentsFromBasket(callback, shoppingCartProgress) {
    try {
        shoppingCartProgress(true);
        var basketID = returnLocalValue("basketId");
        if (basketID !== "") {

            $.ajax({
                type: "POST",
                cache: true,
                contentType: "application/json; charset=utf-8",
                async: true,
                url: serverRelativeUrl + "/_vti_bin/Posten/Cart/CartTransaction.svc" + "/ReturnBasketServices",
                dataType: "json",
                data: JSON.stringify({ basketId: basketID }),
                headers: { "X-TransactionLogID": returnSessionValue(logIDKey) },
                success: function (data) {
                    cList = JSON.parse(data);
                    var obj = JSON.parse(data);
                    var resultSet = [];
                    $.each(obj, function (index, value) {
                        resultSet.push(value.OrderNumber);
                    });
                    setLocalValue(basketOrderNumberKey, JSON.stringify(resultSet));

                    var orderNumbers = ReturnOrderNumbersFromLocalStorage();
                    GetConsignments(obj, orderNumbers, callback, shoppingCartProgress);
                },
                error: function (data) {
                    console.log(data);
                    // Hide progress image
                    shoppingCartProgress(false);               
                }
            });
        }
        else {
            shoppingCartProgress(false);
        }
    }
    catch (e) {
        console.log(e.message);
        shoppingCartProgress(false);
    }
}

function GetConsignments(cartResults, orderNumbers, callback, shoppingCartProgress) {
    $.ajax({
        type: "POST",
        cache: true,
        contentType: "application/json; charset=utf-8",
        async: true,
        url: serverRelativeUrl + servicePath + "/ReturnConsignments",
        dataType: "json",
        data: JSON.stringify({ orderNumbers: orderNumbers }),
        headers: { "X-TransactionLogID": returnSessionValue(logIDKey) },
        success: function (data) {
            var obj = $.parseJSON(data.ReturnConsignmentsResult);

            callback(cartResults, obj);
        },
        error: function (data) {
            console.log(data);
            // Hide progress image
            shoppingCartProgress(false);
        }
    });
}

/* -------------------------------------------------------------------------------------*/
/* --------------------------------------- Models --------------------------------------*/
/* -------------------------------------------------------------------------------------*/

var ConsignmentBE = (function () {
    function consignmentBE(AccountNumber, AdditionalServices, AdditionalInformation, Amount, BaseServiceLeveransId, BookingId, CODAmount, DestinationID, Diameter, FromAddress, Height, Length, MarketplaceBookingID, PaymentReference, Plusgiro, ProductCode, ProductName, ServiceCode, ToAddress, Weight, Width, OrderNumber, cartPrice, FreeText1, FreeText2, Index) {
        this.AccountNumber = AccountNumber;
        this.AdditionalServices = AdditionalServices;
        this.AdditionalInformation = AdditionalInformation;
        this.Amount = Amount;
        this.BaseServiceLeveransId = BaseServiceLeveransId;
        this.BookingId = BookingId;
        this.CODAmount = CODAmount;
        this.DestinationID = DestinationID;
        this.Diameter = Diameter;        
        this.FromAddress = FromAddress;        
        this.Height = Height;
        this.Length = Length;
        this.MarketplaceBookingID = MarketplaceBookingID;
        this.PaymentReference = PaymentReference;
        this.Plusgiro = Plusgiro;
        this.ProductCode = ProductCode;
        this.ProductName = ProductName;
        this.ServiceCode = ServiceCode;
        this.ToAddress = ToAddress;
        this.Weight = Weight;
        this.Width = Width;
        this.OrderNumber = OrderNumber;
        this.Price = parseThousandSeperator(parseFloat(cartPrice).toFixed(2).toLocaleString().replace(".", ","), ' ', ',');
        this.FreeText1 = FreeText1;
        this.FreeText2 = FreeText2;
        this.Index = Index;
    }


    GenerateAdditionalServiceString = function (additionalServices) {
        var val = "";
        if (additionalServices.length > 0) {
            val = "Tilläggstjänst: ";

            $.each(additionalServices, function (index, value) {
                val += value.Name() + ', ';
            });
        }
        return val;
    }

    GenerateTooltipString = function (text1, text2, text3) {
        if (text3 === null) { text3 = ""; }
        return text1 + " <br/> " + text2 + " <br/> " + text3;
    }

    return consignmentBE;
})();


var ISPServiceOrderItemBE = (function () {
    function ispServiceOrderItemBE(item, fromAddress, toAddress, validationResponse, size, weightInGrams, codAmount, plusgiro, accountNumber, referece, details) {
        
        this.basketOrderNumber = "";
        this.item = item;
        this.fromAddress = fromAddress;
        this.toAddress = toAddress;
        this.validationResponse = validationResponse;
        this.serviceCombination = skickaAddToCartVM.SelectedProduct().Name;
        this.serviceCode = skickaAddToCartVM.SelectedProduct().Code;
        this.size = size;
        this.weight = weightInGrams;       
        //CGI CR C34431 START - Replacing "," with "."
        if (codAmount != null) {
            codAmount = codAmount ? codAmount.toLocaleString().replace(",", ".") : codAmount;
            this.codAmount = codAmount;
        }
        //CGI CR C34431 END
        this.codAmount = codAmount;
        this.plusgiro = plusgiro;
        this.postforskottKontoNr = accountNumber;
        this.postforskottPaymentReference = referece;
        this.orderconfirmationUrl = ConfirmationDownloadUrl;
        this.urlText = "Skriv ut frakthandling"; 
        this.url = WaybillUrl;
        this.details = details;
    }

    return ispServiceOrderItemBE;
})();


var ServiceCombinationItemBE = (function () {
    function ServiceCombinationItemBE(selectedProduct) {

        this.BaseServiceId = selectedProduct.ProductId;
        this.Name = selectedProduct.Name;
        this.ServiceKey = selectedProduct.ProductId;
    }

    return ServiceCombinationItemBE;
})();

var PVTValidationObjectBE = (function () {
    function pvtValidationResponseBE(
        size,
        weightInGrams,
        additionalServiceIDs,
        account,
        destinationId,
        productId,
        senderAddress,
        receiverAddress) {
        this.size = size;
        this.weight = weightInGrams;
        this.additionalServiceIDs = additionalServiceIDs;
        if (account != null) {
            if (account.Amount != null) {
                account.Amount = account.Amount ? account.Amount.toLocaleString().replace(",", ".") : account.Amount;
            }
        }
        this.account = account;
        this.countryCode = destinationId;
        this.selectedServiceID = productId;
        this.sender = senderAddress;
        this.receiver = receiverAddress;
    }

    return pvtValidationResponseBE;
})();

var ServiceOrderItemBE = (function () {
    function serviceOrderItemBE(
        currencyCode,
        freeTextField1Data,
        freeTextField2Data,
        selectedAddtionalServiceCodes,
        priceNoVat,
        vat) {
        this.ApplicationId = "ItemNotes"; 
        this.ApplicationName = "Skicka";  
        this.BasketId = returnLocalValue(basketIdKey);
        this.ConfirmationUrl = ConfirmationPageUrl;
        this.CurrencyCode = currencyCode;
        this.CustomApplicationLink = ApplicationUrl;
        this.CustomApplicationLinkText = "Skicka"; 
        this.Details = "";
        this.EditUrl = EditUrl;
        this.FreeTextField1 = "Till: " + freeTextField1Data;
        this.FreeTextField2 = "Mått och vikt:" + freeTextField2Data;
        this.Name = skickaSelectServiceVM.SelectedProduct().Name; 
        this.OrderNumber = "";
        this.ProduceUrl = ProduceUrl;
        this.ServiceImageUrl = ""; 
        this.ServiceOrderLines = selectedAddtionalServiceCodes;
        this.SubTotal = priceNoVat;
        this.Vat = vat;
    }

    return serviceOrderItemBE;
})();


function ServiceOrderLineItem(
        articleId,
        articleName,
        articlePrice,
        total,
        vat,
        orderLineNumber,
        quantity,
        vatPercentage,
        companyCode,
        customerId,
        taxCountry,
        footnoteText,
        vatCode
        )
            {
        this.ArticleId = articleId;
        this.ArticleName = articleName;
        this.ArticlePrice = articlePrice;
        this.CompanyCode = companyCode;
        this.CustomerId = customerId;
        this.FootnoteText = footnoteText;
        this.OrderLineNumber = orderLineNumber;
        this.Quantity = quantity;
        this.TaxCountry = taxCountry;
        this.Total = total;
        this.Vat = vat;
        if (vatPercentage) {
            this.VatPercentage = vatPercentage / 100;
        } else {
            this.VatPercentage = 25 / 100;
        }
        this.VatCode = vatCode;
        }

/* -------------------------------------------------------------------------------------*/
/* --------------------------------------- View model ----------------------------------*/
/* -------------------------------------------------------------------------------------*/

window.skickaAddToCartVM = (function () {
    var vm = {};

    /* --------------------------------- pub/sub ----------------------------------------------*/
    vm.IsShoppingCartEditMode = ko.observable(false).syncWith("IsShoppingCartEditMode", true);

    vm.Products = ko.observableArray([]).subscribeTo("Products");
    vm.SelectedProduct = ko.observable().subscribeTo("SelectedProduct", true);
    vm.CurrentSize = ko.observable(null).subscribeTo("CurrentSize", true);

    vm.SelectedProductSuggestion = ko.observable().subscribeTo("SelectedProductSuggestion");

    vm.AccountIsValid = ko.observable(false).subscribeTo("AccountIsValid", true);
    vm.SenderAddressIsValid = ko.observable(false).subscribeTo("SenderAddressIsValid", true);
    vm.ReceiverAddressIsValid = ko.observable(false).subscribeTo("ReceiverAddressIsValid", true);
    vm.IsValidSelectService = ko.observable(false).subscribeTo("IsValidSelectService", true);
    
    vm.Account = ko.observable(null).subscribeTo("Account", true);
    vm.SenderAddress = ko.observable(null).subscribeTo("SenderAddress", true);
    vm.ReceiverAddress = ko.observable(null).subscribeTo("ReceiverAddress", true);
    vm.SelectedProductSuggestion = ko.observable().subscribeTo("SelectedProductSuggestion", true);
    vm.SelectedDestination = ko.observable().subscribeTo("SelectedDestination", true);

    vm.ShoppingCartList = ko.observable([]).publishOn("ShoppingCartList");
    vm.ConsignmentToEdit = ko.observable().subscribeTo("ConsignmentToEdit", true);

    vm.PvtValid = ko.observable(true);
    vm.ISPValidationError = ko.observable();
    vm.IsValid = ko.pureComputed(function () {
        return vm.AccountIsValid() &&
            vm.SenderAddressIsValid() &&
            vm.ReceiverAddressIsValid()&&
            vm.PvtValid()
    });

    vm.ShowProgress = ko.observable(false);

    vm.ShoppingCartProgress = ko.observable(false).publishOn("ShoppingCartProgress");
    vm.ShoppingCartScrollTo = ko.observable().syncWith("CartScrollTo");

    vm.ServiceOrderLineItemList = ko.observableArray([]);
    vm.ServiceOrderLineItemListString = ko.observable("");
    /* --------------------------------- Observables ------------------------------------------*/

    /* ------------------------------- Public methods  ----------------------------------------*/
    vm.AddToCartClick = function () {
        vm.PvtValid(true);
        ko.postbox.publish("IsAddToCartClicked");
        if (vm.IsValid()=== true) {
            // ok to add to cart
            vm.ShowProgress(true); // Only show progress sign if form data is valid
            
            var selectedAddtionalServicesCodes = [];
            $.each(vm.SelectedProductSuggestion().SelectedAdditionalServices(), function (index, value) {
                selectedAddtionalServicesCodes.push(value.Code());
            });

            var phone =
                ((vm.SelectedProductSuggestion().Product.International && vm.ReceiverAddress().MobilePhoneNumber()!=="") ? "+" + vm.ReceiverAddress().TelephoneCountryPrefix() : "") +
                (vm.ReceiverAddress().TelephoneNumber() ? vm.ReceiverAddress().TelephoneNumber() : "");
            var mobile =
                ((vm.SelectedProductSuggestion().Product.International && vm.ReceiverAddress().MobilePhoneNumber()!=="") ? "+" + vm.ReceiverAddress().MobilePhoneCountryPrefix() : "") +
                StripDashSpace(vm.ReceiverAddress().MobilePhoneNumber());
            console.log(phone);
            console.log(mobile);
            vm.ReceiverAddress().Phone = phone ? phone : "";
            vm.ReceiverAddress().Mobile = mobile ? mobile : "";
            console.log(vm.ReceiverAddress().Phone);
            console.log(vm.ReceiverAddress().Mobile);

            var pvtValidateObj = new PVTValidationObjectBE(
                FormatSize(ko.toJS(vm.CurrentSize)),                
                skickaSelectServiceVM.CurrentWeight().CalculatedWeight(),
                selectedAddtionalServicesCodes,
                ko.toJS(vm.Account),
                vm.SelectedDestination().DestinationID,
                vm.SelectedProductSuggestion().ProductId,
                ko.toJS(vm.SenderAddress),
                ko.toJS(vm.ReceiverAddress));

            pvtValidate(pvtValidateObj, HandlePvtValidate);

        }
    };

    
    vm.PvtValidateCompound = function () {
        var selectedAddtionalServicesCodes = [];
        $.each(vm.SelectedProductSuggestion().SelectedAdditionalServices(), function (index, value) {
            selectedAddtionalServicesCodes.push(value.Code());
        });

        var pvtValidateObj = new PVTValidationObjectBE(
                    FormatSize(ko.toJS(vm.CurrentSize)),                    
                    skickaSelectServiceVM.CurrentWeight().CalculatedWeight(),
                    selectedAddtionalServicesCodes,
                    ko.toJS(vm.Account),
                    vm.SelectedDestination().DestinationID,
                    vm.SelectedProductSuggestion().ProductId,
                    ko.toJS(vm.SenderAddress),
                    ko.toJS(vm.ReceiverAddress));

        var requestData = JSON.stringify(
            {
                size: pvtValidateObj.size,
                weight: pvtValidateObj.weight,
                additionalServiceIDs: pvtValidateObj.additionalServiceIDs,
                account: pvtValidateObj.account,
                countryCode: pvtValidateObj.countryCode,
                selectedServiceID: pvtValidateObj.selectedServiceID,
                sender: pvtValidateObj.sender,
                receiver: pvtValidateObj.receiver,
                serviceOrderItemSettings: new ServiceOrderItemBE(
                vm.SelectedProductSuggestion().Price.Currency())
            });

        try {
            $.ajax({
                type: "POST",
                cache: true,
                contentType: "application/json; charset=utf-8",
                async: true,
                url: serverRelativeUrl + servicePath + "/AddItem",
                dataType: "json",
                data: requestData,
                headers: { "X-TransactionLogID": returnSessionValue(logIDKey) },
                success: function (data) {
                    var result = JSON.parse(data.AddItemResult);
                    if (result.Valid === true) {
                       
                    }
                    else {
                        skickaAddToCartVM.PvtValid(false);
                        skickaAddToCartVM.ShowProgress(false);
                    }
                },
                error: function (data) {
                    //Generate Error Msg
                    console.log(data);
                    return false;
                }
            });
        }
        catch (e) {
            console.log(e.message);
        }
    }


    /* ------------------------------- Private methods  ---------------------------------------*/

    var FormatSize = function (size) {
        size.Length = size.Length ? CmStringToMmString(size.Length) : size.Length;
        if (vm.CurrentSize().IsRoll()) {
            size.Width = undefined;
            size.Height = undefined;
            size.Diameter = size.Diameter ? CmStringToMmString(size.Diameter) : size.Diameter;
        }
        else {
            size.Width = size.Width ? CmStringToMmString(size.Width) : size.Width;
            size.Height = size.Height ? CmStringToMmString(size.Height) : size.Height;
            size.Diameter = undefined;
        }

        return size;
    }

    vm.IsBaseAdditionalService = function (productId, additionalServiceCode) {
        var baseAdditionalService = false;

        switch (productId) {
            case 2: //7
                baseAdditionalService = (additionalServiceCode == '01');

                break;
            case 7: //9 express brev inrikes              
                baseAdditionalService = (additionalServiceCode == 'LX');

                break;
            case 12://10                
                baseAdditionalService = (additionalServiceCode == 'RR');

                break;
            case 13: //13
                baseAdditionalService = (additionalServiceCode == 'RR');

                break;
        }
        return baseAdditionalService;
    };

    var HandlePvtValidate = function (validationResponse) {
        vv = validationResponse;
        var selectedAddtionalServicesCodes = [];
        $.each(vm.SelectedProductSuggestion().SelectedAdditionalServices(), function (index, value) {
            selectedAddtionalServicesCodes.push(value.Code);
        });

        //Collect ServiceOrderLines
        var lineIndex = 0;
        var defaultVAT = 6;
        vm.ServiceOrderLineItemList([]);

        //Base service
        var calculatedVAT = (validationResponse.BaseServicePrice.VATAmount > 0) ? "" : defaultVAT;
        var line = new ServiceOrderLineItem(validationResponse.SapId, vm.SelectedProduct().Name, validationResponse.BaseServicePrice.AmountNoVat,
            validationResponse.BaseServicePrice.AmountNoVat, validationResponse.BaseServicePrice.VATAmount, lineIndex, 1, validationResponse.BaseServicePrice.VATPercentage,
            validationResponse.CompanyCode, "20587085", "SE", calculatedVAT, validationResponse.BaseServicePrice.VatCode);
        vm.ServiceOrderLineItemList.push(line);

        //Additional Services
        $.each(validationResponse.AdditionalServices, function (index1, value1) {
            var addServiceOrderLine = false;
            if (vm.IsBaseAdditionalService(vm.SelectedProduct().ProductId, value1.AdditionalServiceCode)) {
                addServiceOrderLine = true;
            }
            else {
                $.each(vm.SelectedProductSuggestion().SelectedAdditionalServices(), function (index, value) {
                    //Find selected additional service + the hidden included additional service
                    if (value1.AdditionalServiceCode === value.Code()) {
                        addServiceOrderLine = true;
                    }
                });
            }

            //additing service line for the additional service code for missing 1.2m in the receipt.
            if (value1.AdditionalServiceCode == '0000010')
            {
                addServiceOrderLine = true;
            }
            //
            if (addServiceOrderLine) {
                lineIndex = lineIndex + 1;
                calculatedVAT = (value1.Price.VATAmount > 0) ? "" : defaultVAT;

                var additionalLine = new ServiceOrderLineItem(value1.SapId, value1.Name, value1.Price.AmountNoVat,
                    value1.Price.AmountNoVat, value1.Price.VATAmount, lineIndex, 1, value1.Price.VATPercentage, value1.CompanyCode, "20587085", "SE", calculatedVAT,value1.Price.VatCode);
                vm.ServiceOrderLineItemList.push(additionalLine);
                adLine = additionalLine;                
            }
        });
        
        vm.ServiceOrderLineItemListString(JSON.stringify(vm.ServiceOrderLineItemList()));
        //Collect ServiceOrderLines

        var cartItem = new ServiceOrderItemBE(
            vm.SelectedProductSuggestion().Price.Currency(),
            HandleFreeText1(vm.ReceiverAddress()),
            HandleFreeText2(),
            selectedAddtionalServicesCodes,

            validationResponse.Price.AmountNoVat ? validationResponse.Price.AmountNoVat.toFixed(2).toLocaleString().replace(".", ",") : validationResponse.Price.AmountNoVat,
            validationResponse.Price.VATAmount ? validationResponse.Price.VATAmount.toFixed(2).toLocaleString().replace(".", ",") : validationResponse.Price.VATAmount);
        
        if (!vm.IsShoppingCartEditMode()) {
            addToCart(cartItem, validationResponse, HandleAddToCart);
        }
        else {
            updateInCart(cartItem, validationResponse, HandleUpdateInCart);
        }
    }

    var HandleFreeText1 = function (addressObject) {
        var value = "";
        value = addressObject.Name();
        if (addressObject.CompanyName() !== "") {
            value = value + "<br/>";
            value = value + returnStringValue(addressObject.CompanyName());
        }

        value = value + "<br/>";
        value = value + returnStringValue(addressObject.AddressField1());
        value = value; 
        value = value + returnStringValue(addressObject.AddressField2());
        value = value; 
        value = value + returnStringValue(addressObject.ZipCode()) + returnStringValue(addressObject.City());
        value = value; 
        value = value + returnStringValue(addressObject.Country());

        return value;
    }

    var returnStringValue = function (obj) {
        var value = "";
        if (obj !== null && obj !== undefined) {
            value = obj + ', ';
        }
        return value;
    }

    var HandleFreeText2 = function () {
        var value = "";

        //Additional Services
        if (vm.SelectedProductSuggestion().SelectedAdditionalServices().length > 0) {
            value = value + " Tilläggstjänst: ";
            $.each(vm.SelectedProductSuggestion().SelectedAdditionalServices(), function (index, service) {
                value = ', ' + value + service.Name();
            });
        }
        value = value + "<br/>";

        //Size
        var sizeObject = vm.CurrentSize();
        value = sizeObject.Height() + 'x' + sizeObject.Length() + 'x' + sizeObject.Width();

        //Weight
        var weightObject = skickaSelectServiceVM.CurrentWeight().CalculatedWeight();
        if (weightObject < 1000) {
            weightObject = weightObject + ' g';
        }
        else {
            weightObject = weightObject / 1000 + ' kg';
        }
        value = value + ', ' + weightObject;

        return value;
    }

    var HandleAddToCart = function (basketOrderNumber, validationResponse) {
        var serviceOrderItem = new ServiceOrderItemBE(validationResponse);
        var ispServiceOrderItem = new ISPServiceOrderItemBE(
            serviceOrderItem,
            ko.toJS(formatAddress(vm.SenderAddress())),
            ko.toJS(formatAddress(vm.ReceiverAddress())),
            validationResponse,
            FormatSize(ko.toJS(vm.CurrentSize)),            
            skickaSelectServiceVM.CurrentWeight().CalculatedWeight(),
            vm.Account().Amount(),
            vm.Account().AccountType(),
            vm.Account().AccountNumber(),
            vm.Account().Reference());

        var receiverDestinationId = vm.SelectedDestination().ISO;
        informPutInCart(basketOrderNumber, ispServiceOrderItem, basketOrderNumber, receiverDestinationId, HandleInformPutInCart, vm.ShoppingCartScrollTo);
    }

    var HandleUpdateInCart = function (newBasketOrderNumber, oldBasketOrderNumber, validationResponse) {
        var serviceOrderItem = new ServiceOrderItemBE(validationResponse);

        var ispServiceOrderItem = new ISPServiceOrderItemBE(
            serviceOrderItem,
            ko.toJS(formatAddress(vm.SenderAddress())),
            ko.toJS(formatAddress(vm.ReceiverAddress())),
            validationResponse,
            FormatSize(ko.toJS(vm.CurrentSize)),            
            skickaSelectServiceVM.CurrentWeight().CalculatedWeight(),
            vm.Account().Amount(),
            vm.Account().AccountType(),
            vm.Account().AccountNumber(),
            vm.Account().Reference());

        var receiverDestinationId = vm.SelectedDestination().ISO;
        updatePutInCart(newBasketOrderNumber, ispServiceOrderItem, oldBasketOrderNumber, receiverDestinationId, HandleUpdatePutInCart, vm.ShoppingCartScrollTo);
    }

    var HandleInformPutInCart = function (addOrderToCartResult, basketOrderNumber) {
        AddOrderNumberToLocalStorage(basketOrderNumber);
        vm.HandleGetServiceOrderNumbersForBasket();
        retrievePrice();
        // Hide in progress sign
        vm.ShowProgress(false);
        skickaReceiverVM.ResetReceiverAddress();
    }

    var HandleUpdatePutInCart = function (oldOrderNumber, newOrderNumber) {
        removeLocalConsignment(oldOrderNumber);
        AddOrderNumberToLocalStorage(newOrderNumber);
        vm.HandleGetServiceOrderNumbersForBasket();
        vm.IsShoppingCartEditMode(false);

        retrievePrice();

        // Hide in progress sign
        vm.ShowProgress(false);
        skickaReceiverVM.ResetReceiverAddress();
    }

    vm.HandleGetServiceOrderNumbersForBasket = function () {
        // Show progress image
        GetConsignmentsFromBasket(HandleGetConsignments, vm.ShoppingCartProgress);
    }

    var HandleGetConsignments = function (cartResult, consignmentResult) {
        var consignments = [];
        var consignmentIndex = 0;
        $.each(consignmentResult, function (index, value) {
            var cartItem;
            $.each(cartResult, function (index, item) {
                if (item.OrderNumber === value.OrderNumber) {
                    cartItem = item;
                    consignmentIndex++;
                }
            });
            tc = cartItem;
            var consignment = new ConsignmentBE(
               value.AccountNumber,
               GetAdditionalServices(value.AdditionalServices),
               value.AdditionalInformation,
               value.Amount,
               value.BaseServiceLeveransId,
               value.BookingId,
               value.CODAmount,
               value.DestinationID,
               value.Diameter,              
               value.FromAddress,            
               value.Height,
               value.Length,
               value.MarketplaceBookingID,
               value.PaymentReference,
               value.Plusgiro,
               value.ProductCode,
               value.ProductName,
               value.ServiceCode,
               value.ToAddress,    
               value.Weight,
               value.Width,
               value.OrderNumber,
               cartItem.Total,
               cartItem.FreeTextField1,
               cartItem.FreeTextField2,
               consignmentIndex);
            tc = consignment;
            consignments.push(consignment);
        });

        vm.ShoppingCartList(consignments);
        // Hide progress image
        vm.ShoppingCartProgress(false);

    }

    var ToAddressBE = function (address) {
        var mobileSplit = address.Mobile.split(",");
        var phoneSplit = address.Phone.split(",");

        var result = new AddressBE(
            address.Company && address.Company != "",
            address.Name,
            address.Company,
            address.OrganizationNumber,
            address.AddressField1,
            address.AddressField2,
            address.ZipCode,
            address.City,
            address.Country,
            address.Email,
            address.Email,
            mobileSplit.length > 1 ? mobileSplit[0].replace("+", "") : "",
            mobileSplit.length > 1 ? mobileSplit[1] : mobileSplit[0],
            phoneSplit.length > 1 ? phoneSplit[0].replace("+", "") : "",
            phoneSplit.length > 1 ? phoneSplit[1] : phoneSplit[0],
            address.DoorCode);

        return result;
    }

    var GetAdditionalServices = function (services) {
        var additionalServices = [];

        if (services) {
            for (var i = 0, j = services.length; i < j; i++) {
                additionalServices.push(ToAdditionalServiceBE(services[i]));
            }
        }

        return additionalServices;
    }

    var ToAdditionalServiceBE = function (service) {
        var result = new AdditionalServiceBE(
            service.AdditionalServiceCode,
            service.Name,
            service.CompanyCode,
            ToPriceBE(service.Price));
        return result;
    }

    var ToPriceBE = function (price) {
        var result = new PriceBE(
            price.Amount,
            price.VATPercentage,
            price.AmountNoVat,
            price.Currency,
            price.VatCode,//MVU7734 - S
            price.VATAmount);//MVU7734 - S
        return result;
    }

    var CmStringToMmString = function (value) {
        return (Math.round(parseFloat(value.replace(',', '.')) * 100) / 10).toString();
    };

    /* ------------------------------- Subscriptions ------------------------------------------*/

    return vm;
})();

function AddOrderNumberToLocalStorage(basketOrderNumber) {
    var orderNumbers = ReturnOrderNumbersFromLocalStorage();
    orderNumbers.push(basketOrderNumber);

    setLocalValue(basketOrderNumberKey, JSON.stringify(orderNumbers));
}

function ReturnOrderNumbersFromLocalStorage() {
    var orderNumbers = returnLocalValue(basketOrderNumberKey);
    if (orderNumbers !== "" && orderNumbers !== null) {
        orderNumbers = JSON.parse(orderNumbers);
    }
    else {
        orderNumbers = [];
    };

    return orderNumbers;
}

