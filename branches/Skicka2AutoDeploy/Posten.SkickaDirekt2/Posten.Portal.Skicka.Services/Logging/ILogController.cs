﻿namespace Posten.Portal.Skicka.Services.Logging
{
    using System;

    public interface ILogController
    {
        #region Methods

        /// <summary>
        /// Log an unexpected error
        /// </summary>
        /// <param name="transactionLogID">Transaction Log ID</param>
        /// <param name="message">message to log</param>
        string LogError(string transactionLogID, string message);

        /// <summary>
        /// LogError
        /// </summary>
        /// <param name="message">Message to log</param>
        string LogError(string message);

        /// <summary>
        /// LogError
        /// </summary>
        /// <param name="exception">Exception to log</param>
        string LogError(Exception exception);

        /// <summary>
        /// Log a Medium Message
        /// </summary>
        /// <param name="message">Message to log</param>
        string LogMessage(string message);

        /// <summary>
        /// Log a medium message
        /// </summary>
        /// <param name="message">message to log with transaction log ID</param>
        /// <param name="transactionLogID">transaction log ID</param>
        string LogMessage(string transactionLogID, string message);

        /// <summary>
        /// Log a Verbose Message
        /// </summary>
        /// <param name="message">Message to log</param>
        string LogVerbose(string message);

        /// <summary>
        /// Log a Verbose message
        /// </summary>
        /// <param name="transactionLogID">Transaction Log ID</param>
        /// <param name="message">message to log</param>
        string LogVerbose(string transactionLogID, string message);

        /// <summary>
        /// Unregisters the internal diagnostics service
        /// </summary>
        void Unregister();

        #endregion Methods
    }
}