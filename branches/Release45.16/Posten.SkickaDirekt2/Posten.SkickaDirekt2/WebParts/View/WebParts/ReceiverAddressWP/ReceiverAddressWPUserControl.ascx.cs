﻿namespace Posten.SkickaDirekt2.WebParts.UserControls
{
    using System;
    using System.Web.UI;

    using Microsoft.SharePoint.WebControls;

    using Posten.SkickaDirekt2.CustomControls;
    using Posten.SkickaDirekt2.Utilities;

    public partial class ReceiverAddressWPUserControl : UserControl
    {
        #region Fields

        private string address2LabelProperty;
        private string address2TooltipProperty;
        private string addressLabelProperty;
        private string cityLabelProperty;
        private string cityPlaceholderProperty;
        private string companyLabelProperty;
        private string companyNameLabelProperty;
        private string email2LabelProperty;
        private string emailLabelProperty;
        private string emailTooltipProperty;
        private string entryCodeLabelProperty;
        private string headerProperty;
        private string mobilePhoneLabelProperty;
        private string mobilePhoneTooltipProperty;
        private string nameLabelProperty;
        private string privateLabelProperty;
        private string receiverTypeLabelProperty;
        private string telephoneLabelProperty;
        private string telephoneTooltipProperty;
        private string zipLabelProperty;

        #endregion Fields

        #region Properties

        public string Address2LabelProperty
        {
            get
            {
                return address2LabelProperty;
            }
            set
            {
                address2LabelProperty = value;
            }
        }

        public string Address2TooltipProperty
        {
            get
            {
                return address2TooltipProperty;
            }
            set
            {
                address2TooltipProperty = value;
            }
        }

        public string AddressLabelProperty
        {
            get
            {
                return addressLabelProperty;
            }
            set
            {
                addressLabelProperty = value;
            }
        }

        public string CityLabelProperty
        {
            get
            {
                return cityLabelProperty;
            }
            set
            {
                cityLabelProperty = value;
            }
        }

        public string CityPlaceholderProperty
        {
            get
            {
                return cityPlaceholderProperty;
            }
            set
            {
                cityPlaceholderProperty = value;
            }
        }

        public string CompanyLabelProperty
        {
            get
            {
                return companyLabelProperty;
            }
            set
            {
                companyLabelProperty = value;
            }
        }

        public string CompanyNameLabelProperty
        {
            get
            {
                return companyNameLabelProperty;
            }
            set
            {
                companyNameLabelProperty = value;
            }
        }

        public string Email2LabelProperty
        {
            get
            {
                return email2LabelProperty;
            }
            set
            {
                email2LabelProperty = value;
            }
        }

        public string Email2TooltipProperty
        {
            get
            {
                return emailTooltipProperty;
            }
            set
            {
                emailTooltipProperty = value;
            }
        }

        public string EmailLabelProperty
        {
            get
            {
                return emailLabelProperty;
            }
            set
            {
                emailLabelProperty = value;
            }
        }

        public string EntryCodeLabelProperty
        {
            get
            {
                return entryCodeLabelProperty;
            }
            set
            {
                entryCodeLabelProperty = value;
            }
        }

        public string HeaderProperty
        {
            get
            {
                return headerProperty;
            }
            set
            {
                headerProperty = value;
            }
        }

        public string MobilePhoneLabelProperty
        {
            get
            {
                return mobilePhoneLabelProperty;
            }
            set
            {
                mobilePhoneLabelProperty = value;
            }
        }

        public string MobilePhoneTooltipProperty
        {
            get
            {
                return mobilePhoneTooltipProperty;
            }
            set
            {
                mobilePhoneTooltipProperty = value;
            }
        }

        public string NameLabelProperty
        {
            get
            {
                return nameLabelProperty;
            }
            set
            {
                nameLabelProperty = value;
            }
        }

        public ReceiverAddressWP ParentWebPart
        {
            get;
            set;
        }

        public string PrivateLabelProperty
        {
            get
            {
                return privateLabelProperty;
            }
            set
            {
                privateLabelProperty = value;
            }
        }

        public string ReceiverTypeLabelProperty
        {
            get
            {
                return receiverTypeLabelProperty;
            }
            set
            {
                receiverTypeLabelProperty = value;
            }
        }

        public string TelephoneLabelProperty
        {
            get
            {
                return telephoneLabelProperty;
            }
            set
            {
                telephoneLabelProperty = value;
            }
        }

        public string TelephoneTooltipProperty
        {
            get
            {
                return telephoneTooltipProperty;
            }
            set
            {
                telephoneTooltipProperty = value;
            }
        }

        public string ZipLabelProperty
        {
            get
            {
                return zipLabelProperty;
            }
            set
            {
                zipLabelProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        protected override void OnInit(EventArgs e)
        {
            // Register CSS file in the page header
            UIHelper.RegisterCSSInPageHeader(this.Page, "ReceiverAddressWP.css");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            saveButton.ServerClick += saveButton_ServerClick;

            if (Microsoft.SharePoint.SPContext.Current.FormContext.FormMode == SPControlMode.Display)
            {
                // Set properties
                ((ReceiverAddressWPDisplay)this.displayControl).HeaderProperty = this.ParentWebPart.HeaderProperty;
                ((ReceiverAddressWPDisplay)this.displayControl).CompanyNameLabelProperty = this.ParentWebPart.CompanyNameLabelProperty;
                ((ReceiverAddressWPDisplay)this.displayControl).NameLabelProperty = this.ParentWebPart.NameLabelProperty;
                ((ReceiverAddressWPDisplay)this.displayControl).AddressLabelProperty = this.ParentWebPart.AddressLabelProperty;
                ((ReceiverAddressWPDisplay)this.displayControl).Address2LabelProperty = this.ParentWebPart.Address2LabelProperty;
                ((ReceiverAddressWPDisplay)this.displayControl).ZipLabelProperty = this.ParentWebPart.ZipLabelProperty;
                ((ReceiverAddressWPDisplay)this.displayControl).EmailLabelProperty = this.ParentWebPart.EmailLabelProperty;
                ((ReceiverAddressWPDisplay)this.displayControl).Email2LabelProperty = this.ParentWebPart.Email2LabelProperty;
                ((ReceiverAddressWPDisplay)this.displayControl).MobilePhoneLabelProperty = this.ParentWebPart.MobilePhoneLabelProperty;
                ((ReceiverAddressWPDisplay)this.displayControl).ReceiverTypeLabelProperty = this.ParentWebPart.ReceiverTypeLabelProperty;
                ((ReceiverAddressWPDisplay)this.displayControl).PrivateLabelProperty = this.ParentWebPart.PrivateLabelProperty;
                ((ReceiverAddressWPDisplay)this.displayControl).CompanyLabelProperty = this.ParentWebPart.CompanyLabelProperty;
                ((ReceiverAddressWPDisplay)this.displayControl).CityPlaceholderProperty = this.ParentWebPart.CityPlaceholderProperty;
                ((ReceiverAddressWPDisplay)this.displayControl).CityLabelProperty = this.ParentWebPart.CityLabelProperty;
                ((ReceiverAddressWPDisplay)this.displayControl).Address2TooltipProperty = this.ParentWebPart.Address2TooltipProperty;
                ((ReceiverAddressWPDisplay)this.displayControl).EmailTooltipProperty = this.ParentWebPart.EmailTooltipProperty;
                ((ReceiverAddressWPDisplay)this.displayControl).MobilePhoneTooltipProperty = this.ParentWebPart.MobilePhoneTooltipProperty;
                ((ReceiverAddressWPDisplay)this.displayControl).EntryCodeLabelProperty = this.ParentWebPart.EntryCodeLabelProperty;
                ((ReceiverAddressWPDisplay)this.displayControl).TelephoneTooltipProperty = this.ParentWebPart.TelephoneTooltipProperty;
                ((ReceiverAddressWPDisplay)this.displayControl).TelephoneLabelProperty = this.ParentWebPart.TelephoneLabelProperty;

                // Show correct control
                this.displayControl.Visible = true;
                this.editControl.Visible = false;
                this.saveButton.Visible = false;
            }
            else if (Microsoft.SharePoint.SPContext.Current.FormContext.FormMode == SPControlMode.Edit)
            {
                // Set properties
                ((ReceiverAddressWPEdit)this.editControl).HeaderProperty = this.ParentWebPart.HeaderProperty;
                ((ReceiverAddressWPEdit)this.editControl).CompanyNameLabelProperty = this.ParentWebPart.CompanyNameLabelProperty;
                ((ReceiverAddressWPEdit)this.editControl).NameLabelProperty = this.ParentWebPart.NameLabelProperty;
                ((ReceiverAddressWPEdit)this.editControl).AddressLabelProperty = this.ParentWebPart.AddressLabelProperty;
                ((ReceiverAddressWPEdit)this.editControl).Address2LabelProperty = this.ParentWebPart.Address2LabelProperty;
                ((ReceiverAddressWPEdit)this.editControl).ZipLabelProperty = this.ParentWebPart.ZipLabelProperty;
                ((ReceiverAddressWPEdit)this.editControl).EmailLabelProperty = this.ParentWebPart.EmailLabelProperty;
                ((ReceiverAddressWPEdit)this.editControl).Email2LabelProperty = this.ParentWebPart.Email2LabelProperty;
                ((ReceiverAddressWPEdit)this.editControl).MobilePhoneLabelProperty = this.ParentWebPart.MobilePhoneLabelProperty;
                ((ReceiverAddressWPEdit)this.editControl).ReceiverTypeLabelProperty = this.ParentWebPart.ReceiverTypeLabelProperty;
                ((ReceiverAddressWPEdit)this.editControl).PrivateLabelProperty = this.ParentWebPart.PrivateLabelProperty;
                ((ReceiverAddressWPEdit)this.editControl).CompanyLabelProperty = this.ParentWebPart.CompanyLabelProperty;
                ((ReceiverAddressWPEdit)this.editControl).CityPlaceholderProperty = this.ParentWebPart.CityPlaceholderProperty;
                ((ReceiverAddressWPEdit)this.editControl).CityLabelProperty = this.ParentWebPart.CityLabelProperty;
                ((ReceiverAddressWPEdit)this.editControl).Address2TooltipProperty = this.ParentWebPart.Address2TooltipProperty;
                ((ReceiverAddressWPEdit)this.editControl).EmailTooltipProperty = this.ParentWebPart.EmailTooltipProperty;
                ((ReceiverAddressWPEdit)this.editControl).MobilePhoneTooltipProperty = this.ParentWebPart.MobilePhoneTooltipProperty;
                ((ReceiverAddressWPEdit)this.editControl).EntryCodeLabelProperty = this.ParentWebPart.EntryCodeLabelProperty;
                ((ReceiverAddressWPEdit)this.editControl).TelephoneTooltipProperty = this.ParentWebPart.TelephoneTooltipProperty;
                ((ReceiverAddressWPEdit)this.editControl).TelephoneLabelProperty = this.ParentWebPart.TelephoneLabelProperty;

                // Show correct control
                this.editControl.Visible = true;
                this.saveButton.Visible = true;
                this.displayControl.Visible = false;
            }
        }

        private void saveButton_ServerClick(object sender, EventArgs e)
        {
            this.ParentWebPart.HeaderProperty = ((ReceiverAddressWPEdit)this.editControl).HeaderProperty;
            this.ParentWebPart.CompanyNameLabelProperty = ((ReceiverAddressWPEdit)this.editControl).CompanyNameLabelProperty;
            this.ParentWebPart.NameLabelProperty = ((ReceiverAddressWPEdit)this.editControl).NameLabelProperty;
            this.ParentWebPart.AddressLabelProperty = ((ReceiverAddressWPEdit)this.editControl).AddressLabelProperty;
            this.ParentWebPart.Address2LabelProperty = ((ReceiverAddressWPEdit)this.editControl).Address2LabelProperty;
            this.ParentWebPart.ZipLabelProperty = ((ReceiverAddressWPEdit)this.editControl).ZipLabelProperty;
            this.ParentWebPart.EmailLabelProperty = ((ReceiverAddressWPEdit)this.editControl).EmailLabelProperty;
            this.ParentWebPart.Email2LabelProperty = ((ReceiverAddressWPEdit)this.editControl).Email2LabelProperty;
            this.ParentWebPart.MobilePhoneLabelProperty = ((ReceiverAddressWPEdit)this.editControl).MobilePhoneLabelProperty;
            this.ParentWebPart.ReceiverTypeLabelProperty = ((ReceiverAddressWPEdit)this.editControl).ReceiverTypeLabelProperty;
            this.ParentWebPart.PrivateLabelProperty = ((ReceiverAddressWPEdit)this.editControl).PrivateLabelProperty;
            this.ParentWebPart.CompanyLabelProperty = ((ReceiverAddressWPEdit)this.editControl).CompanyLabelProperty;
            this.ParentWebPart.CityPlaceholderProperty = ((ReceiverAddressWPEdit)this.editControl).CityPlaceholderProperty;
            this.ParentWebPart.CityLabelProperty = ((ReceiverAddressWPEdit)this.editControl).CityLabelProperty;
            this.ParentWebPart.Address2TooltipProperty = ((ReceiverAddressWPEdit)this.editControl).Address2TooltipProperty;
            this.ParentWebPart.EmailTooltipProperty = ((ReceiverAddressWPEdit)this.editControl).EmailTooltipProperty;
            this.ParentWebPart.MobilePhoneTooltipProperty = ((ReceiverAddressWPEdit)this.editControl).MobilePhoneTooltipProperty;
            this.ParentWebPart.EntryCodeLabelProperty = ((ReceiverAddressWPEdit)this.editControl).EntryCodeLabelProperty;
            this.ParentWebPart.TelephoneTooltipProperty = ((ReceiverAddressWPEdit)this.editControl).TelephoneTooltipProperty;
            this.ParentWebPart.TelephoneLabelProperty = ((ReceiverAddressWPEdit)this.editControl).TelephoneLabelProperty;

            this.ParentWebPart.SaveProperties();
        }

        #endregion Methods
    }
}