﻿namespace Posten.Portal.Skicka.Services.BusinessEntities
{
    using System;
    using System.Linq;
    using System.Runtime.Serialization;

    using Posten.Portal.Skicka.PVT.Proxy;
    using Posten.Portal.Skicka.Services.Utils;

    /// <summary>
    /// Contains information about the destination.
    /// </summary>
    [Serializable]
    [DataContract]
    public class DestinationsBE : BaseBE
    {
        #region Constructors

        public DestinationsBE()
        {
        }

        public DestinationsBE(DestinationResponse destinationResponse)
            : base(destinationResponse.status, destinationResponse.statusMsg, destinationResponse.responseInfos)
        {
            if (destinationResponse.status == responseStatus.SUCCESS)
            {
                this.Destinations = Array.ConvertAll(
                    destinationResponse.destinations,
                    d => this.TranslateDestinationToDestinationBE(d));

                this.Destinations = this.Destinations.Where(o => o != null).ToArray();
            }
        }

        #endregion Constructors

        #region Properties

        [DataMember]
        public DestinationBE[] Destinations
        {
            get;
            set;
        }

        #endregion Properties

        #region Methods

        private DestinationBE TranslateDestinationToDestinationBE(destination destination)
        {
            try
            {
                return new DestinationBE(destination);
            }
            catch (Exception ex)
            {
                LogUtility.LogCriticalException(ex, this.GetType(), System.Reflection.MethodBase.GetCurrentMethod().Name);

                return null;
            }
        }

        #endregion Methods
    }
}