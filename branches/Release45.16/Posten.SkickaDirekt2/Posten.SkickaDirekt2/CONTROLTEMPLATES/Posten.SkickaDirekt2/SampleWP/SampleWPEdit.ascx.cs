﻿namespace Posten.SkickaDirekt2.CustomControls
{
    using System;
    using System.Web.UI;

    public partial class SampleWPEdit : UserControl
    {
        #region Fields

        private string sampleProperty;

        #endregion Fields

        #region Properties

        public string SampleProperty
        {
            get
            {
                return propertyEdit.Value;
            }
            set
            {
                sampleProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(propertyEdit.Value))
            {
                propertyEdit.Value = sampleProperty;
            }
        }

        #endregion Methods
    }
}