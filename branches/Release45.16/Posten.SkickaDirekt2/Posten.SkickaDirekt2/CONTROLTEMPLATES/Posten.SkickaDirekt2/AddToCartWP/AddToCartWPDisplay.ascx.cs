﻿namespace Posten.SkickaDirekt2.CustomControls
{
    using System;
    using System.Web.UI;

    public partial class AddToCartWPDisplay : UserControl
    {
        #region Fields

        private string accountValidationProperty;
        private string addToCartButtonProperty;
        private string receiverValidationProperty;
        private string senderValidationProperty;

        private string confirmationUrlProperty;
        private string waybillUrlProperty;
        private string confirmationPageUrlProperty;
        private string applicationUrlProperty;
        private string editUrlProperty;
        private string produceUrlProperty;

        #endregion Fields

        #region Properties
       
        public string ConfirmationUrlProperty
        {
            get
            {
                return confirmationUrlProperty;
            }
            set
            {
                confirmationUrlProperty = value;
            }
        }

       
        public string WaybillUrlProperty
        {
            get
            {
                return waybillUrlProperty;
            }
            set
            {
                waybillUrlProperty = value;
            }
        }

       
        public string ConfirmationPageUrlProperty
        {
            get
            {
                return confirmationPageUrlProperty;
            }
            set
            {
                confirmationPageUrlProperty = value;
            }
        }

       
        public string ApplicationUrlProperty
        {
            get
            {
                return applicationUrlProperty;
            }
            set
            {
                applicationUrlProperty = value;
            }
        }

       
        public string EditUrlProperty
        {
            get
            {
                return editUrlProperty;
            }
            set
            {
                editUrlProperty = value;
            }
        }

        
        public string ProduceUrlProperty
        {
            get
            {
                return produceUrlProperty;
            }
            set
            {
                produceUrlProperty = value;
            }
        }

        public string AccountValidationProperty
        {
            get
            {
                return accountValidationProperty;
            }
            set
            {
                accountValidationProperty = value;
            }
        }

        public string AddToCartButtonProperty
        {
            get
            {
                return addToCartButtonProperty;
            }
            set
            {
                addToCartButtonProperty = value;
            }
        }

        public string ReceiverValidationProperty
        {
            get
            {
                return receiverValidationProperty;
            }
            set
            {
                receiverValidationProperty = value;
            }
        }

        public string SenderValidationProperty
        {
            get
            {
                return senderValidationProperty;
            }
            set
            {
                senderValidationProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            this.addToCartButton.Value = addToCartButtonProperty;
            this.accountValidationPropertyDisplay.InnerText = accountValidationProperty;
            this.senderValidationPropertyDisplay.InnerText = senderValidationProperty;
            this.recevicerValidationPropertyDisplay.InnerText = receiverValidationProperty;
        }

        #endregion Methods
    }
}