﻿namespace Posten.Portal.Skicka.Services.Utils
{
    public class ConstantsAndEnums
    {
        #region Fields

        public const string TransactionLogID = "TransactionLogID";
        public const string CorrelationID = "CorrelationID";
        public static string SkickaMarketPlaceID = "posten.se";
        public static string SkickaZipCodeServiceUrlAppKey = "SkickaZipCodeServiceUrl";

        #endregion Fields

        #region Enumerations

        public enum Characteristics
        {
            FAST,
            RELIABLE,
            SEARCHABLE
        }

        #endregion Enumerations
    }
}