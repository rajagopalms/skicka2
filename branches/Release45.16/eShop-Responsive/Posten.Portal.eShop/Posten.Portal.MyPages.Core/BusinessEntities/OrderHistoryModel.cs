﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Posten.Portal.MyPages.BusinessEntities
{
    public class OrderHistoryModel
    {
        public string OrderNumber { get; set; }

        public DateTime OrderDate { get; set; }

        public decimal OrderAmount { get; set; }

        public string Status { get; set; }

        public string StatusDescription { get; set; }
    }
}
