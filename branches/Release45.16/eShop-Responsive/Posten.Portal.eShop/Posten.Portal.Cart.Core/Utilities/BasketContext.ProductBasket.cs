﻿using System;
using System.Linq;
using System.Collections.Generic;
using Posten.Portal.Cart.BusinessEntities;
using Posten.Portal.Cart.Services;
using Posten.Portal.Cart.Utilities;
using Posten.Portal.Platform.Common.Container;

namespace Posten.Portal.Cart
{
    // Implements the ProductBasket methods
    public partial class BasketContext
    {
        private IProductBasketService productBasket;
        private ICollection<IProductOrderItem> productOrderItems;
        

        /// <summary>
        /// Returns only ProductOrder Items which are pruchsed, product items for invoice fees etc are not returned.
        /// </summary>
        public ICollection<IProductOrderItem> ProductOrderItems
        {
            get
            {
                if (this.productOrderItems == null)
                {
                    this.EnsureBasket();

                    // only call the web-service if the basket exists ensure that fees are not included.
                    this.productOrderItems = this.IsEmpty ? new List<IProductOrderItem>() : (from items in this.ProductBasket.GetProductOrders(this.BasketId.ToString()) select items).ToList();
                }

                return this.productOrderItems;
            }
        }

       
        private IProductBasketService ProductBasket
        {
            get
            {
                if (this.productBasket == null)
                {
                    this.productBasket = IoC.Resolve<IProductBasketService>();
                }

                return this.productBasket;
            }
        }

        public string AddProductOrder(IProductOrderItem productOrder)
        {
            Utils.CheckNullValue("productOrder", productOrder);
            this.EnsureBasket();

            string newBasketId = this.BasketId.ToString();
            string orderNumber = string.Empty;

            // TODO: handle exception
            var result = this.ProductBasket.AddProductOrder(ref newBasketId, productOrder, ref orderNumber);

            // always refresh the cookie when we add new products to the basket
            BasketCookie.CurrentBasketId = this.basketId = newBasketId.ToGuid();

            this.InvalidateCache();

            return orderNumber;
        }

       
        public IProductOrderItem GetProductOrder(string orderNumber)
        {
            Utils.CheckNullValue("orderNumber", orderNumber);
            this.EnsureBasket();

            if (this.IsEmpty)
            {
                return null;
            }

            // TODO: handle exception
            return this.ProductBasket.GetProductOrderItem(this.BasketId.ToString(), orderNumber);
        }

        public string UpdateProductOrder(string orderNumber, IProductOrderItem productOrder)
        {
            Utils.CheckNullValue("orderNumber", orderNumber);
            Utils.CheckNullValue("productOrder", productOrder);
            this.EnsureBasket();

            // remove from method parameter?
            productOrder.OrderNumber = orderNumber;

            // TODO: handle exception
            var result = this.ProductBasket.UpdateProductOrder(this.BasketId.ToString(), productOrder);

            this.InvalidateCache();

            return result;
        }

        public bool RemoveProductOrder(string orderNumber)
        {
            Utils.CheckNullValue("orderNumber", orderNumber);
            this.EnsureBasket();

            if (this.IsEmpty)
            {
                return true;
            }

            var result = this.ProductBasket.RemoveProductOrder(this.BasketId.ToString(), orderNumber);

            this.InvalidateCache();

            return result;
        }

        
    }
}
