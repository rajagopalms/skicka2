﻿namespace Posten.Portal.Cart.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Posten.Portal.Cart.BusinessEntities;

    public interface ICartSettingsService
    {
        #region Methods

        bool AddBlockedSSn(string ssn);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        CartSettingItems GetItems();

        bool RemoveBlockedSSn(string ssn);

        bool RemoveBlockService(string id);

        bool SaveBlockService(string id, string name);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="items"></param>
        bool SaveItems(CartSettingItems items);

        #endregion Methods
    }
}