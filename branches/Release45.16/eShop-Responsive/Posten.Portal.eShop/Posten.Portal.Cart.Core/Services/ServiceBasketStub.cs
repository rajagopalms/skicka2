﻿namespace Posten.Portal.Cart.Services
{
    using System;
    using System.Collections.Generic;

    using Posten.Portal.Cart.BusinessEntities;

    public class ServiceBasketStub : IServiceBasketService
    {
        #region Methods

        // List which holds added ordernumbers
        private static List<ServiceOrderItem> serviceOrdersNumbers = new List<ServiceOrderItem>();

        public bool AddServiceOrder(ref string basketId, IServiceOrderItem serviceOrderItem, ref string orderNumber)
        {
            basketId = Guid.NewGuid().ToString();
            orderNumber = Guid.NewGuid().ToString();

            serviceOrdersNumbers.Add(new ServiceOrderItem() { OrderNumber = orderNumber });

            return true;
        }

        public IServiceOrderItem GetServiceOrderItem(string basketId, string serviceOrderNumber)
        {
            throw new NotImplementedException();
        }

        public ICollection<IServiceOrderItem> GetServiceOrders(string basketId)
        {
            return serviceOrdersNumbers.ToArray();
        }

        public ICollection<IServiceOrderItem> GetServiceOrdersByBookingId(string bookingId)
        {
            throw new NotImplementedException();
        }

        public ICollection<IServiceOrderItem> GetServiceOrdersByTransactionId(long transactionId)
        {
            throw new NotImplementedException();
        }

        public bool RemoveServiceOrder(string basketId, string serviceOrderNumber)
        {
            throw new NotImplementedException();
        }

        public string UpdateServiceOrder(string basketId, IServiceOrderItem serviceOrderItem)
        {
            throw new NotImplementedException();
        }

        #endregion Methods
    }
}