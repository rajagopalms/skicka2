﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Posten.Portal.Cart.Presentation.ViewData
{
    public class MessageResults
    {
        public int code { get; set; }

        public string message { get; set; }
    }

    public enum AccountType
    {
        Private = 0,
        Company,
        Premium
    }
}
