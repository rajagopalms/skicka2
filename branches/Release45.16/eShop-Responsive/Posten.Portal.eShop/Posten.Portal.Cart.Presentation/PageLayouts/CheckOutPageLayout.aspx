﻿<%@ Page language="C#" Inherits="Microsoft.SharePoint.Publishing.PublishingLayoutPage,Microsoft.SharePoint.Publishing,Version=14.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePointWebControls" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="PublishingWebControls" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="PublishingNavigation" Namespace="Microsoft.SharePoint.Publishing.Navigation" Assembly="Microsoft.SharePoint.Publishing, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="PostenWebControls" Namespace="Posten.Portal.Platform.Common.Presentation.Controls" Assembly="Posten.Portal.Platform.Common.Presentation, Version=3.0.0.0, Culture=neutral, PublicKeyToken=9f4da00116c38ec5" %>
<%@ Register TagPrefix="uc" TagName="CheckOut" Src="~/_CONTROLTEMPLATES/Posten/Cart/2.0/CheckOut.ascx" %>
<%@ Import Namespace="Posten.Portal.Cart.Presentation" %>

<asp:Content ContentPlaceholderID="PlaceHolderAdditionalPageHead" runat="server">	
    <SharePointWebControls:CssRegistration ID="CssRegistration1" name="<% $SPUrl:~sitecollection/Style Library/~language/Core Styles/page-layouts-21.css %>" runat="server"/>
	<PublishingWebControls:EditModePanel ID="EditModePanel1" runat="server">
		<!-- Styles for edit mode only-->
		<SharePointWebControls:CssRegistration ID="CssRegistration2" name="<% $SPUrl:~sitecollection/Style Library/~language/Core Styles/edit-mode-21.css %>"
			After="<% $SPUrl:~sitecollection/Style Library/~language/Core Styles/page-layouts-21.css %>" runat="server"/>
	</PublishingWebControls:EditModePanel>
	<SharePointWebControls:CssRegistration ID="CssRegistration3" name="<% $SPUrl:~sitecollection/Style Library/~language/Core Styles/rca.css %>" runat="server"/>
	<SharePointWebControls:FieldValue id="PageStylesField" FieldName="HeaderStyleDefinitions" runat="server"/>
    <SharePointWebControls:CssRegistration ID="CssRegistration6" name="/_layouts/Posten/Cart/Presentation/styles/Cart.css" runat="server"/>
    <script type="text/javascript" src="http://code.jquery.com/jquery-git.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>    
 
</asp:Content>
<asp:Content ContentPlaceholderID="PlaceHolderPageTitle" runat="server">
	<SharePointWebControls:FieldValue id="PageTitle" FieldName="Title" runat="server"/>
</asp:Content>
<asp:Content ContentPlaceholderID="PlaceHolderPageTitleInTitleArea" runat="server">
	<SharePointWebControls:FieldValue ID="FieldValue1" FieldName="Title" runat="server"/>
</asp:Content>
<asp:Content ContentPlaceHolderId="PlaceHolderTitleBreadcrumb" runat="server">
</asp:Content>

<asp:Content ContentPlaceholderID="PlaceHolderMain" runat="server">
    <div class="checkOut-title"><%= Utils.GetResourceString("CheckOut_Page_Title") %></div>
    <div class="checkOut-subtitle">
        <a class="continue-shop" href="javascript:history.go(-1)"><%= Utils.GetResourceString("Continue_Shopping_Text") %></a>
        <div class="help">
            <strong><%= Utils.GetResourceString("Need_Help_Label") %></strong>&nbsp;<%= Utils.GetResourceString("Customer_Service_Label") %><strong>&nbsp; <%= Utils.GetResourceString("Customer_Service_Number_Text") %></strong>
        </div>
    </div>
    <div class="middle-zone">
        <uc:CheckOut id="ucCheckOut" runat="server" MinValue="1" MaxValue="10" />
    </div>
</asp:Content>