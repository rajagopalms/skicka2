﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Posten.Portal.MyPages.Services;
using Posten.Portal.Platform.Common.Container;
using System.Collections.Generic;

namespace Posten.Portal.MyPages.Presentation.ControlTemplates
{
    public partial class OrderHistoryUserControl : UserControl
    {
        public int OrdersToShow
        {
            get { return this.pagerHistory.PageSize; }
            set { this.pagerHistory.PageSize = value; }
        }

        public string HeaderText
        {
            get { return this.orderHistoryModule.HeaderText; }
            set { this.orderHistoryModule.HeaderText = value; }
        }

        public string FooterText
        {
            get { return this.orderHistoryModule.FooterText; }
            set { this.orderHistoryModule.FooterText = value; }
        }

        public string FooterUrl
        {
            get { return this.orderHistoryModule.FooterUrl; }
            set { this.orderHistoryModule.FooterUrl = value; }
        }

        public bool AllowPaging
        {
            get { return this.gvHistory.AllowPaging; }
            set { this.gvHistory.AllowPaging = value; }
        }

        public bool AllowSorting
        {
            get { return this.gvHistory.AllowSorting; }
            set { this.gvHistory.AllowSorting = value; }
        }

        public ICollection<BusinessEntities.OrderHistoryModel> OrderHistoryList
        {
            get
            {
                //check if User has orders if not hide Order history control
                // TODO: exception handling and caching
                var orderHistoryService = IoC.Resolve<IOrderHistoryService>();
                return orderHistoryService.GetOrderHistory(MyPagesContext.Current.Username);
            }
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            this.pagerHistory.Visible = this.AllowPaging;
            if (!Page.IsPostBack)
            {
                orderHistoryModule.Visible = (this.OrderHistoryList.Count > 0);
            }
        }

        protected void OrderHistorySelecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            // TODO: exception handling and caching
            var orderHistoryService = IoC.Resolve<IOrderHistoryService>();
            var orderHistoryList = orderHistoryService.GetOrderHistory(MyPagesContext.Current.Username);


            //Hide Pager if Order count is less than page size
            this.pagerHistory.Visible = (orderHistoryList.Count > this.OrdersToShow);



            if (!this.AllowPaging && this.OrdersToShow > 0)
            {
                e.Result = orderHistoryList.Take(this.OrdersToShow);
            }
            else
            {
                e.Result = orderHistoryList;
            }
        }
    }
}
