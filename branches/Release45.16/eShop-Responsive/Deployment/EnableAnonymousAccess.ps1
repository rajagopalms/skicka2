function enableAnonymousAccess($url)
{
	$webapp = Get-SPWebApplication $url
	$iis = $webapp.IisSettings.get_Item(0)
	$iis.AllowAnonymous = $true
	Write-Host "Allowing Anonymous Access - Done"
	$webapp.Update($true)
	Write-Host "Updating Web Application - Done"
	
	( Get-SPWebApplication $url | Get-SPSite | Get-SPWeb | Where {$_ -ne $null -and $_.HasUniqueRoleAssignments -eq $true } ) | ForEach-Object { $_.AnonymousState = [Microsoft.SharePoint.SPWeb+WebAnonymousState]::On; $_.Update(); }
	Write-Host "Allowing Anonymous Access to Site Collections - Done"
}

Add-PsSnapin Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue
$url = "http://" + $env:COMPUTERNAME
enableAnonymousAccess $url
