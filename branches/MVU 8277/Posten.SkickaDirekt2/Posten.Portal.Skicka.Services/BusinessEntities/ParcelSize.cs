﻿namespace Posten.Portal.Skicka.Services.BusinessEntities
{
    using System;
    using System.Runtime.Serialization;

    using Posten.Portal.Skicka.PVT.Proxy;

    [Serializable]
    [DataContract]
    public class ParcelSizeBE
    {
        #region Constructors

        public ParcelSizeBE(ParcelSize parcelSize)
        {
            this.DataAccessObjectToBusinessEntity(parcelSize);
        }

        public ParcelSizeBE()
        {
        }

        #endregion Constructors

        #region Properties

        [DataMember]
        public int Diameter
        {
            get;
            set;
        }

        [DataMember]
        public int Height
        {
            get;
            set;
        }

        [DataMember]
        public int Length
        {
            get;
            set;
        }

        [DataMember]
        public int RollLength
        {
            get;
            set;
        }

        [DataMember]
        public int Width
        {
            get;
            set;
        }

        #endregion Properties

        #region Methods

        public void DataAccessObjectToBusinessEntity(ParcelSize parcelSize)
        {
            this.Length = parcelSize.length;
            this.Width = parcelSize.width;
            this.Height = parcelSize.height;
            this.Diameter = parcelSize.diameter;
            this.RollLength = parcelSize.rollLength;
        }

        #endregion Methods
    }
}