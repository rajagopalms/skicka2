.\LoadConfig.ps1

$webApplicationUrl = $settings["webApplicationUrl"]
#$webApplicationClaimsUrl = $settings["webApplicationClaimsUrl"]
$webApplicationName = $settings["webApplicationName"]
#$webApplicationClaimsName = $settings["webApplicationClaimsName"]
$webApplicationPort = $settings["webApplicationPort"]
$webApplicationHostname = $settings["webApplicationHostname"]
#$webApplicationClaimsHostname = $settings["webApplicationClaimsHostname"]
$webApplicationPoolName = $settings["webApplicationPoolName"]
$webApplicationPoolAccountName = $settings["webApplicationPoolAccountName"]
$webApplicationDBServerName = $settings["webApplicationDBServerName"]
$webApplicationDBName = $settings["webApplicationDBName"]
#$webApplicationMembershipProvider = $settings["webApplicationMembershipProvider"]
#$webApplicationRoleProvider = $settings["webApplicationRoleProvider"]

echo "----------------------------------------------------------------------------"
echo "Creating new web application: " $webApplicationUrl
echo "----------------------------------------------------------------------------"
New-SPWebApplication -Name $webApplicationName -Port $webApplicationPort -URL $webApplicationUrl -ApplicationPool $webApplicationPoolName -ApplicationPoolAccount $webApplicationPoolAccountName -DatabaseName $webApplicationDBName -DatabaseServer $webApplicationDBServerName -HostHeader $webApplicationHostname

#$winAp = new-SPAuthenticationProvider -UseWindowsIntegratedAuthentication 
#-DisableKerberos
#$newWebApp = New-SPWebApplication -Name $webApplicationName -Port $webApplicationPort -HostHeader $webApplicationHostname -URL $webApplicationUrl -ApplicationPool $webApplicationPoolName -ApplicationPoolAccount (Get-SPManagedAccount $webApplicationPoolAccountName) -DatabaseName $webApplicationDBName -DatabaseServer $webApplicationDBServerName -AuthenticationProvider $winAp 
#$newWebApp.UseClaimsAuthentication = $true
#$newWebApp.Update()
#$newWebApp.MigrateUsers($true)

#$app = Get-SPWebApplication $webApplicationUrl
#$app.IisSettings.Keys | ForEach-Object { ($app.IisSettings[$_]).AllowAnonymous = $true; $app.Update(); $app.ProvisionGlobally(); }

#echo "----------------------------------------------------------------------------"
#echo "Extending the web application: " $webApplicationUrl " to internet zone at url: " $webApplicationClaimsUrl 
#echo "----------------------------------------------------------------------------"
#$fbaAp = new-SPAuthenticationProvider -ASPNETMembershipProvider $webApplicationMembershipProvider -ASPNETRoleProviderName $webApplicationRoleProvider
#Get-SPWebApplication -Identity $webApplicationUrl | New-SPWebApplicationExtension -Name $webApplicationClaimsName -HostHeader $webApplicationClaimsHostname -Zone Internet -URL $webApplicationClaimsUrl -Port $webApplicationPort -AuthenticationProvider $fbaAp

