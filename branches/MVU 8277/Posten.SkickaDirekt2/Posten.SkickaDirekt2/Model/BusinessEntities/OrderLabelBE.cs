﻿namespace Posten.Portal.Skicka.Services.BusinessEntities
{
    using System;

    [Serializable]
    public class OrderLabelBE
    {
        #region Properties

        public string CustomerNumber
        {
            get;
            set;
        }

        public string LabelType
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string ProdId
        {
            get;
            set;
        }

        public string Quantity
        {
            get;
            set;
        }

        #endregion Properties
    }
}