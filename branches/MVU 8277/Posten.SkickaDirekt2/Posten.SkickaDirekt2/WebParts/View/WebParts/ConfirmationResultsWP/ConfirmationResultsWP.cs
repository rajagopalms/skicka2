﻿namespace Posten.SkickaDirekt2.WebParts
{
    using System.ComponentModel;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls.WebParts;

    using Microsoft.SharePoint;
    using Microsoft.SharePoint.WebPartPages;

    using Posten.SkickaDirekt2.WebParts.Presenter;
    using Posten.SkickaDirekt2.WebParts.UserControls;
    using Posten.SkickaDirekt2.WebParts.View.Interfaces;

    [ToolboxItemAttribute(false)]
    public class ConfirmationResultsWP : System.Web.UI.WebControls.WebParts.WebPart
    {
        #region Fields

        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/Posten.SkickaDirekt2.WebParts.UserControls/ConfirmationResultsWP/ConfirmationResultsWPUserControl.ascx";

        
        #endregion Fields

        #region Constructors

        public ConfirmationResultsWP()
        {
           
        }

        #endregion Constructors

        #region Properties

        

        #endregion Properties

        #region Methods

       
        protected override void CreateChildControls()
        {
            ConfirmationResultsWPUserControl control = (ConfirmationResultsWPUserControl)Page.LoadControl(_ascxPath);           
            Controls.Add(control);
        }

        #endregion Methods
    }
}