﻿namespace Posten.Portal.SkickaDirekt2.WebServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;
    using System.ServiceModel.Activation;
    using System.Web;
    using System.Web.Script.Serialization;

    using Posten.Portal.Platform.Common.Container;
    using Posten.Portal.Skicka.Services.BusinessEntities;
    using Posten.Portal.Skicka.Services.PVT;
    using Posten.Portal.Skicka.Services.SkickaService;
    using Posten.Portal.Skicka.Services.Utils;
    using Posten.Portal.SkickaDirekt2.WebServices.Interfaces;
    using Posten.Portal.Skicka.Services.Cart;
    using System.Text.RegularExpressions;

    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class TransactionService : ITransactionService
    {
        #region Fields

        private PVTController pvtService;
        private ISkickaService skickaService;
        private CartAgent cartService;
         
        #endregion Fields

        #region Constructors

        public TransactionService()
        {            
          this.skickaService = new SkickaServiceAgent(HttpContext.Current);
           
            //this.cartService = new CartAgent();
        }

        #endregion Constructors

        #region Methods

        private string checkingInput(string input)
        {
            Regex CharReplace = new Regex(@"([<>${}[\]\""/\\'%;()&])");
            string resultChars = CharReplace.Replace(input, "");
            return resultChars;
        }

        /// <summary>
        /// Adds the selected service order item to the cart
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public void AddOrderToCart(
            string basketOrderNumber,
            ValidationResponseBE validationResponse,
            string serviceName,
            string serviceCode,
            SizeBE size,
            int weight,
            AddressBE fromAddress,
            AddressBE toAddress,
            string codAmount,
            bool plusgiro,
            string postforskottKontoNr,
            string postforskottPaymentReference,
            string orderconfirmationUrl,
            string urlText,
            string url,
            string details,
            string receiverDestinationId)
        {
            if (fromAddress.AddressField1 != null && fromAddress.AddressField1 != String.Empty)
            {
                fromAddress.AddressField1 = checkingInput(fromAddress.AddressField1);
            }

            if (fromAddress.AddressField2 != null && fromAddress.AddressField2 != String.Empty)
            {
                fromAddress.AddressField2 = checkingInput(fromAddress.AddressField2);
            }

            if (fromAddress.Name != null && fromAddress.Name != String.Empty)
            {
                fromAddress.Name = checkingInput(fromAddress.Name);
            }

            if (fromAddress.CompanyName != null && fromAddress.CompanyName != String.Empty)
            {
                fromAddress.CompanyName = checkingInput(fromAddress.CompanyName);
            }
            
            
            //To address
            if (toAddress.AddressField1 != null && toAddress.AddressField1 != String.Empty)
            {
                toAddress.AddressField1 = checkingInput(toAddress.AddressField1);
            }
            if (toAddress.AddressField2 != null && toAddress.AddressField2 != String.Empty)
            {
                toAddress.AddressField2 = checkingInput(toAddress.AddressField2);
            }

            if (toAddress.Name != null && toAddress.Name != String.Empty)
            {
                toAddress.Name = checkingInput(toAddress.Name);
            }

            if (toAddress.CompanyName != null && toAddress.CompanyName != String.Empty)
            {
                toAddress.CompanyName = checkingInput(toAddress.CompanyName);
            }
            

            var response = this.skickaService.InformPutInCart(
                basketOrderNumber,
                validationResponse,
                serviceName,
                serviceCode,
                size,
                weight,
                fromAddress,
                toAddress,
                codAmount,
                plusgiro,
                postforskottKontoNr,
                postforskottPaymentReference,
                orderconfirmationUrl,
                urlText,
                url,
                details,
                receiverDestinationId);
        }

        public string FindReceivingPlace(string bookingId)
        {
            ReceivingPlaceBE receivingPlace = skickaService.FindReceivingPlace(bookingId);
            return this.SerializeObjectToJson<ReceivingPlaceBE>(receivingPlace);
        }

        public string GetAvailableOrderLabels()
        {
            this.skickaService = new SkickaServiceAgent(HttpContext.Current);
            List<OrderLabelBE> orderLabels = skickaService.GetAvailableOrderLabels();
            return this.SerializeObjectToJson<List<OrderLabelBE>>(orderLabels);
        }

        public string GetConfigurationItem()
        {
            this.skickaService = new SkickaServiceAgent(HttpContext.Current);
            ConfigurationItemBE configurationItem = skickaService.GetConfigurationItem();
            return this.SerializeObjectToJson<ConfigurationItemBE>(configurationItem);
        }

        public string GetConsignment(string orderId)
        {
            this.skickaService = new SkickaServiceAgent(HttpContext.Current);
            ConsignmentBE consignment = skickaService.GetConsignment(orderId);
            return this.SerializeObjectToJson<ConsignmentBE>(consignment);
        }

        public string GetConsignments(string bookingId)
        {
            this.skickaService = new SkickaServiceAgent(HttpContext.Current);
            List<ConsignmentBE> consignments = skickaService.GetConsignments(bookingId);
            return this.SerializeObjectToJson<List<ConsignmentBE>>(consignments);
        }

        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        ///// <summary>
        ///// Get the city/area name based on zip code
        ///// </summary>
        public string GetDestinations()
        {
            this.pvtService = new PVTController(HttpContext.Current);
            DestinationsBE destinations = pvtService.GetDestinations();
            return this.SerializeObjectToJson<DestinationsBE>(destinations);
        }

        public IEnumerable<byte> GetItemNotesPDF(string bookingId)
        {
            this.skickaService = new SkickaServiceAgent(HttpContext.Current);
            return skickaService.GetItemNotesPDF(bookingId);
        }

        public string GetPreferredProduct(long productId, string destinationID, SizeBE measurements, int weight)
        {
            this.pvtService = new PVTController(HttpContext.Current);
            PriceAndRanksBE result = pvtService.GetPreferredProduct(productId, destinationID, measurements, weight);
            return this.SerializeObjectToJson<PriceAndRanksBE>(result);
        }

        public string GetProducts(string countryCode)
        {
            this.pvtService = new PVTController(HttpContext.Current);
            ProductsBE serviceCombinationBE = pvtService.GetProducts(countryCode);
            return this.SerializeObjectToJson<ProductsBE>(serviceCombinationBE);
        }

        public string GetProductSuggestions(string countryCode, SizeBE measurements, int weight, ConstantsAndEnums.Characteristics[] filters)
        {
            this.pvtService = new PVTController(HttpContext.Current);
            return this.SerializeObjectToJson<PriceAndRanksBE>(pvtService.GetProductSuggestions(countryCode, measurements, weight, filters));
        }

        /// <summary>
        /// Returns a new GUID as the transaction log ID
        /// </summary>
        /// <returns></returns>
        public string GetTransactionLogID()
        {
            return Guid.NewGuid().ToString();
        }

        public string GetValidServicePoints(string bookingId, IEnumerable<string> productionPointIds)
        {
            this.skickaService = new SkickaServiceAgent(HttpContext.Current);
            List<ReceivingPlaceBE> servicePoints = skickaService.GetValidServicePoints(bookingId, productionPointIds.ToArray());
            return SerializeObjectToJson<List<ReceivingPlaceBE>>(servicePoints);
        }

        public string OrderLabels(IEnumerable<string> prodIds, string customerName, string contactPerson, string email, string streetAddress, string zip, string city, string phone, string addressCo)
        {                
           return skickaService.OrderLabels(prodIds.ToArray(), customerName, contactPerson, email, streetAddress, zip, city, phone, addressCo);
        }


        /// <summary>
        /// Removes consignment with the given order number from ItemNotes DB
        /// </summary>
        /// <param name="orderNumber"></param>
        /// <returns></returns>
        public bool RemoveConsignment(string orderNumber)
        {

            return this.skickaService.RemoveConsignment(orderNumber);
        }

        /// <summary>
        /// Returns consignment with the given order number
        /// </summary>
        /// <param name="orderNumber"></param>
        /// <returns></returns>
        public string ReturnConsignment(string orderNumber)
        {
            ConsignmentBE result = this.skickaService.GetConsignment(orderNumber);
            return this.SerializeObjectToJson<ConsignmentBE>(result);
        }

        /// <summary>
        /// Returns all consignments for a list of ordernumbers
        /// </summary>
        /// <param name="orderNumber"></param>
        /// <returns></returns>
        public string ReturnConsignments(List<string> orderNumbers)
        {
            List<ConsignmentBE> consignments = new List<ConsignmentBE>();

            if (!orderNumbers.Equals(null))
            {
                foreach (string orderNumber in orderNumbers)
                {
                    ConsignmentBE result = this.skickaService.GetConsignment(orderNumber);
                    if (result != null)
                    {
                        consignments.Add(result);
                    }
                }
            }

            return this.SerializeObjectToJson<List<ConsignmentBE>>(consignments);
        }

        public string TestService()
        {
            this.pvtService = new PVTController(HttpContext.Current);
            return this.pvtService.TestService();
        }

        public void UpdateOrderInCart(
            string basketOrderNumber,
            ValidationResponseBE validationResponse,
            ServicePropertiesBE serviceCombination,
             string serviceName,
            string serviceCode,
            SizeBE size,
            int weight,
            AddressBE fromAddress,
            AddressBE toAddress,
            string codAmount,
            bool plusgiro,
            string postforskottKontoNr,
            string postforskottPaymentReference,
            string orderconfirmationUrl,
            string urlText,
            string url,
            string details,string receiverDestinationId, string updateOrderId)
        {
            if (fromAddress.AddressField1 != null && fromAddress.AddressField1 != String.Empty)
            {
                fromAddress.AddressField1 = checkingInput(fromAddress.AddressField1);
            }

            if (fromAddress.AddressField2 != null && fromAddress.AddressField2 != String.Empty)
            {
                fromAddress.AddressField2 = checkingInput(fromAddress.AddressField2);
            }

            if (fromAddress.Name != null && fromAddress.Name != String.Empty)
            {
                fromAddress.Name = checkingInput(fromAddress.Name);
            }

            if (fromAddress.CompanyName != null && fromAddress.CompanyName != String.Empty)
            {
                fromAddress.CompanyName = checkingInput(fromAddress.CompanyName);
            }
            //To address
            if (toAddress.AddressField1 != null && toAddress.AddressField1 != String.Empty)
            {
                toAddress.AddressField1 = checkingInput(toAddress.AddressField1);
            }
            if (toAddress.AddressField2 != null && toAddress.AddressField2 != String.Empty)
            {
                toAddress.AddressField2 = checkingInput(toAddress.AddressField2);
            }

            if (toAddress.Name != null && toAddress.Name != String.Empty)
            {
                toAddress.Name = checkingInput(toAddress.Name);
            }

            if (toAddress.CompanyName != null && toAddress.CompanyName != String.Empty)
            {
                toAddress.CompanyName = checkingInput(toAddress.CompanyName);
            }            
            
            var response = this.skickaService.InformUpdateInCart(updateOrderId,
                basketOrderNumber, 
                validationResponse.BaseService.PvtId, 
                serviceName, serviceCode, 
                validationResponse,
                serviceCombination,
                size,
                weight,
                fromAddress,
                toAddress,
                codAmount,
                plusgiro,
                postforskottKontoNr,
                postforskottPaymentReference,
                orderconfirmationUrl,
                urlText,
                url,
                details, receiverDestinationId);
        }

        public string ValidateAddItem(SizeBE size, int weight, IEnumerable<string> additionalServiceIDs, AccountBE account, string countryCode, long selectedServiceID, AddressBE sender, AddressBE receiver, ServicePropertiesBE serviceCombination)
        {
            string returnValue = string.Empty;

            return returnValue;
        }

        /// <summary>
        /// Returns a validatetion response object based on PVT validation 
        /// </summary>
        /// <param name="size"></param>
        /// <param name="weight"></param>
        /// <param name="additionalServiceIDs"></param>
        /// <param name="account"></param>
        /// <param name="countryCode"></param>
        /// <param name="selectedServiceID"></param>
        /// <param name="sender"></param>
        /// <param name="receiver"></param>
        /// <returns></returns>
        public string ValidateItem(SizeBE size, int weight, IEnumerable<string> additionalServiceIDs, AccountBE account, string countryCode, long selectedServiceID, AddressBE sender, AddressBE receiver)
        {
            this.pvtService = new PVTController(HttpContext.Current);

            if (sender.AddressField1 != null && sender.AddressField1 != String.Empty)
            {
                sender.AddressField1 = checkingInput(sender.AddressField1);
            }

            if (sender.AddressField2 != null && sender.AddressField2 != String.Empty)
            {
                sender.AddressField2 = checkingInput(sender.AddressField2);
            }

            if (sender.Name != null && sender.Name != String.Empty)
            {
                sender.Name = checkingInput(sender.Name);
            }

            if (sender.CompanyName != null && sender.CompanyName != String.Empty)
            {
                sender.CompanyName = checkingInput(sender.CompanyName);
            }


            //To address
            if (receiver.AddressField1 != null && receiver.AddressField1 != String.Empty)
            {
                receiver.AddressField1 = checkingInput(receiver.AddressField1);
            }
            if (receiver.AddressField2 != null && receiver.AddressField2 != String.Empty)
            {
                receiver.AddressField2 = checkingInput(receiver.AddressField2);
            }

            if (receiver.Name != null && receiver.Name != String.Empty)
            {
                receiver.Name = checkingInput(receiver.Name);
            }

            if (receiver.CompanyName != null && receiver.CompanyName != String.Empty)
            {
                receiver.CompanyName = checkingInput(receiver.CompanyName);
            }

            ValidationResponseBE returnValue = this.pvtService.ValidateItem(
               
                size,
                weight,
                additionalServiceIDs.ToList(),
                account,
                countryCode,
                selectedServiceID,
                sender,
                receiver);

            return this.SerializeObjectToJson<ValidationResponseBE>(returnValue);
        }

        /// <summary>
        /// Get the city/area name based on zip code
        /// </summary>
        public string ZipToCity(string zip)
        {
            this.pvtService = new PVTController(HttpContext.Current);
            return this.pvtService.ZipToCity(zip);
        }

        public string AddItem(SizeBE size, int weight, IEnumerable<string> additionalServiceIDs, AccountBE account, string countryCode, long selectedServiceID, AddressBE sender, AddressBE receiver,
            ServiceOrderItem serviceOrderItemSettings)
        {
            var returnValue = string.Empty;
            try
            {
                this.pvtService = new PVTController(HttpContext.Current);
                //ValidateItem
                ValidationResponseBE validationValue = this.pvtService.ValidateItem(
                    size,
                    weight,
                    additionalServiceIDs.ToList(),
                    account,
                    countryCode,
                    selectedServiceID,
                    sender,
                    receiver);

                //If data is valid by PVT standards
                if (validationValue.Valid)
                {
                    //REST call to E-Commerce with service order lines
                    ServiceOrderItem serviceOrderItem = serviceOrderItemSettings;

                    // Add complementary values from validation response
                    //cartService.AddItemToCart(serviceOrderItem.ApplicationId, serviceOrderItem.ApplicationName, serviceOrderItem.BasketId, serviceOrderItem.ConfirmationUrl,
                    //serviceOrderItem.CurrencyCode, serviceOrderItem.CustomApplicationLink, serviceOrderItem.CustomApplicationLinkText, serviceOrderItem.Details, serviceOrderItem.EditUrl,
                    //serviceOrderItem.FreeTextField1, serviceOrderItem.FreeTextField2, serviceOrderItem.Name, serviceOrderItem.OrderNumber, serviceOrderItem.ProduceUrl, serviceOrderItem.ServiceImageUrl,
                    //serviceOrderItem.ServiceOrderLines,
                    //subTotal,
                    //vat,
                    //total);

                    //Add to ItemNotes
                    //AddOrderToCart(
                    //basketOrderNumber,
                    //validationValue,
                    //serviceName,
                    //serviceCode,
                    //size,
                    //weight,
                    //fromAddress,
                    //toAddress,
                    //codAmount,
                    //plusgiro,
                    //postforskottKontoNr,
                    //postforskottPaymentReference,
                    //orderconfirmationUrl,
                    //urlText,
                    //url,
                    //details,
                    //receiverDestinationId);
                }
            }
            catch (Exception exception)
            {
                var message = exception.Message;
            }

            return returnValue;
        }

        /// <summary>
        /// Method that takes any type of object and serializes it to Json
        /// </summary>
        /// <typeparam name="T">Type of object to be serialized</typeparam>
        /// <param name="objectInstance">Object to be serialized</param>
        /// <returns>Json string</returns>
        private string SerializeObjectToJson<T>(T objectInstance)
        {           
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            return javaScriptSerializer.Serialize(objectInstance);
        }

        #endregion Methods
    }
}