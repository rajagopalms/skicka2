﻿using System;

namespace Posten.Portal.Cart.BusinessEntities
{
    public class PaymentArticle : IPaymentArticle
    {
        public string ArticleId { get; set; }

        public string ArticleName { get; set; }

        public long Quantity { get; set; }

        public long ArticlePrice { get; set; }

        public long TotalPrice { get; set; }

        public long TotalPriceVatIncluded { get; set; }

        public long Tax { get; set; }

        public string OrderNumber { get; set; }

        public string CompanyCode { get; set; }

        public int TaxRate { get; set; }

        public string TaxCountry { get; set; }

        public string CustomerId { get; set; }

        public int QuantityDecimals { get; set; }

        public string ArticleDescription { get; set; }

        public string Freetext { get; set; }

        public string FootnoteText { get; set; }

        public string ReferenceText { get; set; }

        public string TaxCode { get; set; }
    }
}
