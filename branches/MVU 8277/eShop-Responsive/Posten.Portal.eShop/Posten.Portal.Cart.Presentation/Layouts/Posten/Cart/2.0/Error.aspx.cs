﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;

namespace Posten.Portal.Cart.Presentation.ApplicationPages
{
    public partial class ErrorPage : UnsecuredLayoutsPageBase
    {
        protected override bool AllowAnonymousAccess
        {
            get { return true; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.ErrorMessage.Text = this.Request.QueryString["Message"] ?? string.Empty;
        }
    }
}
