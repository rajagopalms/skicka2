﻿namespace Posten.Portal.Cart.Presentation.ApplicationPages.Legacy
{
    using System;

    using Microsoft.SharePoint;
    using Microsoft.SharePoint.WebControls;

    public partial class PaymentRevert : LayoutsPageBase
    {
        #region Properties

        protected override bool AllowAnonymousAccess
        {
            get { return true; }
        }

        #endregion Properties

        #region Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Redirect("../Common/Payment.aspx?Layout=1.5&Action=Revert&" + Request.Url.Query.Substring(1));
        }

        #endregion Methods
    }
}