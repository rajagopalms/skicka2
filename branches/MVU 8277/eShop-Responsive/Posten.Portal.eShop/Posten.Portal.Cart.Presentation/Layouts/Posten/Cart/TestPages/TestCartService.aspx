﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestCartService.aspx.cs" Inherits="Posten.Portal.Cart.Presentation.ApplicationPages.TestCartService" DynamicMasterPageFile="~masterurl/custom.master" %>

<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
<script type="text/javascript" src="/_layouts/Posten/Cart/Common/Scripts/MiniBasket.js"></script>
<script type="text/javascript" src="/_layouts/Posten/Cart/Common/Scripts/jquery.tmpl.min.js"></script>
</asp:Content>

<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">
<div class="box colspan-4">

<script type="text/javascript">
jQuery(function ($) {
    Posten.MiniBasket.init(function (basket) { $('#kundkorgen_minibasket').html($('#miniBasketTemplate').tmpl(basket)); });
    Posten.MiniBasket.update();
});
</script>

<link href="/_layouts/Posten/Cart/1.5/Styles/minibasket.css" type="text/css" rel="stylesheet" />
<link href="/_layouts/Posten/Cart/1.5/Styles/basket_common.css" type="text/css" rel="stylesheet" />
<!--[if IE 6]>
<link href="/_layouts/Posten/Cart/1.5/Styles/basket_common_ie6.css" type="text/css" rel="stylesheet" />
<![endif]-->
<!--[if IE]>
<link href="/_layouts/Posten/Cart/1.5/Styles/basket_common_ie.css" type="text/css" rel="stylesheet" />
<![endif]-->
<div id="kundkorgen_minibasket"></div>

<script id="miniBasketTemplate" type="text/x-jquery-tmpl"> 
    <div id="minibasketlineitems" class="basket-item-wrapper">
        <div class="inner-wrapper">
            <div class="basket-item-container">
                <div class="top-border"></div>
                <div class="left-border">
                    <ul class="cart-items">
                        {{tmpl(OrderItems) "#orderItemTemplate"}}
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div id="minibasketpanel" class="minibasket-container">
        <table class="minibasket-inner-container">
            <tr>
                <td class="minibasket-spacer"></td>
                <td>
                    <div class="minibasket-separator"></div>
                    <a id="expandLineItemTable">
                        <span class="arrow-right-white">&nbsp;</span>
                        <span id="minibasket_show">Visa varukorg:</span>
                        <span class="minibasket-articles">${NumberOfOrderItems}&nbsp;artiklar</span>
                        <span class="clear"></span>
                    </a>
                    <div class="minibasket-separator"></div>
                </td>
                <td></td>
                <td></td>
                <td>
                    <span class="minibasket-buttons basket-commonstyles"><a id="CheckoutButton" href="#"
                        class="button blue"><span><strong>Till kassan</strong></span> </a></span><span class="payment-amount">
                            ${ValueIncludingVat}
                        </span><span class="payment-sum">Totalt:</span>
                    <div class="clear"></div>
                </td>
                <td class="minibasket-spacer"></td>
            </tr>
        </table>
    </div>
</script>

<script id="orderItemTemplate" type="text/x-jquery-tmpl">
    <li class="product">
        <div class="info">
            <h4>${Name}</h4>
            <p class="description">{DESCRIPTION}</p>
        </div>
        <div class="price-container">
            <a class="button-delete" data-ordernumber="${OrderNumber}" href="javascript:void(0)" title="Ta bort">
                <img src="/_layouts/Posten/Cart/1.5/images/icon-delete.gif" alt="Ta bort" />
            </a>
            {{if EditUrl}}
            <a class="button-edit" data-ordernumber="${OrderNumber}" href="${EditUrl}" title="&Auml;ndra">
                <image src="/_layouts/Posten/Cart/1.5/images/icon-edit.gif" alt="&Auml;ndra" />
            </a>
            {{/if}}
            <div class="h4title">{PRICE}</div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </li>
</script>

</div>
</asp:Content>

<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
Test Mini Basket
</asp:Content>

<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server" >
Test Mini Basket
</asp:Content>
