﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Posten.Portal.Cart.Core, Version=2.0.0.0, Culture=neutral, PublicKeyToken=9f4da00116c38ec5" %>
<%@ Import Namespace="Posten.Portal.Cart.BusinessEntities" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI.WebControls" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestServicePage.aspx.cs" Inherits="Posten.Portal.Cart.Presentation.TestPages.TestServicePage" DynamicMasterPageFile="~masterurl/custom.master" %>

<asp:Content ID="Content9" ContentPlaceHolderID="PlaceHolderMain" runat="server">
<script src="/_layouts/Posten/Cart/Presentation/js/common/jquery.cookie.js" type="text/javascript"></script>
    <script type="text/javascript">
        
    </script>
  <div>
    <p>
      <asp:Label ID="uiAddOrderLineLable" runat="server" Text="Add ServiceOrder" Font-Bold="true"/>
    </p>
    <p>
      <asp:Label ID="Label1" AssociatedControlID="NumberOfLineItemsTextBox" runat="server" Text="Number of line items:"></asp:Label><br />
      <asp:TextBox ID="NumberOfLineItemsTextBox" runat="server" Text="1"></asp:TextBox>
    </p>
    <p>
      <asp:Label ID="Label2" AssociatedControlID="AppIdRadioButton" runat="server" Text="Service: "></asp:Label><br />
          <asp:RadioButtonList ID="AppIdRadioButton" runat="server" >
          <asp:ListItem Selected="True" Value="ItemNotes" Text="Skicka"></asp:ListItem>
      </asp:RadioButtonList></p>
    <p>
      Service Order Name: <asp:TextBox runat="server" ID="txtServicename" Text="DPD Företagspaket 09:00"></asp:TextBox>
    </p>
    <p>
      Free Text Field1:<asp:TextBox runat="server" ID="txtFTF1" Text="Till: anders stuve, 456, 13185, STOCKHOLM."></asp:TextBox>
    </p>
    <p>
      Free Text Field2: <asp:TextBox runat="server" ID="txtFTF2" Text="Mått och vikt: 40x40x40 cm, 3 kg"></asp:TextBox>
    </p>
    <p>
      Details: <asp:TextBox runat="server" ID="txtDetails" Text="<font style='margin-right:10px;font-family:Verdana,Arial,sans-serif'>F&ouml;rs&auml;ndelseid:<a style='color:#031e50;font-size:10px;font-family:Verdana,Arial,sans-serif' href='http://www.posten.se/tracktrace/TrackConsignments_do.jsp?trackntraceAction=saveSearch&amp;lang=SE&amp;consignmentId=81642079655SE' target='_blank'>81642079655SE</a></font>"></asp:TextBox>
    </p>
    <div>
      <asp:Button ID="uiAddServiceOrder" runat="server" Text="Add ServiceOrder to Basket"
        OnClick="uiAddServiceOrder_Click" />
    </div>
    <br />
    <br />
    <h3>
      Basket</h3>
    <table>
      <asp:Repeater ID="uiBasketLineItems" runat="server">
        <HeaderTemplate>
          <tr>
            <th>
              OrderNo
            </th>
          </tr>
        </HeaderTemplate>
        <ItemTemplate>
          <tr>
            <td>
              <%# Eval("OrderNumber") %>
            </td>
          </tr>
        </ItemTemplate>
      </asp:Repeater>
    </table>
  </div>
</asp:Content>