﻿namespace Posten.Portal.Cart.Presentation.ApplicationPages
{
    using System;

    using Microsoft.SharePoint;
    using Microsoft.SharePoint.WebControls;

    public partial class TestCartService : UnsecuredLayoutsPageBase
    {
        #region Properties

        protected override bool AllowAnonymousAccess
        {
            get { return true; }
        }

        #endregion Properties

        #region Methods

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        #endregion Methods
    }
}