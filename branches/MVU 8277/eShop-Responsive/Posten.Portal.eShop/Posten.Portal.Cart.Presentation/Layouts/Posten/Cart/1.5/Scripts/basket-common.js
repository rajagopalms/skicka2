﻿/*globals $, jQuery*/
var PostenBasket = {}; //todo: $.namespace('PostenBasket');
(function() {

  PostenBasket.BasketModalTemplate = '<div class="basket-modal basket-commonstyles" id="basket_modal_delete_confirm">' +
  '<a href="#"><span class="exit close"></span></a>' +
  '<h2>Ta bort orderrad?</h2>' +
  '<p>&Auml;r du s&auml;ker p&aring; att du vill ta bort orderrad?</p><br />' +
  '<a href="#" class="button white-large close"><span><strong>Avbryt</strong></span></a>' +
  '<a href="#" class="button blue-large close basket-modal-ok"><span><strong>Ja</strong></span></a>' +
'</div>';

  PostenBasket.PaymentErrorMessagePopUp = '<div class="basket-modal basket-commonstyles" id="basket_modal_paymenterror">' +
  '<a href="#"><span class="exit close"></span></a>' +
  '<h2>Din beställning kunde tyvärr inte genomföras.</h2>' +
  '<p></p><br />' +
  '<a href="#" class="button blue-large close basket-modal-ok"><span><strong>OK</strong></span></a>' +
'</div>';

  PostenBasket.ShowPaymentErrorMessagePopUp = function(text) {
    $("#s4-bodyContainer").append(PostenBasket.PaymentErrorMessagePopUp);
    $("#basket_modal_paymenterror p").text(text);

    $("#basket_modal_paymenterror").overlay({
      closeOnClick: false,
      color:'#000000',
      loadSpeed : 200,
      opacity: 0.9,
      // load it immediately after the construction
      load: true
    });
  };

})();