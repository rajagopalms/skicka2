﻿
// Requirement:
// jQuery postMessage plugin
// benalman.com/projects/jquery-postmessage-plugin/

window.PostenCosIFrame = new function () {

    this.monitorResize = function () {

        // Keep track of the iframe and height.
        var iframe = jQuery("iframe.applicationFrame"), if_height;

        iframe.each(function (index, item) {
            var oneframe = $(item);
            var iframe_src = oneframe.attr('src').split('/')[0] + '//' + oneframe.attr('src').split('/')[2];

            jQuery.receiveMessage(function (e) {

                // Get the height from the passsed data.
                var h = Number(e.data.replace(/.*if_height=(\d+)(?:&|$)/, '$1'));

                // Scroll hack
                if (!isNaN(h) && h == 0) {
                    //jQuery('#s4-workspace')[0].scrollTop = 0;
                }

                // Height has changed, update the iframe.
                if (!isNaN(h) && h > 0 && h !== if_height) {
                    oneframe.height(if_height = h);
                }
            }, iframe_src);
        });


    }
};