﻿<?xml version="1.0" encoding="utf-8"?>
<feature xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="5ca70d6f-2425-42a8-bb4b-4824de43837f" creator="Jonas Karlman &lt;jonas.karlman@dqc.se&gt;" defaultResourceFile="PostenMyPages" description="$Resources:Feature_PostenMyPages_Presentation_Description;" featureId="5ca70d6f-2425-42a8-bb4b-4824de43837f" imageUrl="Posten/PostenFeature.gif" receiverAssembly="$SharePoint.Project.AssemblyFullName$" receiverClass="$SharePoint.Type.cc24f2b7-b9c9-4808-abe4-14eaa9d0242c.FullName$" solutionId="00000000-0000-0000-0000-000000000000" title="$Resources:Feature_PostenMyPages_Presentation_Title;" version="AAEAAAD/////AQAAAAAAAAAEAQAAAA5TeXN0ZW0uVmVyc2lvbgQAAAAGX01ham9yBl9NaW5vcgZfQnVpbGQJX1JldmlzaW9uAAAAAAgICAgBAAAAAAAAAAAAAAAAAAAACw==" upgradeActionsReceiverAssembly="$SharePoint.Project.AssemblyFullName$" upgradeActionsReceiverClass="$SharePoint.Type.cc24f2b7-b9c9-4808-abe4-14eaa9d0242c.FullName$" deploymentPath="$SharePoint.Feature.FileNameWithoutExtension$_1.0" xmlns="http://schemas.microsoft.com/VisualStudio/2008/SharePointTools/FeatureModel">
  <activationDependencies>
    <referencedFeatureActivationDependency minimumVersion="" itemId="dc386f5f-5c07-43f1-9614-f23a4f557416" />
  </activationDependencies>
  <projectItems>
    <projectItemReference itemId="678bbd25-9095-488b-a48c-09401f351c1a" />
  </projectItems>
</feature>