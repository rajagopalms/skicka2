﻿namespace Posten.Portal.Skicka.Services.BusinessEntities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Contains information about the delivery for this service and destination.
    /// </summary>
    [Serializable]
    [DataContract]
    public class DeliveryInfoBE
    {
        #region Properties

        /// <summary>
        /// True if the delivery will be delayed for this service and destination.
        /// </summary>
        [DataMember]
        public bool Delayed
        {
            get;
            set;
        }

        /// <summary>
        /// Days left before the letter or package reaches its destination.
        /// 0 means the same day. 1 means tomorrow.
        /// </summary>
        [DataMember]
        public int DeliveryDay
        {
            get;
            set;
        }

        /// <summary>
        /// What time the letter or package reaches its destination.
        /// </summary>
        /// <example>
        /// 2359 means 23:59
        /// 14 means 14:00
        /// </example>
        [DataMember]
        public int DeliveryHour
        {
            get;
            set;
        }

        /// <summary>
        /// True if the letter or package will be delivered on a saturday.
        /// </summary>
        [DataMember]
        public bool Saturday
        {
            get;
            set;
        }

        #endregion Properties

        #region Methods

        public override string ToString()
        {
            string deliveryhour = this.DeliveryHour.ToString();
            if (deliveryhour.Length == 4)
            {
                deliveryhour = deliveryhour.Insert(2, ":");
            }

            switch (this.DeliveryDay)
            {
                case 0:
                    return "före klockan " + deliveryhour;

                case 1:
                    return "nästa vardag före klockan " + deliveryhour;

                default:
                    return "inom " + this.DeliveryDay + " arbetsdagar";
            }
        }

        #endregion Methods
    }
}