﻿namespace Posten.Portal.Skicka.Services.BusinessEntities
{
    using System;
    using System.Runtime.Serialization;

    using Posten.Portal.Skicka.PVT.Proxy;

    [DataContract]
    public class ServicePropertiesBE
    {
        #region Constructors

        public ServicePropertiesBE(Properties dataAccessObject)
        {
            this.DataAccessObjectToBusinessEntity(dataAccessObject);
        }

        public ServicePropertiesBE()
        {
        }

        #endregion Constructors

        #region Enumerations

        public enum ShowValue
        {
            HIDE,
            SHOW,
            REQUIRED,
        }

        #endregion Enumerations

        #region Properties

        [DataMember]
        public ShowValue DoorCode
        {
            get;
            set;
        }

        [DataMember]
        public bool FilterCOD
        {
            get;
            set;
        }

        [DataMember]
        public bool FilterDelivery
        {
            get;
            set;
        }

        [DataMember]
        public bool FilterExpress
        {
            get;
            set;
        }

        [DataMember]
        public bool FilterTrace
        {
            get;
            set;
        }

        [DataMember]
        public bool FilterValue
        {
            get;
            set;
        }

        [DataMember]
        public bool LetterAllowed
        {
            get;
            set;
        }

        [DataMember]
        public ShowValue MailNotification
        {
            get;
            set;
        }

        [DataMember]
        public int MaxWeightIntervals
        {
            get;
            set;
        }

        [DataMember]
        public ShowValue PhoneNotification
        {
            get;
            set;
        }

        [DataMember]
        public ShowValue SMSNotification
        {
            get;
            set;
        }

        [DataMember]
        public ShowValue SenderEmail 
        { 
            get; 
            set;
        }

        [DataMember]
        public bool Vat
        {
            get;
            set;
        }

        #endregion Properties

        #region Methods

        private void DataAccessObjectToBusinessEntity(Properties dataAccessObject)
        {
            this.Vat = dataAccessObject.vat;

            this.SMSNotification = (ShowValue)Enum.Parse(typeof(ShowValue), Enum.GetName(typeof(showValue), dataAccessObject.smsNotification));
            this.MailNotification = (ShowValue)Enum.Parse(typeof(ShowValue), Enum.GetName(typeof(showValue), dataAccessObject.mailNotification));
            this.PhoneNotification = (ShowValue)Enum.Parse(typeof(ShowValue), Enum.GetName(typeof(showValue), dataAccessObject.phoneNotification));
            this.SenderEmail = (ShowValue)Enum.Parse(typeof(ShowValue), Enum.GetName(typeof(showValue), dataAccessObject.senderEMail));
            this.DoorCode = (ShowValue)Enum.Parse(typeof(ShowValue), Enum.GetName(typeof(showValue), dataAccessObject.doorCode));
            this.FilterTrace = dataAccessObject.filterTrace;
            this.FilterDelivery = dataAccessObject.filterDelivery;
            this.FilterCOD = dataAccessObject.filterCOD;
            this.FilterExpress = dataAccessObject.filterExpress;
            this.FilterValue = dataAccessObject.filterValue;
            this.LetterAllowed = dataAccessObject.letterAllowed;
            this.MaxWeightIntervals = dataAccessObject.maxWeightIntervals;
        } 

        #endregion Methods
    }
}