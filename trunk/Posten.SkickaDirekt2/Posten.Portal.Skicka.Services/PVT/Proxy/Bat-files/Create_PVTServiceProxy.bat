@Echo off
Rem Set the Host variable according to environment
Rem Set Host=f10.service.utv.posten.se
Rem Host=i10.service.utv.posten.se

Rem Path to executable
Set SvcUtilExe="C:\Program Files (x86)\Microsoft SDKs\Windows\v7.0A\Bin\SvcUtil.exe"

Echo *******************************************
Echo Creates proxy for PVTService
Echo Server: PvtServiceBean.wsdl
Echo *******************************************
Pause

%SvcUtilExe% PvtServiceBean.wsdl /out:..\PVTServiceProxy.cs /config:..\PVTServiceProxy.config /namespace:*,Posten.Portal.Skicka.PVT.Proxy /serializer:XmlSerializer
Pause

Rem NOTE! This file must be saved with encoding CodePage 1252 (not UTF-8!)

