﻿namespace Posten.SkickaDirekt2.WebParts
{
    using System.ComponentModel;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls.WebParts;

    using Microsoft.SharePoint;
    using Microsoft.SharePoint.WebPartPages;

    using Posten.SkickaDirekt2.Resources;
    using Posten.SkickaDirekt2.WebParts.UserControls;

    [ToolboxItemAttribute(false)]
    public class SenderAddressWP : System.Web.UI.WebControls.WebParts.WebPart
    {
        #region Fields

        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/Posten.SkickaDirekt2.WebParts.UserControls/SenderAddressWP/SenderAddressWPUserControl.ascx";

        private string address2LabelProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SenderAddress_Address2Label") as string;
        private string address2TooltipProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SenderAddress_Address2Tooltip") as string;
        private string addressLabelProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SenderAddress_AddressLabel") as string;
        private string cityLabelProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SenderAddress_CityLabel") as string;
        private string cityPlaceholderProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SenderAddress_CityPlaceholder") as string;
        private string companyLabelProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SenderAddress_CompanyLabel") as string;
        private string companyNameLabelProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SenderAddress_CompanyNameLabel") as string;
        private string email2LabelProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SenderAddress_Email2Label") as string;
        private string emailLabelProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SenderAddress_EmailLabel") as string;
        private string emailTooltipProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SenderAddress_EmailTooltip") as string;
        private string headerProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SenderAddress_Header") as string;
        private string nameLabelProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SenderAddress_NameLabel") as string;
        private string organizationNumberLabelProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SenderAddress_OrganizationNumberLabel") as string;
        private string privateLabelProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SenderAddress_PrivateLabel") as string;
        private string senderTypeLabelProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SenderAddress_SenderTypeLabel") as string;
        private string zipLabelProperty = HttpContext.GetGlobalResourceObject("SkickaDirekt2Resources", "WebPart_SenderAddress_ZipLabel") as string;

        #endregion Fields

        #region Properties

        [WebBrowsable(true),
        WebDisplayName("Address field 2 Label:"),
        WebDescription("Label for Address field 2"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string Address2LabelProperty
        {
            get
            {
                return address2LabelProperty;
            }
            set
            {
                address2LabelProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Address field 2 Tooltip:"),
        WebDescription("Tooltip for Address field 2"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string Address2TooltipProperty
        {
            get
            {
                return address2TooltipProperty;
            }
            set
            {
                address2TooltipProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Address Label:"),
        WebDescription("Label for Address field"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string AddressLabelProperty
        {
            get
            {
                return addressLabelProperty;
            }
            set
            {
                addressLabelProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("City Label:"),
        WebDescription("Label for City field"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string CityLabelProperty
        {
            get
            {
                return cityLabelProperty;
            }
            set
            {
                cityLabelProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("City text:"),
        WebDescription("Text to be shown in City field"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string CityPlaceholderProperty
        {
            get
            {
                return cityPlaceholderProperty;
            }
            set
            {
                cityPlaceholderProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Company  Label:"),
        WebDescription("Label for Company radio button "),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string CompanyLabelProperty
        {
            get
            {
                return companyLabelProperty;
            }
            set
            {
                companyLabelProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Company Name Label:"),
        WebDescription("Label for Company Name field"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string CompanyNameLabelProperty
        {
            get
            {
                return companyNameLabelProperty;
            }
            set
            {
                companyNameLabelProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Email verification Label:"),
        WebDescription("Label for Email verification field"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string Email2LabelProperty
        {
            get
            {
                return email2LabelProperty;
            }
            set
            {
                email2LabelProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Email Label:"),
        WebDescription("Label for Email field"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string EmailLabelProperty
        {
            get
            {
                return emailLabelProperty;
            }
            set
            {
                emailLabelProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Email Tooltip:"),
        WebDescription("Tooltip for Email field"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string EmailTooltipProperty
        {
            get
            {
                return emailTooltipProperty;
            }
            set
            {
                emailTooltipProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Header:"),
        WebDescription("Sender Address Header"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string HeaderProperty
        {
            get
            {
                return headerProperty;
            }
            set
            {
                headerProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Name Label:"),
        WebDescription("Label for Name field"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string NameLabelProperty
        {
            get
            {
                return nameLabelProperty;
            }
            set
            {
                nameLabelProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Organization Number Label:"),
        WebDescription("Label for Organization Number field"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string OrganizationNumberLabelProperty
        {
            get
            {
                return organizationNumberLabelProperty;
            }
            set
            {
                organizationNumberLabelProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Private Label:"),
        WebDescription("Label for Private radio button"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string PrivateLabelProperty
        {
            get
            {
                return privateLabelProperty;
            }
            set
            {
                privateLabelProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Private/Company Label:"),
        WebDescription("Label for selection of Private/Company"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string SenderTypeLabelProperty
        {
            get
            {
                return senderTypeLabelProperty;
            }
            set
            {
                senderTypeLabelProperty = value;
            }
        }

        [WebBrowsable(true),
        WebDisplayName("Zip Label:"),
        WebDescription("Label for Zip field"),
        Category(Constants.WebPartCategory),
        Personalizable(PersonalizationScope.Shared)]
        public string ZipLabelProperty
        {
            get
            {
                return zipLabelProperty;
            }
            set
            {
                zipLabelProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        public void SaveProperties()
        {
            SPWeb web = SPContext.Current.Web;
            SPFile file = web.GetFile(HttpContext.Current.Request.Url.ToString());
            using (SPLimitedWebPartManager manager = file.GetLimitedWebPartManager(PersonalizationScope.Shared))
            {
                SenderAddressWP webPart = (SenderAddressWP)manager.WebParts[this.ID];

                webPart.HeaderProperty = this.headerProperty;
                webPart.CompanyNameLabelProperty = this.companyNameLabelProperty;
                webPart.OrganizationNumberLabelProperty = this.organizationNumberLabelProperty;
                webPart.NameLabelProperty = this.nameLabelProperty;
                webPart.AddressLabelProperty = this.addressLabelProperty;
                webPart.Address2LabelProperty = this.address2LabelProperty;
                webPart.ZipLabelProperty = this.zipLabelProperty;
                webPart.EmailLabelProperty = this.emailLabelProperty;
                webPart.Email2LabelProperty = this.email2LabelProperty;
                webPart.SenderTypeLabelProperty = this.senderTypeLabelProperty;
                webPart.PrivateLabelProperty = this.privateLabelProperty;
                webPart.CompanyLabelProperty = this.companyLabelProperty;
                webPart.CityPlaceholderProperty = this.cityPlaceholderProperty;
                webPart.CityLabelProperty = this.cityLabelProperty;
                webPart.Address2TooltipProperty = this.address2TooltipProperty;
                webPart.EmailTooltipProperty = this.emailTooltipProperty;

                web.AllowUnsafeUpdates = true;
                manager.SaveChanges(webPart);
                web.AllowUnsafeUpdates = false;
                manager.Web.Dispose();
            }
        }

        protected override void CreateChildControls()
        {
            SenderAddressWPUserControl control = (SenderAddressWPUserControl)Page.LoadControl(_ascxPath);
            control.ParentWebPart = this;
            Controls.Add(control);
        }

        #endregion Methods
    }
}