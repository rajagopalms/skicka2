﻿namespace Posten.Portal.Skicka.Services.BusinessEntities
{
    using System;
    using System.Runtime.Serialization;

    using Posten.Portal.Skicka.PVT.Proxy;

    [Serializable]
    [DataContract]
    public class PriceAndRankBE
    {
        #region Constructors

        public PriceAndRankBE()
        {
        }

        public PriceAndRankBE(PriceAndRank dataAccessObject)
        {
            this.ProductId = dataAccessObject.productId;
            this.Price = new PriceBE(dataAccessObject.price);
            this.Rank = dataAccessObject.rank;
        }

        #endregion Constructors

        #region Properties

        [DataMember]
        public PriceBE Price
        {
            get;
            set;
        }

        [DataMember]
        public long ProductId
        {
            get;
            set;
        }

        [DataMember]
        public int Rank
        {
            get;
            set;
        }

        #endregion Properties
    }
}