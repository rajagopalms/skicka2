﻿namespace Posten.Portal.SkickaDirekt2.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Xml.Linq;

    using Microsoft.SharePoint.Administration;

    using Posten.SkickaDirekt2.Resources;

    public class ConfigHelper
    {
        #region Fields

        private string ConfigOwner;
        private List<SPWebConfigModification> modifications;
        private List<CompareElement> sourceElements;
        private List<CompareElement> templateElements;

        #endregion Fields

        #region Constructors

        //private LogController logger;
        public ConfigHelper()
        {
            modifications = new List<SPWebConfigModification>();
            sourceElements = new List<CompareElement>();
            templateElements = new List<CompareElement>();
            //logger = new LogController();
        }

        #endregion Constructors

        #region Methods

        public List<SPWebConfigModification> GetModifications(string configOwner, string templateFile, string configurationMode)
        {
            try
            {
                ConfigOwner = configOwner;
                XDocument sourceWebConfig = XMLHelper.ReturnXMLSource(string.Format("{0}/{1}", Constants.pathXml, Constants.SourceWebConfigFilename));

                templateFile = ReturnTargetTemplateConfig(templateFile, configurationMode);
                XDocument templateWebConfig = XMLHelper.ReturnXMLSource(templateFile);

                if (sourceWebConfig != null)
                {
                    FillSourceElements(sourceWebConfig);
                }

                if (templateWebConfig != null)
                {
                    FillTemplateElements(templateWebConfig);
                }

                FillModificationElements();

            }
            catch (Exception exception)
            {
                //logger.LogError(exception);
            }
            return modifications;
        }

        public void RemoveOwnerCustomisations(SPWebApplication webApp, string owner)
        {
            try
            {
                if (webApp != null)
                {
                    List<SPWebConfigModification> mods = webApp.WebConfigModifications.Where(m => m.Owner == owner).ToList();

                    if (mods.Count > 0)
                    {
                        foreach (SPWebConfigModification mod in mods)
                        {
                            webApp.WebConfigModifications.Remove(mod);
                        }
                        webApp.Update();
                        webApp.Farm.Services.GetValue<SPWebService>(webApp.WebService.Id).ApplyWebConfigModifications();
                    }
                }
            }
            catch (Exception exception)
            {
                //logger.LogError(exception);
            }
        }

        public string ReturnTargetTemplateConfig(string templateConfig, string configurationMode)
        {
            string value = templateConfig;
            try
            {
                if (string.IsNullOrEmpty(templateConfig))
                {
                    if (configurationMode.ToUpper().Equals(ConfigurationType.DEV.ToString().ToUpper()))
                    {
                        value = string.Format("{0}/{1}", Constants.pathXml, Constants.SourceTemplateFilename_DEV);
                    }
                    else if (configurationMode.ToUpper().Equals(ConfigurationType.PROD.ToString().ToUpper()))
                    {
                        value = string.Format("{0}/{1}", Constants.pathXml, Constants.SourceTemplateFilename_PROD);
                    }
                    else if (configurationMode.ToUpper().Equals(ConfigurationType.RIT.ToString().ToUpper()))
                    {
                        value = string.Format("{0}/{1}", Constants.pathXml, Constants.SourceTemplateFilename_RIT);
                    }
                    else
                    {   //Defaults to PROD
                        value = string.Format("{0}/{1}", Constants.pathXml, Constants.SourceTemplateFilename_PROD);
                    }
                }
            }
            catch (Exception exception)
            {
                //logger.LogError(exception);
            }
            return value;
        }

        private void FillModificationElements()
        {
            try
            {
                foreach (CompareElement te in templateElements)
                {
                    IEnumerable<CompareElement> elements = sourceElements.Where<CompareElement>(x => x.SimplePath == te.SimplePath);

                    if (elements.Count() <= 0)
                    {
                        SPWebConfigModification ModElement = new SPWebConfigModification()
                        {
                            Path = te.ParentXPath,
                            Name = te.NameXPath,
                            Sequence = (uint)0,
                            Owner = ConfigOwner,
                            Type = SPWebConfigModification.SPWebConfigModificationType.EnsureChildNode,
                            Value = te.Element.ToString()
                        };
                        modifications.Add(ModElement);
                    }
                    else
                    {
                        var element = elements.First<CompareElement>();
                        bool IsTempParent = te.Element.HasElements;

                        if (te.Element.HasAttributes)
                        {
                            bool IsSourceParent = elements.Where<CompareElement>(x => x.Element.HasElements).Count() >= 1;

                            if (IsSourceParent)
                            {
                                foreach (XAttribute attr in te.Element.Attributes())
                                {
                                    SPWebConfigModification ModElement = new SPWebConfigModification()
                                    {
                                        Path = string.Format("{0}/{1}", te.ParentXPath, te.Element.Name.LocalName),
                                        Name = attr.Name.LocalName,
                                        Sequence = (uint)0,
                                        Owner = ConfigOwner,
                                        Type = SPWebConfigModification.SPWebConfigModificationType.EnsureAttribute,
                                        Value = attr.Value
                                    };
                                    modifications.Add(ModElement);
                                }
                            }
                            else
                            {
                                bool ExistExact = elements.Where<CompareElement>(x => x.NameXPath == te.NameXPath).Count() >= 1;
                                if (!ExistExact) // Do not take ownership if exact match, leave as is!
                                {
                                    SPWebConfigModification ModElement = new SPWebConfigModification()
                                    {
                                        Path = te.ParentXPath,
                                        Name = te.NameXPath,
                                        Sequence = (uint)0,
                                        Owner = ConfigOwner,
                                        Type = SPWebConfigModification.SPWebConfigModificationType.EnsureChildNode,
                                        Value = te.Element.ToString()
                                    };
                                    modifications.Add(ModElement);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                //logger.LogError(exception);
            }
        }

        private void FillSourceElements(XDocument sourceWebConfig)
        {
            try
            {
                XElement Root = sourceWebConfig.Root;
                IterateSourceElements(Root, Root.Name.LocalName, "");
            }
            catch (Exception exception)
            {
                //logger.LogError(exception);
            }
        }

        private void FillTemplateElements(XDocument sourceTemplate)
        {
            try
            {
                XElement Root = sourceTemplate.Root;
                IterateTemplateElements(Root, Root.Name.LocalName, "");
            }
            catch (Exception exception)
            {
                //logger.LogError(exception);
            }
        }

        private string GetXpathFromElement(XElement xe)
        {
            try
            {
                string Xpath = xe.Name.LocalName;
                foreach (XAttribute attr in xe.Attributes())
                {
                    string name = attr.Name.LocalName;
                    string value = attr.Value;
                    Xpath += string.Format("[@{0}='{1}']", name, value);
                }
                return Xpath;
            }
            catch (Exception exception)
            {
                //logger.LogError(exception);
            }
            return null;
        }

        private void IterateSourceElements(XElement xeChild, string path, string parentPath)
        {
            try
            {
                string NameXpath = GetXpathFromElement(xeChild);
                sourceElements.Add(new CompareElement() { Element = xeChild, SimplePath = path, ParentXPath = parentPath, NameXPath = NameXpath });

                if (xeChild.HasElements)
                {
                    foreach (XElement xe in xeChild.Nodes())
                    {
                        string ParentPath = parentPath != "" ? string.Format("{0}/{1}", parentPath, NameXpath) : NameXpath;
                        IterateSourceElements(xe, string.Format("{0}/{1}", path, xe.Name.LocalName), ParentPath);
                    }
                }
            }
            catch (Exception exception)
            {
                //logger.LogError(exception);
            }
        }

        private void IterateTemplateElements(XElement xeChild, string path, string parentPath)
        {
            try
            {
                string NameXpath = GetXpathFromElement(xeChild);
                templateElements.Add(new CompareElement() { Element = xeChild, SimplePath = path, ParentXPath = parentPath, NameXPath = NameXpath });

                if (xeChild.HasElements)
                {
                    string ParentPath = parentPath != "" ? string.Format("{0}/{1}", parentPath, NameXpath) : NameXpath;
                    foreach (XElement xe in xeChild.Nodes())
                    {
                        IterateTemplateElements(xe, string.Format("{0}/{1}", path, xe.Name.LocalName), ParentPath);
                    }
                }
            }
            catch (Exception exception)
            {
                //logger.LogError(exception);
            }
        }

        #endregion Methods

        #region Nested Types

        public class CompareElement
        {
            #region Fields

            public XElement Element;
            public string NameXPath;
            public string ParentXPath;
            public string SimplePath;

            #endregion Fields
        }

        #endregion Nested Types
    }
}