﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReceiverAddressWPEdit.ascx.cs" Inherits="Posten.SkickaDirekt2.CustomControls.ReceiverAddressWPEdit" %>

<div id="ReceiverAddressEdit">
    <div class="borders rounded bottomMargin">
        <div class="paddings lightGradient header bottomBorder topRounded">
            <input id="headerPropertyEdit" runat="server" type="text" enableviewstate="true" />
        </div>
    </div>
    <div class="paddings">
        <div class="leftPartEdit">
            <input id="receiverTypeLabelPropertyEdit" class="inputEdit" runat="server" type="text" enableviewstate="true" />
        </div>
        <div class="rightPartEdit rightPartText">
            <input type="radio" />
            <label for="senderTypePrivate" class="rightMargin">
                <input id="privateLabelPropertyEdit" class="inputEditNarrow" runat="server" type="text" enableviewstate="true" />
            </label>
            <input type="radio" class="leftMargin" />
            <label for="senderTypeCompany">
                <input id="companyLabelPropertyEdit" class="inputEditNarrow" runat="server" type="text" enableviewstate="true" />
            </label>
        </div>
        <div class="bothClear bottomMargin"></div>

        <div id="receiverCompanyNameDisplay" class="bottomMargin">
            <div class="leftPartEdit">
                <input id="companyNameLabelPropertyEdit" class="inputEdit" runat="server" type="text" enableviewstate="true" />
            </div>
            <div class="rightPartEdit">
                <input type="text" class="field " />
            </div>
            <div class="bothClear"></div>
        </div>

        <div id="receiverNameDisplay" class="bottomMargin">
            <div class="leftPartEdit">
                <input id="nameLabelPropertyEdit" class="inputEdit" runat="server" type="text" enableviewstate="true" />
            </div>
            <div class="rightPartEdit">
                <input type="text" class="field" />
            </div>
            <div class="bothClear"></div>
        </div>

        <div id="receiverAddressDisplay" class="bottomMargin">
            <div class="leftPartEdit">
                <input id="addressLabelPropertyEdit" class="inputEdit" runat="server" type="text" enableviewstate="true" />
            </div>
            <div class="rightPartEdit">
                <input type="text" class="field" />
            </div>
            <div class="bothClear"></div>
        </div>

        <div id="receiverAddress2Display" class="bottomMargin">
            <div class="leftPartEdit">
                <input id="address2LabelPropertyEdit" class="inputEdit" runat="server" type="text" enableviewstate="true" />
                <span class="circle tooltip">
                    <div class="buttonGradient insideCircle bold">?</div>
                </span>
                <br />
                <textarea class="textareaEdit" rows="3" cols="20" id="address2TooltipPropertyEdit" runat="server" enableviewstate="true" />
            </div>
            <div class="rightPartEdit">
                <input type="text" class="field" />
            </div>
            <div class="bothClear"></div>
        </div>

        <div id="receiverZipCodeDisplay" class="bottomMargin">
            <div class="leftPartEdit">
                <input id="zipLabelPropertyEdit" class="inputEdit" runat="server" type="text" enableviewstate="true" />
            </div>
            <div class="rightPartEdit">
                <input type="text" class="field smallField" />
            </div>
            <div class="bothClear"></div>
        </div>

        <div id="receiverCityDisplay" class="bottomMargin">
            <div class="leftPartEdit">
                <input id="cityLabelPropertyEdit" class="inputEdit" runat="server" type="text" enableviewstate="true" />
            </div>
            <div class="rightPartEdit">
                <input id="cityPlaceholderPropertyEdit" class="field" runat="server" type="text" enableviewstate="true" />
            </div>
            <div class="bothClear"></div>
        </div>

        <div id="receiverEmailDisplay" class="bottomMargin">
            <div class="leftPartEdit">
                <input id="emailLabelPropertyEdit" class="inputEdit" runat="server" type="text" enableviewstate="true" />
                <span class="circle tooltip">
                    <div class="buttonGradient insideCircle bold">?</div>
                </span>
                <br />
                <textarea class="textareaEdit" rows="3" cols="20" id="emailTooltipPropertyEdit" runat="server" enableviewstate="true" />
            </div>
            <div class="rightPartEdit">
                <input type="text" class="field" />
            </div>
            <div class="bothClear"></div>
        </div>

        <div id="receiverEmailConfirmDisplay" class="bottomMargin">
            <div class="leftPartEdit">
                <input id="email2LabelPropertyEdit" class="inputEdit" runat="server" type="text" enableviewstate="true" />
            </div>
            <div class="rightPartEdit">
                <input type="text" class="field" />
            </div>
            <div class="bothClear"></div>
        </div>

        <div id="receiverMobilePhoneDisplay" class="bottomMargin">
            <div class="leftPartEdit">
                <input id="mobilePhoneLabelPropertyEdit" class="inputEdit" runat="server" type="text" enableviewstate="true" />
                <span class="circle tooltip">
                    <div class="buttonGradient insideCircle bold">?</div>
                </span>
                <br />
                <textarea class="textareaEdit" rows="3" cols="20" id="mobilePhoneTooltipPropertyEdit" runat="server" enableviewstate="true" />
            </div>
            <div class="rightPartEdit">
                <input type="text" class="field" />
            </div>
            <div class="bothClear"></div>
        </div>

        <div id="receiverTelephoneDisplay" class="bottomMargin">
            <div class="leftPartEdit">
                <input id="telephoneLabelPropertyEdit" class="inputEdit" runat="server" type="text" enableviewstate="true" />
                <span class="circle tooltip">
                    <div class="buttonGradient insideCircle bold">?</div>
                </span>
                <br />
                <textarea class="textareaEdit" rows="3" cols="20" id="telephoneTooltipPropertyEdit" runat="server" enableviewstate="true" />
            </div>
            <div class="rightPartEdit">
                <input type="text" class="field" />
            </div>
            <div class="bothClear"></div>
        </div>

        <div id="receiverCodeDisplay" class="bottomMargin">
            <div class="leftPartEdit">
                <input id="entryCodeLabelPropertyEdit" class="inputEdit" runat="server" type="text" enableviewstate="true" />
            </div>
            <div class="rightPartEdit">
                <input type="text" class="field" />
            </div>
            <div class="bothClear"></div>
        </div>
    </div>
</div>