﻿namespace Posten.SkickaDirekt2.CustomControls
{
    using System;
    using System.Web.UI;

    public partial class SenderAddressWPDisplay : UserControl
    {
        #region Fields

        private string address2LabelProperty;
        private string address2TooltipProperty;
        private string addressLabelProperty;
        private string cityLabelProperty;
        private string cityPlaceholderProperty;
        private string companyLabelProperty;
        private string companyNameLabelProperty;
        private string email2LabelProperty;
        private string emailLabelProperty;
        private string emailTooltipProperty;
        private string headerProperty;
        private string mobilePhoneLabelProperty;
        private string mobilePhoneTooltipProperty;
        private string nameLabelProperty;
        private string organizationNumberLabelProperty;
        private string privateLabelProperty;
        private string senderTypeLabelProperty;
        private string zipLabelProperty;

        #endregion Fields

        #region Properties

        public string Address2LabelProperty
        {
            get
            {
                return address2LabelProperty;
            }
            set
            {
                address2LabelProperty = value;
            }
        }

        public string Address2TooltipProperty
        {
            get
            {
                return address2TooltipProperty;
            }
            set
            {
                address2TooltipProperty = value;
            }
        }

        public string AddressLabelProperty
        {
            get
            {
                return addressLabelProperty;
            }
            set
            {
                addressLabelProperty = value;
            }
        }

        public string CityLabelProperty
        {
            get
            {
                return cityLabelProperty;
            }
            set
            {
                cityLabelProperty = value;
            }
        }

        public string CityPlaceholderProperty
        {
            get
            {
                return cityPlaceholderProperty;
            }
            set
            {
                cityPlaceholderProperty = value;
            }
        }

        public string CompanyLabelProperty
        {
            get
            {
                return companyLabelProperty;
            }
            set
            {
                companyLabelProperty = value;
            }
        }

        public string CompanyNameLabelProperty
        {
            get
            {
                return companyNameLabelProperty;
            }
            set
            {
                companyNameLabelProperty = value;
            }
        }

        public string Email2LabelProperty
        {
            get
            {
                return email2LabelProperty;
            }
            set
            {
                email2LabelProperty = value;
            }
        }

        public string EmailLabelProperty
        {
            get
            {
                return emailLabelProperty;
            }
            set
            {
                emailLabelProperty = value;
            }
        }

        public string EmailTooltipProperty
        {
            get
            {
                return emailTooltipProperty;
            }
            set
            {
                emailTooltipProperty = value;
            }
        }

        public string HeaderProperty
        {
            get
            {
                return headerProperty;
            }
            set
            {
                headerProperty = value;
            }
        }

        public string MobilePhoneLabelProperty
        {
            get
            {
                return mobilePhoneLabelProperty;
            }
            set
            {
                mobilePhoneLabelProperty = value;
            }
        }

        public string MobilePhoneTooltipProperty
        {
            get
            {
                return mobilePhoneTooltipProperty;
            }
            set
            {
                mobilePhoneTooltipProperty = value;
            }
        }

        public string NameLabelProperty
        {
            get
            {
                return nameLabelProperty;
            }
            set
            {
                nameLabelProperty = value;
            }
        }

        public string OrganizationNumberLabelProperty
        {
            get
            {
                return organizationNumberLabelProperty;
            }
            set
            {
                organizationNumberLabelProperty = value;
            }
        }

        public string PrivateLabelProperty
        {
            get
            {
                return privateLabelProperty;
            }
            set
            {
                privateLabelProperty = value;
            }
        }

        public string SenderTypeLabelProperty
        {
            get
            {
                return senderTypeLabelProperty;
            }
            set
            {
                senderTypeLabelProperty = value;
            }
        }

        public string ZipLabelProperty
        {
            get
            {
                return zipLabelProperty;
            }
            set
            {
                zipLabelProperty = value;
            }
        }

        #endregion Properties

        #region Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            this.headerPropertyDisplay.InnerText = headerProperty;
            this.companyNameLabelPropertyDisplay.InnerText = companyNameLabelProperty;
            this.organizationNumberLabelPropertyDisplay.InnerText = organizationNumberLabelProperty;
            this.nameLabelPropertyDisplay.InnerText = nameLabelProperty;
            this.addressLabelPropertyDisplay.InnerText = addressLabelProperty;
            this.address2LabelPropertyDisplay.InnerText = address2LabelProperty;
            this.zipLabelPropertyDisplay.InnerText = zipLabelProperty;
            this.cityLabelPropertyDisplay.InnerText = cityLabelProperty;
            this.emailLabelPropertyDisplay.InnerText = emailLabelProperty;
            this.email2LabelPropertyDisplay.InnerText = email2LabelProperty;
            this.senderTypeLabelPropertyDisplay.InnerText = senderTypeLabelProperty;
            this.privateLabelPropertyDisplay.InnerText = privateLabelProperty;
            this.companyLabelPropertyDisplay.InnerText = companyLabelProperty;
            this.address2TooltipPropertyDisplay.InnerHtml = address2TooltipProperty;
            this.emailTooltipPropertyDisplay.InnerHtml = emailTooltipProperty;
            this.senderCity.Attributes["placeholder"] = cityPlaceholderProperty;
        }

        #endregion Methods
    }
}