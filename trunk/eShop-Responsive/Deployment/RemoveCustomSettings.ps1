param ([string]$environment)

function Remove-AppSettingNode
	($PathToConfig=$(throw 'Configuration file is required'),
		 [string]$Name = $(throw 'No Name Specified'))
{	
	$doc = $PathToConfig
		$node = $doc.configuration."appSettings".SelectSingleNode("add[@key='$Name']")
		if ($node)
		{
			$doc.configuration."appSettings".removechild($node)
		}
	
}

function Remove-endpointNode
	($PathToConfig=$(throw 'Configuration file is required'),
		 [string]$Name = $(throw 'No Name Specified'))
{	
	$doc = $PathToConfig
		$node = $doc.configuration."system.serviceModel".client.SelectSingleNode("endpoint[@name='$Name']")
		if ($node)
		{
			$doc.configuration."system.serviceModel".client.removechild($node)
		}
	
} 

function Remove-ConfigSettings
([string]$SourcePath=$(throw 'Source file is required'),
	[string]$Environment=$(throw 'Environment Name is required'))
{
	if (Test-Path $SourcePath)
	{
		Write-Host "Loading Source XML"
		$xmldata = new-object "System.Xml.XmlDocument"		
		$xmldata.LoadXml((get-content $SourcePath))		
		
		$webConfig = $xmldata.SelectSingleNode("root/$Environment").getATTRIBUte("webconfigpath")
		
		Write-Host "Loading Web.Config XML"
		$webconfigxml = new-object "System.Xml.XmlDocument"		
		$webconfigxml.LoadXml((get-content $webConfig))		
		
		$settingxmlNode = $xmldata.SelectNodes("root/$Environment/AppSettings/setting")
		foreach($setting in $settingxmlNode)
		{	
			Write-Host "Removing appsettings: " $setting.key
			Remove-AppSettingNode $webconfigxml $setting.key
		}
		
		$endpointxmlNode = $xmldata.SelectNodes("root/$Environment/ServiceEndpoint/endpoint")
		foreach($sep in $endpointxmlNode)
		{			
			Write-Host "Removing service endpoint: " $sep.name
			Remove-endpointNode $webconfigxml $sep.name
		}
		
		$webconfigxml.Save($webConfig)
	}
}
$sourceXML = "source.xml"

Remove-ConfigSettings $sourceXML $environment