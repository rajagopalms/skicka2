﻿using System;
using System.Collections.Generic;
using Posten.Portal.eShop.BusinessEntities;

namespace Posten.Portal.eShop.Services
{
    public class CatalogServiceStub : ICatalogService
    {
        public ICollection<CategoryModel> GetCategory(string key)
        {
            IList<CategoryModel> list = new List<CategoryModel>();

            if (key == Guid.Empty.ToString("N"))
            {
                list.Add(NewCategory(new Guid("00000000-0000-0000-0000-000000000001"), "SubCat11"));
                list.Add(NewCategory(new Guid("00000000-0000-0000-0000-000000000002"), "SubCat12"));
                list.Add(NewCategory(new Guid("00000000-0000-0000-0000-000000000003"), "SubCat13"));
                list.Add(NewCategory(new Guid("00000000-0000-0000-0000-000000000004"), "SubCat14"));
                list.Add(NewCategory(new Guid("00000000-0000-0000-0000-000000000005"), "SubCat15"));
                list.Add(NewCategory(new Guid("00000000-0000-0000-0000-000000000006"), "SubCat16"));
            }
            else if (key == new Guid("aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee").ToString("N"))
            {
                list.Add(NewCategory(new Guid("aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeee1"), "SubCat21"));
                list.Add(NewCategory(new Guid("aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeee2"), "SubCat22"));
                list.Add(NewCategory(new Guid("aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeee3"), "SubCat23"));
                list.Add(NewCategory(new Guid("aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeee4"), "SubCat24"));
                list.Add(NewCategory(new Guid("aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeee5"), "SubCat25"));
                list.Add(NewCategory(new Guid("aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeee6"), "SubCat26"));
            }

            return list;
        }

        public ICollection<CategoryModel> GetCategoryList()
        {
            return this.GetCategory(Guid.Empty.ToString("N"));
        }

        public ICollection<ProductModel> GetProductList()
        {
            return new List<ProductModel>();
        }

        public ProductModel GetProduct(string productId)
        {
            return new ProductModel { Id = productId, Name = productId };
        }

        private static CategoryModel NewCategory(Guid id, string name)
        {
            CategoryModel pc = new CategoryModel();
            pc.Id = id.ToString("N");
            pc.Name = name;
            return pc;
        }


        public static List<Uri> GetImagesUri(string productid)
        {
            List<Uri> images = new List<Uri>();
            if (productid.Equals("240859"))
            {
                images.Add(new Uri(string.Format("http://{0}/_layouts/images/Posten/eshop/ProductSample/240859_1.jpg", Environment.MachineName)));
                images.Add(new Uri(string.Format("http://{0}/_layouts/images/Posten/eshop/ProductSample/240859_1_large.jpg", Environment.MachineName)));
                images.Add(new Uri(string.Format("http://{0}/_layouts/images/Posten/eshop/ProductSample/240859_1_small.jpg", Environment.MachineName)));

                images.Add(new Uri(string.Format("http://{0}/_layouts/images/Posten/eshop/ProductSample/240859_2.jpg", Environment.MachineName)));
                images.Add(new Uri(string.Format("http://{0}/_layouts/images/Posten/eshop/ProductSample/240859_2_large.jpg", Environment.MachineName)));
                images.Add(new Uri(string.Format("http://{0}/_layouts/images/Posten/eshop/ProductSample/240859_2_small.jpg", Environment.MachineName)));

                images.Add(new Uri(string.Format("http://{0}/_layouts/images/Posten/eshop/ProductSample/240859_3.jpg", Environment.MachineName)));
                images.Add(new Uri(string.Format("http://{0}/_layouts/images/Posten/eshop/ProductSample/240859_3_large.jpg", Environment.MachineName)));
                images.Add(new Uri(string.Format("http://{0}/_layouts/images/Posten/eshop/ProductSample/240859_3_small.jpg", Environment.MachineName)));
            }
            else if (productid.Equals("242186"))
            {
                images.Add(new Uri(string.Format("http://{0}/_layouts/images/Posten/eshop/ProductSample/242186_1.jpg", Environment.MachineName)));
                images.Add(new Uri(string.Format("http://{0}/_layouts/images/Posten/eshop/ProductSample/242186_1_large.jpg", Environment.MachineName)));
                images.Add(new Uri(string.Format("http://{0}/_layouts/images/Posten/eshop/ProductSample/242186_1_small.jpg", Environment.MachineName)));

                images.Add(new Uri(string.Format("http://{0}/_layouts/images/Posten/eshop/ProductSample/242186_2.jpg", Environment.MachineName)));
                images.Add(new Uri(string.Format("http://{0}/_layouts/images/Posten/eshop/ProductSample/242186_2_large.jpg", Environment.MachineName)));
                images.Add(new Uri(string.Format("http://{0}/_layouts/images/Posten/eshop/ProductSample/242186_2_small.jpg", Environment.MachineName)));

                images.Add(new Uri(string.Format("http://{0}/_layouts/images/Posten/eshop/ProductSample/242186_3.jpg", Environment.MachineName)));
                images.Add(new Uri(string.Format("http://{0}/_layouts/images/Posten/eshop/ProductSample/242186_3_large.jpg", Environment.MachineName)));
                images.Add(new Uri(string.Format("http://{0}/_layouts/images/Posten/eshop/ProductSample/242186_3_small.jpg", Environment.MachineName)));

            }

            else if (productid.Equals("241998"))
            {
                images.Add(new Uri(string.Format("http://{0}/_layouts/images/Posten/eshop/ProductSample/241998_1.jpg", Environment.MachineName)));
                images.Add(new Uri(string.Format("http://{0}/_layouts/images/Posten/eshop/ProductSample/241998_1_large.jpg", Environment.MachineName)));
                images.Add(new Uri(string.Format("http://{0}/_layouts/images/Posten/eshop/ProductSample/241998_1_small.jpg", Environment.MachineName)));

                images.Add(new Uri(string.Format("http://{0}/_layouts/images/Posten/eshop/ProductSample/241998_2.jpg", Environment.MachineName)));
                images.Add(new Uri(string.Format("http://{0}/_layouts/images/Posten/eshop/ProductSample/241998_2_large.jpg", Environment.MachineName)));
                images.Add(new Uri(string.Format("http://{0}/_layouts/images/Posten/eshop/ProductSample/241998_2_small.jpg", Environment.MachineName)));

                images.Add(new Uri(string.Format("http://{0}/_layouts/images/Posten/eshop/ProductSample/241998_3.jpg", Environment.MachineName)));
                images.Add(new Uri(string.Format("http://{0}/_layouts/images/Posten/eshop/ProductSample/241998_3_large.jpg", Environment.MachineName)));
                images.Add(new Uri(string.Format("http://{0}/_layouts/images/Posten/eshop/ProductSample/241998_3_small.jpg", Environment.MachineName)));
                
            }

            return images;
        }

        public static List<string> GetProductPackages()
        {
            List<string> packages = new List<string>();
            packages.Add("10 box");
            packages.Add("20 box");
            packages.Add("30 box");
            return packages;
        }
    }
}
