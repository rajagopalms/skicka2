﻿using System;

namespace Posten.Portal.Cart.BusinessEntities
{
    public class ServiceOrderLineItem : IServiceOrderLineItem
    {
        public string ArticleId { get; set; }

        public string ArticleName { get; set; }

        public decimal ArticlePrice { get; set; }

        public string CompanyCode { get; set; }

        public string CustomerId { get; set; }

        public string FootnoteText { get; set; }

        public int OrderLineNumber { get; set; }

        public decimal Quantity { get; set; }

        public string TaxCountry { get; set; }

        public string VatCode { get; set; }

        public decimal Total { get; set; }

        public decimal Vat { get; set; }

        public decimal VatPercentage { get; set; }
    }
}
