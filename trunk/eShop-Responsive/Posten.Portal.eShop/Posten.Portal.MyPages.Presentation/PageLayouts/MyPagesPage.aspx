﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Register Tagprefix="SharePointWebControls" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="PublishingWebControls" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="PublishingNavigation" Namespace="Microsoft.SharePoint.Publishing.Navigation" Assembly="Microsoft.SharePoint.Publishing, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="MyPagesWebControls" Namespace="Posten.Portal.MyPages.Presentation.CustomControls" Assembly="$SharePoint.Project.AssemblyFullName$" %>
<%@ Page language="C#" Inherits="Posten.Portal.MyPages.Presentation.PageLayouts.MyPagesLayoutPage" %>

<asp:Content ContentPlaceholderID="PlaceHolderAdditionalPageHead" runat="server">
	<SharePointWebControls:CssRegistration name="<% $SPUrl:~sitecollection/Style Library/~language/Core Styles/page-layouts-21.css %>" runat="server"/>
	<PublishingWebControls:EditModePanel runat="server">
		<!-- Styles for edit mode only-->
		<SharePointWebControls:CssRegistration name="<% $SPUrl:~sitecollection/Style Library/~language/Core Styles/edit-mode-21.css %>"
			After="<% $SPUrl:~sitecollection/Style Library/~language/Core Styles/page-layouts-21.css %>" runat="server"/>
	</PublishingWebControls:EditModePanel>
	<SharePointWebControls:CssRegistration name="<% $SPUrl:~sitecollection/Style Library/~language/Core Styles/rca.css %>" runat="server"/>
    <SharePointWebControls:CssRegistration name="/_layouts/Posten/MyPages/Styles/mypages.css" runat="server"/>
	<SharePointWebControls:FieldValue id="PageStylesField" FieldName="HeaderStyleDefinitions" runat="server"/>
</asp:Content>
<asp:Content ContentPlaceholderID="PlaceHolderPageTitle" runat="server">
	<SharePointWebControls:FieldValue id="PageTitle" FieldName="Title" runat="server"/>
</asp:Content>
<asp:Content ContentPlaceholderID="PlaceHolderPageTitleInTitleArea" runat="server">
	<SharePointWebControls:FieldValue FieldName="Title" runat="server"/>
</asp:Content>
<asp:Content ContentPlaceHolderId="PlaceHolderTitleBreadcrumb" runat="server">
	<SharePointWebControls:ListSiteMapPath runat="server" SiteMapProviders="CurrentNavigation" RenderCurrentNodeAsLink="false" PathSeparator="" CssClass="s4-breadcrumb" NodeStyle-CssClass="s4-breadcrumbNode" CurrentNodeStyle-CssClass="s4-breadcrumbCurrentNode" RootNodeStyle-CssClass="s4-breadcrumbRootNode" NodeImageOffsetX=0 NodeImageOffsetY=353 NodeImageWidth=16 NodeImageHeight=16 NodeImageUrl="/_layouts/images/fgimg.png" HideInteriorRootNodes="true" SkipLinkText="" />
</asp:Content>
<asp:Content ContentPlaceholderID="PlaceHolderMain" runat="server">
	<div class="box colspan-3 page">
        <h1 class="title">
            <SharePointWebControls:TextField runat="server" FieldName="Title" />
        </h1>
        <PostenWebControls:HideEmptyField FieldName="PublishingPageImage" runat="server">
            <div class="captioned-image">
                <div class="image">
	                <PublishingWebControls:RichImageField FieldName="PublishingPageImage" runat="server"/>
                </div>
                <div class="caption">
	                <PublishingWebControls:RichHtmlField FieldName="PublishingImageCaption" AllowTextMarkup="false" AllowTables="false" AllowLists="false" AllowHeadings="false" AllowStyles="false" AllowFontColorsMenu="false" AllowParagraphFormatting="false" AllowFonts="false" AllowInsert="false" PreviewValueSize="Small" runat="server"/>
                </div>
            </div>
        </PostenWebControls:HideEmptyField>
        <PostenWebControls:HideEmptyField FieldName="Comments" runat="server">
            <div class="introduction">
                <SharePointWebControls:NoteField FieldName="Comments" runat="server"/>
            </div>
        </PostenWebControls:HideEmptyField>
        <div class="webpartzone">
            <WebPartPages:WebPartZone runat="server" ID="LeftColumnZone" AllowPersonalization="False" PartChromeType="None" Title="<%$Resources:cms,WebPartZoneTitle_LeftColumn%>" />
        </div>
    </div>
    <div class="box webpartzone">
        <WebPartPages:WebPartZone runat="server" ID="RightColumnZone" AllowPersonalization="False" PartChromeType="None" Title="<%$Resources:cms,WebPartZoneTitle_RightColumn%>" />
    </div>
</asp:Content>
