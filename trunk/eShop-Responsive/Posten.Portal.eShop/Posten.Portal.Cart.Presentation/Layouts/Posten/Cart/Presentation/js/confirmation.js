﻿window.confirmation = function () { };

confirmation.prototype.init = function () {

    //remove cookies set in checkout
    $.cookie("private_comp_change", null, { path: "/", expires: 7 });
    $.cookie("setprivateorcompany", null, { path: "/", expires: 7 });
    $.cookie("isprivate", null, { path: "/", expires: 7 });
    $.cookie("ctl00_PlaceHolderMain_ucCheckOut_txtSocialSecNo", null, { path: "/", expires: 7 });
    $.cookie("ctl00_PlaceHolderMain_ucCheckOut_InvoiceMobileNoTextBox", null, { path: "/", expires: 7 });
    $.cookie("ctl00_PlaceHolderMain_ucCheckOut_InvoiceLastNameTextBox", null, { path: "/", expires: 7 });
    $.cookie("ctl00_PlaceHolderMain_ucCheckOut_InvoiceFirstNameTextBox", null, { path: "/", expires: 7 });
    $.cookie("getaddressbuttonclicked", null, { path: "/", expires: 7 });
    $.cookie("return_address", null, { path: "/", expires: 7 });

    //hide minicart.
    $("#GlobalMenu, #Search, #GlobalCart, #GlobalUserInfo, #Footer").html("");
    $(".SubMenu").parent().html("");

    //set url of the logo posten.se
    $("#Logo a.logo").attr("href", "http://www.posten.se");


    //this.GetHeight();

    $("#lnkShowOrder").click(function () {
        if ($("#divShowOrder").is(":visible")) {
            $("#divShowOrder").hide();
        }
        else {
            $("#divShowOrder").show();
        }
    });

    PostenCosIFrame.monitorResize();

}

confirmation.prototype.ShowGlobalCart = function () {
    $("#GlobalCart").show();
}


